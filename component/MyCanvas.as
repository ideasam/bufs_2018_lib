package component
{
	import starling.geom.Polygon;
	import starling.display.Canvas;

	/**
	 * @author everseen
	 */
	public class MyCanvas extends Canvas
	{
		private var _thickness:Number = 30;
		private var _color:uint = 0x000000;
		private var _alpha:Number = 1;
		private var _fromX:Number;
		private var _fromY:Number;
 
		public function MyCanvas()
		{
			super();
			
		}
 
		public function lineStyle(thickness:Number = 1, color:uint = 0x000000, alpha:Number = 1):void
		{
			_thickness = thickness;
			_color = color;
			_alpha = alpha;
			beginFill(_color, _alpha);
		}
 
		public function moveTo(x:Number, y:Number):void
		{
			_fromX = x;
			_fromY = y;
		}
 
		public function lineTo(x:Number, y:Number):void
		{
			this.drawCircle(x, y, _thickness/2);
			this.drawPolygon(line(_fromX, _fromY, x, y, _thickness));
			_fromX = x;
			_fromY = y;
		}
 
 		private function line(fromX:Number, fromY:Number, toX:Number, toY:Number, thickness:Number):Polygon
		{
			var len:Number, fXOffset:Number, fYOffset:Number;
 				
			fXOffset = toX - fromX;
			fYOffset = toY - fromY;
 
			len = Math.sqrt(fXOffset * fXOffset + fYOffset * fYOffset);
			fXOffset = fXOffset * thickness / (len * 2);
			fYOffset = fYOffset * thickness / (len * 2);
			
			return new Polygon ([
				fromX + fYOffset, fromY - fXOffset,
				toX  + fYOffset, toY - fXOffset,
				toX  - fYOffset, toY + fXOffset,
				fromX - fYOffset, fromY + fXOffset
			]);
		}
	}
}