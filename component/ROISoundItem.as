package component
{
	import flash.geom.Rectangle;

	public class ROISoundItem
	{
		public var _bounds:Rectangle;
		public var _sound:String = "";
		
		public function get bounds():Rectangle { return _bounds }
		public function get sound():String { return _sound }
		
		public function ROISoundItem(bounds:Rectangle, sound:String)
		{
			_bounds = bounds;
			_sound = sound; 
		}
	}
}