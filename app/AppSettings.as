package app
{
	import pages.PageNavigator;
	
	public class AppSettings
	{
		public static var isIOS:Boolean = false;
		public static var ratio:Number;
		
		public static var width:int;
		public static var height:int;
		
		public static var DefaultColor:int = 0xF0F0F0;
		
		//단어페이지에 나오는 버튼 갯수.
		public static var pageNavigator:PageNavigator = null;
		public static var WORDPAGE_TYPE:int = 3;
		public static var PAGE_CLASSES:Array = null;
		public static var PAGE_CATEGORY_TITLES:Array = null;
		public static var APPBAR_NAME:String = "";
		public static var SUBJECT_CODE:String = "";
		public static var IS_IMAGE_TEXT:Boolean = false;
		
		public static const CONV_USER_STR:String = "유저";
		public static const CONV_INOF_STR:String = "지문";
		
		public static var isTablet:Boolean = false;
		
		//페이지별 폰트 옵션
		public static var fsLightColor :uint = 0xffffff;
		public static var fsDarkColor  :uint = 0x000000;
		public static var fsDoulosColor:uint = 0x333333;		
		public static var fsLightSize :int = 24;
		public static var fsDarkSize  :int = 24;
		public static var fsDoulosSize:int = 24;
		
		public static var fsWordPageWordColor:uint = 0x559966;
		public static var fsWordPagePronColor:uint = 0x666666;
		public static var fsWordPageMeanColor:uint = 0x333333;		
		public static var fsWordPageWordSize:int = 30;
		public static var fsWordPagePronSize:int = 30;
		public static var fsWordPageMeanSize:int = 30;
		
		public static var fsConversationPageTitleColor :uint = 0x000000;
		public static var fsConversationPageOriginColor:uint = 0x000000;
		public static var fsConversationPageTransColor :uint = 0x559966;		
		public static var fsConversationPageTitleSize :int = 24;
		public static var fsConversationPageOriginSize:int = 24;
		public static var fsConversationPageTransSize :int = 24;
	}
}