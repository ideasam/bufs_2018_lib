package pages.type
{
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.DataLoader;
	import com.greensock.loading.LoaderMax;
	
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	
	import app.AppSettings;
	
	import component.RectItem;
	
	import feathers.controls.ImageLoader;
	import feathers.controls.ScrollContainer;
	import feathers.controls.ToggleButton;
	
	import pages.PageContent;
	
	import starling.display.Image;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.filters.GlowFilter;
	
	public class ToggleDialogPage extends BasePage
	{
		protected static const TAG_RECT:String = "tag_rect";
		protected static const TAG_SND_TEXT:String = "tag_sound_text";
		
		private var imgLoader:ImageLoader = null;
		private var imgLoaderScreen:ImageLoader = null;
		private var scrollCont:ScrollContainer;
		
		private var toggleBtn:ToggleButton;
		
		private var _loaderQueue:LoaderMax = null;
		private var _dataLoaderJsonRect:DataLoader = null;
		private var _dataLoaderTxtSoundTime:DataLoader = null;
		
		
		public function ToggleDialogPage()
		{
			this.hasPlayerDock = true;
			
			super();
		}
		
		override public function draw():void
		{
			scrollCont = new ScrollContainer();
			scrollCont.minimumDragDistance = 0.2;
			scrollCont.width = AppSettings.width;
			scrollCont.height = AppSettings.height;
			this.addChild(scrollCont);
			
			imgLoader = new ImageLoader();
			imgLoader.visible = false;
			scrollCont.addChild(imgLoader);
			
			imgLoaderScreen = new ImageLoader();
			imgLoaderScreen.touchable = false;
			
			scrollCont.addChild(imgLoaderScreen);
			
			toggleBtn = new ToggleButton();
			toggleBtn.defaultSkin = new Image(Root.assets.getTexture("btnMeaning_D"));
			toggleBtn.defaultSelectedSkin = new Image(Root.assets.getTexture("btnMeaning_N"));
			toggleBtn.x = 510;
			toggleBtn.y = AppSettings.height - 140;
			this.addChild(toggleBtn);
			
			super.draw();
			
			mPlayerDock.visible = false;
			mPlayerDock.addEventListener(Event.CHANGE, onChangePlayDock);
		}
		
		private function onChangePlayDock(event:Event, sequence:int):void
		{
			trace("sequence:",sequence);
			rectEffect.filter = null;
			
			rectEffect.x = vecSoundItem[sequence].bounds.x;
			rectEffect.y = vecSoundItem[sequence].bounds.y;
			rectEffect.width = vecSoundItem[sequence].bounds.width;
			rectEffect.height = vecSoundItem[sequence].bounds.height;
			
			rectEffect.filter = new GlowFilter(0x29CDC8,0.8,8);
			rectEffect.visible = true;
		}
		
		override public function loadContent():void
		{
			imgLoader.addEventListener(Event.COMPLETE, onCompleteLoad);
			
			if(!imgLoader.source) imgLoader.source = new URLRequest("data/img/chapter"+idxChapter+"/page"+idxPage+".png").url;
			if(!imgLoaderScreen.source) 
			{
				imgLoaderScreen.source = new URLRequest("data/img/chapter"+idxChapter+"/page"+idxPage+"w.png").url;
				toggleBtn.addEventListener(Event.TRIGGERED, onTriggeredToggle);
			}
			
			if(!_dataLoaderJsonRect)
			{
				_dataLoaderJsonRect = new DataLoader("data/rect/ch"+idxChapter+"_"+idxPage+".txt", {name:TAG_RECT});
				_dataLoaderTxtSoundTime = new DataLoader("data/sound/ch"+idxChapter+"_"+idxPage+".txt", {name:TAG_SND_TEXT});
				_loaderQueue = new LoaderMax({name:"mainQueue", onComplete:completeHandler});
				_loaderQueue.append(_dataLoaderJsonRect);
				_loaderQueue.append(_dataLoaderTxtSoundTime);
				_loaderQueue.load();
				
				mPlayerDock.setSource(new URLRequest("data/sound/ch"+idxChapter+"_"+idxPage+".mp3").url);
			}
			
			super.loadContent();
		}
		
		override public function activate():void
		{
			super.activate();
			
			//mPlayerDock.setSound(new URLRequest("data/sound/ch"+idxChapter+"_"+idxPage+".mp3").url);
		}
		
		private var prevTouchIdx:int = -1;
		private var vecSoundItem:Vector.<RectItem> = new Vector.<RectItem>;
		
		private function completeHandler(event:LoaderEvent):void 
		{
			var strJsonRect:String = LoaderMax.getContent(TAG_RECT);
			var strSoundTime:String = LoaderMax.getContent(TAG_SND_TEXT);
			
			if(!strJsonRect || !strSoundTime) return;
			
			var arrTimes:Array = strSoundTime.split("\n");
			
			scrollCont.addChildAt(rectEffect,0);
			
			var jsData:Object = JSON.parse(strJsonRect);
			var len:int = jsData.rects.length;
			
			trace("?",len, arrTimes.length);
			
			if(arrTimes.length < len)
			{
				len = arrTimes.length;
			}
			
			for(var i:int=0; i<len;++i)
			{
				vecSoundItem.push(new RectItem(new Rectangle(jsData.rects[i].x, jsData.rects[i].y, jsData.rects[i].width,jsData.rects[i].height)));
			}
			
			
			
			for (i = 0; i < len; i++)
			{
				var strTime:String = arrTimes[i];
				
				var arrColon:Array = strTime.split(":");
				var strColon:String = arrColon[1]; 
				var arrComma:Array = strColon.split(".");
				
				for (var j:int = 0; j < arrComma.length; j++) 
					arrComma[j] = parseFloat(arrComma[j]); 
				
				vecSoundItem[i].min = arrColon[0];
				vecSoundItem[i].sec = arrComma[0];
				vecSoundItem[i].mil = arrComma[1];
			}
			
			mPlayerDock.setSoundItem(vecSoundItem);
			
			scrollCont.addEventListener(TouchEvent.TOUCH, onTouchCont);
		}
		
		private function onTouchCont(event:TouchEvent):void
		{
			if(scrollCont.isScrolling || PageContent.isContentScrolling) return;
			
			var touch:Touch = event.getTouch(imgLoader);
			
			if(touch)
			{
				switch(touch.phase)
				{
					case TouchPhase.BEGAN:
						prevTouchIdx = getTouchAreaIdx();
						break;
					
					case TouchPhase.ENDED:
						
						var curTouchIdx:int = getTouchAreaIdx();
						if(prevTouchIdx > -1 && curTouchIdx>-1)
						{
							//this.dispatchEventWith(Event.CHANGE, false, curTouchIdx+1);
							rectEffect.filter = null;
							
							rectEffect.x = vecSoundItem[curTouchIdx].bounds.x;
							rectEffect.y = vecSoundItem[curTouchIdx].bounds.y;
							rectEffect.width = vecSoundItem[curTouchIdx].bounds.width;
							rectEffect.height = vecSoundItem[curTouchIdx].bounds.height;
							
							rectEffect.filter = new GlowFilter(0x29CDC8,0.8,8);
							rectEffect.visible = true;
							
							mPlayerDock.playPart(curTouchIdx,true);
						}
						
						prevTouchIdx = -1;
						break;
				}
			}
			
			function getTouchAreaIdx():int
			{
				var len:int =vecSoundItem.length;
				for (var i:int=0; i<len; ++i)
				{
					if(vecSoundItem[i].bounds.containsPoint(touch.getLocation(imgLoader)))
					{
						return i;
					}
				}
				return -1;
			}
		}
		
		private function onCompleteLoad():void
		{
			imgLoader.removeEventListener(Event.COMPLETE, onCompleteLoad);
			imgLoader.validate();
			
			imgLoader.visible = true;
			
			this.endIndicator.y = imgLoader.y + imgLoader.height+20;
			scrollCont.addChild(endIndicator);
		}
		
		private function onTriggeredToggle(event:Event):void
		{
			event.stopPropagation();
			imgLoaderScreen.visible =toggleBtn.isSelected;
		}
		
		override public function unloadContent():void
		{
			super.unloadContent();
			
			imgLoader.source = null;
			imgLoaderScreen.source = null;
			
			if(rectEffect) rectEffect.filter = null;
			
			if(_loaderQueue) _loaderQueue.cancel();
			if(mPlayerDock) mPlayerDock.stop();
			
			_dataLoaderJsonRect = null;
			_dataLoaderTxtSoundTime =null;
		}
	}
}
