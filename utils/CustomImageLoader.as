package utils
{
	import flash.display.Bitmap;
	
	import feathers.controls.ImageLoader;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;

	public class CustomImageLoader extends Sprite
	{
		private var _bitmap:Bitmap;
		private var _loader:ImageLoader = null;
		
		public function CustomImageLoader()
		{
			_loader = new ImageLoader();
			this.addChild(_loader);
			
			//_loader.addEventListener(Event.COMPLETE, onComleteLoad);
		}
		
		private function onComleteLoad():void
		{
			this.dispatchEventWith(Event.COMPLETE);
		}
		
		public function get bitmap():Bitmap
		{
			return _bitmap;
		}

		public function set bitmap(value:Bitmap):void
		{
			_bitmap = value;
			_loader.source = Texture.fromBitmap(value);
		}

		public function terminate():void
		{
			if(_bitmap) _bitmap.bitmapData.dispose();
			
			if(_loader) _loader.source = null;
		}
	}
}