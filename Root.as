package
{
	import com.greensock.TweenMax;
	
	import app.AppSettings;
	import app.LocalSavedData;
	
	import feathers.controls.StackScreenNavigator;
	import feathers.extensions.MaterialDesignSpinner;
	
	import pages.PageEvent;
	import pages.PageNavigator;
	import pages.data.StudyInfo;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.display.Stage;
	import starling.assets.AssetManager;
	
	import theme.CustomTheme;
	
	public class Root extends Sprite
	{
		private static var sAssets:AssetManager;
		private static var pageNavigator:PageNavigator;
		private static var _spinner:MaterialDesignSpinner;
		
		public function Root()
		{
			super();
		}
		
		public function start(assets:AssetManager, removeSplash:Function):void
		{
			trace("ROOT - start");
			_spinner = new MaterialDesignSpinner();
			_spinner.scale = 1.3;
			_spinner.color = 0x00BCD4;
			//_spinner.width = 200;
			//_spinner.height = 200;
			//stage.addChild(_spinner);
			
			
			_spinner.x = (AppSettings.width - 60)>>1;
			_spinner.y = (AppSettings.height - 100);
//			_spinner.y = (AppSettings.height - 48)>>1;
			
			sAssets = assets;
			
			new CustomTheme();
			
			StudyInfo.currentChapter = LocalSavedData.latestStudyChapter;
			
			pageNavigator = new PageNavigator();
			pageNavigator.addEventListener(PageEvent.LOADED, onCompleteDraw);
			addChild(pageNavigator);
			
			function onCompleteDraw():void
			{	
				pageNavigator.removeEventListener(PageEvent.LOADED, onCompleteDraw);
				TweenMax.delayedCall(0.5, removeSplash);
			}
		}
		
		public static function showLoadingSpinner(isShow:Boolean):void
		{
			var _stage:Stage = Starling.current.stage;
			if(isShow && !_stage.contains(_spinner))
			{
				//_stage.touchable = false;
				_stage.addChild(_spinner);
			}
			else 
			{
				_stage.removeChild(_spinner);
				//_stage.touchable = true;
			}
		}
		
		public static function get assets():AssetManager { return sAssets; }
		public static function get navigator():StackScreenNavigator
		{
			return pageNavigator._navigator;
		}
	}
}