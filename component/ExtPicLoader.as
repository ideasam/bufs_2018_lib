package component
{
	import flash.system.ImageDecodingPolicy;
	import flash.system.LoaderContext;
	import flash.events.IOErrorEvent;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.net.URLRequest;
	
	import starling.textures.Texture;

	public class ExtPicLoader
	{
		public function ExtPicLoader()
		{
		}
		
		public static function loadBitmap(url:String, onComplete:Function):void
		{
			// create the loader
			var loader:Loader = new Loader();
			var loaderContext:LoaderContext = new LoaderContext();
   			loaderContext.imageDecodingPolicy = ImageDecodingPolicy.ON_LOAD;
			loader.load ( new URLRequest (url) , loaderContext);
			loader.contentLoaderInfo.addEventListener ( flash.events.Event.COMPLETE, onCompleteLoad );
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onError);

			function onCompleteLoad ( event : flash.events.Event ):void
			{
				// grab the loaded bitmap
				var loadedBitmap:Bitmap = event.currentTarget.loader.content as Bitmap;
				onComplete(loadedBitmap);
			}
			
			function onError(event:*):void
			{
				//trace(event);
			}
		}
		
		public static function loadTexture(url:String, onComplete:Function):void
		{
			// create the loader
			var loader:Loader = new Loader();
			loader.load ( new URLRequest (url) );
			loader.contentLoaderInfo.addEventListener ( flash.events.Event.COMPLETE, onCompleteLoad );
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onError);
			
			function onCompleteLoad ( event : flash.events.Event ):void
			{
				// grab the loaded bitmap
				var loadedBitmap:Bitmap = event.currentTarget.loader.content as Bitmap;
				
				// create a texture from the loaded bitmap
				var texture:Texture = Texture.fromBitmap ( loadedBitmap );
//				var img:Image = new Image(texture);
//				addChild(img); 
				
				onComplete(texture);
			}
			
			function onError(event:*):void
			{
				//trace(event);
			}
		}
	}
}