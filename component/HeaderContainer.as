package component
{
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	
	import app.AppSettings;
	
	import component.ScrollSprite;
	
	import feathers.controls.ImageLoader;
	import feathers.controls.ToggleButton;
	
	import pages.PageNavigator;
	import pages.data.StudyInfo;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.text.TextFormat;
	
	public class HeaderContainer extends Sprite
	{
		public static const TAP_MENU:String = "tapMenu";
		public static const SET_MENU:String = "setContent";
		public static const SET_TITLES:String = "setTitles";
		
		private var bg:Image;
		private var bgOriginalHeight:int;
		private var icon:ImageLoader;
		private var textTitle:TextField;
		
		private var btnExpansion:Button;
		private var indicator:Image;
		private var subIndicator:Image;
		
		private var timelineExpansion:TimelineLite = null;
		private var _isExpanded:Boolean = false;
		private var btnMenu:Button;
		private var vecBtnNavigator:Vector.<Button>;
		
		private var menu:ScrollSprite = null;
		
		private const NAVIGATOR_MENU_OFFSET:int = 40;
		
		public function getOriginHeight():int
		{
			return bgOriginalHeight;
		}
		
		public function hideBtnExpansion():void
		{
			btnExpansion.touchable = false;
			TweenLite.to(btnExpansion, 0.5, {alpha:0, scaleX:0.2, scaleY:0.2});
		}
		
		public function showBtnExpansion():void
		{
			btnExpansion.touchable = true;
			TweenLite.to(btnExpansion, 0.5, {alpha:1, scaleX:1, scaleY:1});
		}
		
		public function HeaderContainer()
		{
			super();
			drawUI();
			drawNavigator(getTitles());
			this.addEventListener(SET_MENU, onSetmenu);
			this.addEventListener(SET_TITLES, onSetTitles);
		}
		
		//AppSettings.PAGE_CATEGORY_TITLES 초기화가 필요함
		public function getTitles():Array
		{
			var curTitles:Array = new Array();
			var len:int = AppSettings.PAGE_CATEGORY_TITLES[StudyInfo.currentChapter].length;			
			for (var i:int = 0; i < len; i++)
			{
				var title:String = AppSettings.PAGE_CATEGORY_TITLES[StudyInfo.currentChapter][i];
				
				if (0 == curTitles.length)
					curTitles.push(title);
				
				if (curTitles[curTitles.length - 1] != title)
					curTitles.push(title);
			}
			return curTitles;
		}
		
		public function onSetmenu(event:*, data:Object):void
		{
			menu = data as ScrollSprite;
		}
		
		public function onSetTitles(event:*):void
		{
			timelineExpansion = null;
			drawNavigator(getTitles());
		}
		
		private function drawUI():void
		{
			bg = new Image(Root.assets.getTexture("header/topbar"));			
			this.addChild(bg);
			bgOriginalHeight = bg.height;
			
			textTitle = new TextField(480, 60, AppSettings.APPBAR_NAME + " " + StudyInfo.currentChapter + "과");
			textTitle.format = new TextFormat("NotoSansCJKkr-Medium", 30, 0xf0f0f0);
			this.addChild(textTitle);
			textTitle.x = 80;
			textTitle.y = 10;
			
			btnMenu = new Button(Root.assets.getTexture("header/btnList"));
			this.addChild(btnMenu);
			btnMenu.x = 0;
			btnMenu.y = 0;
			btnMenu.addEventListener(Event.TRIGGERED, onTriggerdBtnMenu);
						
			btnExpansion = new Button(Root.assets.getTexture("header/btnToggle"));
			this.addChild(btnExpansion);
			btnExpansion.pivotX = btnExpansion.width >> 1;
			btnExpansion.pivotY = btnExpansion.height >> 1;
			btnExpansion.x = AppSettings.width - btnExpansion.width + btnExpansion.pivotX;
			btnExpansion.y = btnExpansion.pivotY;
			btnExpansion.addEventListener(Event.TRIGGERED, onTriggerdBtnExpansion);
		}
		
		private function drawNavigator(titles:Array):void
		{
			textTitle.text = AppSettings.APPBAR_NAME + " " + StudyInfo.currentChapter + "과";
			
			if (titles.length < 2 && titles.length > 5)
			{
				var err:Error= new Error("drawNavigator: 지원하는 AppBar 버튼은 2 이상 5 이하입니다.");
				trace(err.toString());  
			}
			else
			{
				if (null != indicator) this.removeChild(indicator);
				if (null != subIndicator) this.removeChild(subIndicator);
				
				var width:int = AppSettings.width / titles.length;
				
				indicator = new Image(Root.assets.getTexture("header/indicator"));
				indicator.width = width;				
				indicator.x = 0;
				indicator.y = bg.height - indicator.height;
				indicator.alpha = 0.5;
				this.addChild(indicator);
				
				var cnt:int = 0;
				for (cnt = 0; cnt < titles.length; cnt++)
					if (titles[cnt] != titles[0])
						break;
				
				subIndicator = new Image(Root.assets.getTexture("header/indicator"));
				subIndicator.width = int(width/cnt);
				subIndicator.x = 0;
				subIndicator.y = bg.height - subIndicator.height;
				subIndicator.alpha = 0.5;
				this.addChild(subIndicator);
				
				var i:int;
				if (null != vecBtnNavigator)
				{
					for (i = 0; i < vecBtnNavigator.length; i++)
						this.removeChild(vecBtnNavigator[i]);
				}
				
				vecBtnNavigator = new Vector.<Button>();
				for (i = 0; i < titles.length; i++)
				{
					var btnNavigator:Button = new Button(Root.assets.getTexture("alphaBox"));
					btnNavigator.textFormat = new TextFormat("NotoSansCJKkr-Medium", 30, 0xf0f0f0);
					btnNavigator.text = titles[i];
					btnNavigator.alpha = 0;
					btnNavigator.width = width;
					btnNavigator.height = 80;
					btnNavigator.x = i * width;
					btnNavigator.y = 0;
					btnNavigator.touchable = false;
					btnNavigator.visible = false;
					this.addChild(btnNavigator);
					vecBtnNavigator.push(btnNavigator);
				}
			}
		}
		
		private function setTimelineExpasion():void
		{
			timelineExpansion = new TimelineLite({onComplete:onCopmleteExpansion, onReverseComplete:onReverseExpansion});
			timelineExpansion.add([
				new TweenLite(btnExpansion, 0.3, {rotation:Math.PI/2}),
				new TweenLite(bg, 0.3, {scaleY:2}),
				new TweenLite(subIndicator, 0.3, {y:bgOriginalHeight*2 - subIndicator.height}),
				new TweenLite(indicator, 0.3, {y:bgOriginalHeight*2 - indicator.height})], 0, "step1");

			for (var i:int = 0; i < vecBtnNavigator.length; i++)
			{
				timelineExpansion.add(new TweenLite(vecBtnNavigator[i], 0.3, {alpha:1, y: bg.height - indicator.height}), 0.15, "step2");
			}
		}
		
		private function onCopmleteExpansion():void
		{
			for (var i:int = 0; i < vecBtnNavigator.length; i++)
			{
				vecBtnNavigator[i].touchable = true;
				vecBtnNavigator[i].addEventListener(Event.TRIGGERED, onTriggerdBtnNavigator);
			}
			menu.resizeScroll(AppSettings.width, AppSettings.height - bgOriginalHeight*2);
		}
		
		private function onReverseExpansion():void
		{
			for (var i:int = 0; i < vecBtnNavigator.length; i++)
			{	
				vecBtnNavigator[i].visible = false;
				vecBtnNavigator[i].y = 0;
			}
		}
		
		private function onTriggerdBtnExpansion():void
		{
			var i:int;
			if (this.isExpanded)
			{
				this.isExpanded = false;
				for (i = 0; i < vecBtnNavigator.length; i++)
				{
					vecBtnNavigator[i].touchable = false;
				}
				menu.resizeScroll(AppSettings.width, AppSettings.height - bgOriginalHeight);
				
				if (null == timelineExpansion) setTimelineExpasion();
				timelineExpansion.reverse();
			}
			else
			{
				this.isExpanded = true;
				for (i = 0; i < vecBtnNavigator.length; i++)
				{
					vecBtnNavigator[i].y = bg.height - indicator.height - NAVIGATOR_MENU_OFFSET;
					vecBtnNavigator[i].visible = true;
				}
				if (null == timelineExpansion) setTimelineExpasion();
				timelineExpansion.play();
			}
			if (menu.visible)
			{
				TweenLite.to(menu, 0.3, { y: (isExpanded) ? bgOriginalHeight*2 : bgOriginalHeight });
			}
			
			AppSettings.pageNavigator.dispatchEventWith(PageNavigator.ON_EXPANDING_HEAD, false, this.isExpanded);
		}
		
		//AppBar를 없앨 때 사용
		public function expansion():void
		{
			if (!this.isExpanded)
			{
				this.isExpanded = true;
				
				for (var i:int = 0; i < vecBtnNavigator.length; i++)
				{
					vecBtnNavigator[i].y = bg.height - indicator.height - NAVIGATOR_MENU_OFFSET;
					vecBtnNavigator[i].visible = true;
				}
				if (null == timelineExpansion) setTimelineExpasion();
				timelineExpansion.play();
			}
		}
		
		public function contraction():void
		{
			if (this.isExpanded)
			{
				this.isExpanded = false;
				
				for (var i:int = 0; i < vecBtnNavigator.length; i++)
				{
					vecBtnNavigator[i].touchable = false;
				}
				menu.resizeScroll(AppSettings.width, AppSettings.height - bgOriginalHeight);
				
				if (null == timelineExpansion) setTimelineExpasion();
				timelineExpansion.reverse();
			}
		}
		
		public function setIndicator(title:String):void
		{
			var len:int = vecBtnNavigator.length;
			for (var i:int = 0; i < len; i++)
			{
				if (title == vecBtnNavigator[i].text)
				{
					var cnt:int = 0;
					var curTitles:Array = AppSettings.PAGE_CATEGORY_TITLES[StudyInfo.currentChapter];
					for (var j:int = 0; j < curTitles.length; j++)
					{
						if (curTitles[j] == title)
							cnt++;
					}
					
					for (var k:int = 0; k < curTitles.length; k++)
					{
						if (curTitles[k] == title)
							break;
					}
					
					var taretSubIndicator:int = StudyInfo.currentPage - k;
					var w:int = int(vecBtnNavigator[i].width/cnt);
					TweenLite.to(indicator, 0.3, {x:vecBtnNavigator[i].x});
					TweenLite.to(subIndicator, 0.3, {x:vecBtnNavigator[i].x + w*taretSubIndicator, width:w});
					break;
				}
			}
		}

		private function onTriggerdBtnMenu():void
		{
			this.dispatchEventWith(TAP_MENU);
		}
		
		private function onTriggerdBtnNavigator(event:Event):void
		{	
			var btn:Button = event.target as Button;
			var curTitles:Array = AppSettings.PAGE_CATEGORY_TITLES[StudyInfo.currentChapter];
			for (var k:int = 0; k < curTitles.length; k++)
			{
				if (curTitles[k] == btn.text)
					break;
			}
			StudyInfo.currentPage = k;
			setIndicator(btn.text);
			AppSettings.pageNavigator.dispatchEventWith(PageNavigator.ON_TOUCH_HEAD_BTN, false, btn.text);
		}
		
		public function get isExpanded():Boolean
		{
			return _isExpanded;
		}
		
		public function set isExpanded(b:Boolean):void
		{
			_isExpanded = b;
		}		
	}
}