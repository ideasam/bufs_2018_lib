package pages.type
{
	import com.greensock.TweenMax;
	
	import flash.net.URLRequest;
	
	import app.AppSettings;
	
	import component.ScrollSprite;
	import component.SoundManager;
	import component.SpeechBalloon;
	
	import feathers.controls.ImageLoader;
	import feathers.controls.ToggleButton;
	import feathers.events.FeathersEventType;
	
	import pages.data.StudyInfo;
	
	import starling.display.Image;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import com.greensock.TimelineLite;
	
	public class ConversationPage extends BasePage
	{
		private var currentBallonTimeline:TimelineLite = null; 
		
		private var togglePlay:ToggleButton;
		private var bottomBar:Image;
		
		private var vecSplit:Vector.<Image> = new Vector.<Image>;
		private var vecBalloon:Vector.<SpeechBalloon> = new Vector.<SpeechBalloon>;
		private var lenVecBalloon:int;
		
		private var chapterDataOrigin:Object;
		private var chapterDataTrans:Object;
		
		private var currentSeq:int = 0;		
		private var imgLoader:ImageLoader = null;
		
		public function ConversationPage()
		{			
			super();
		}
		
		override public function loadContent():void
		{
			imgLoader = new ImageLoader();
			imgLoader.source = new URLRequest("data/img/chapter"+idxChapter+"/page"+idxPage+".png").url;
			imgLoader.addEventListener(Event.COMPLETE, onTitleImageLoadComplete);
			scrollCont.addChild(imgLoader);
		}
		
		override public function unloadContent():void
		{
			removeChild(imgLoader);
			imgLoader.source = null;
			this.removeEventListener(ScrollSprite.ON_RESIZE_OFFSET, onResizeOffset);
			super.unloadContent();
		}
		
		public function onTitleImageLoadComplete( evt:Event ):void
		{
			draw();
			
			this.addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameContConv);
			this.addEventListener(FeathersEventType.SCROLL_START, onScroll);
			this.addEventListener(ScrollSprite.ON_RESIZE_OFFSET, onResizeOffset);
			
			isLoaded = true;
		}
		
		override public function draw():void
		{
			super.draw();
			
			drawConversation();
			
			bottomBar = new Image(Root.assets.getTexture("bottomBar"));
			bottomBar.y = AppSettings.height - bottomBar.height
			this.addChild(bottomBar);
			
			togglePlay = new ToggleButton();
			togglePlay.defaultSkin = new Image(Root.assets.getTexture("textConversation/btnPlayN"));
			togglePlay.defaultSelectedSkin = new Image(Root.assets.getTexture("textConversation/btnPlayS"));
			
			this.addChild(togglePlay);
			togglePlay.validate();
			togglePlay.x = (AppSettings.width - togglePlay.width) >>1;
			togglePlay.y = AppSettings.height - togglePlay.height;
			
			addEventListener(Event.CHANGE, onChangeToggleTrans);
			addEventListener(Event.SELECT, onSelectBallon);
			togglePlay.addEventListener(Event.TRIGGERED, onTriggredBtnPlay);
		}
		
		//Speech Ballon Mode: -1 (Splitter), 0 (유저), 1 (상대), 2 (정보) 
		private function drawConversation():void
		{
			this.height = AppSettings.height;			
			this.y = 0;
			
			var jsonOrigin:Object = Root.assets.getObject("dialogOrigin");
			var jsonTrans:Object = Root.assets.getObject("dialogTrans");
			
			var strChapter:String = "chapter" + (StudyInfo.currentChapter);
			chapterDataOrigin = jsonOrigin[strChapter];
			chapterDataTrans = jsonTrans[strChapter];
			
			var ballonStartPos:int = imgLoader.originalSourceHeight;
			var mPaddingTop:int = 20;
			var posY_N:int = 0;
			var posY_S:int = 0;
			
			var len:int = chapterDataOrigin.length;
			var cnt:int = 0;
			var cntSplit:int = 0;
			
			var tDataOri:Object = null;
			var tDataTrs:Object = null;
			
			for(var i:int = 0; i <len;++i)
			{
				tDataOri = chapterDataOrigin[i];
				tDataTrs = chapterDataTrans[i];
				
				var balloon:SpeechBalloon = null;
				
				if(tDataOri.page == (idxPage))
				{
					if ("NULL" == tDataOri.spk)
					{
						//Splitter 만들기
						balloon = new SpeechBalloon(idxChapter, idxPage, "", "", -1, "");
						
						if(cnt > 0)
						{
							posY_N = vecBalloon[cnt-1].posY_N + vecBalloon[cnt-1].heightN;
							posY_S = vecBalloon[cnt-1].posY_S + vecBalloon[cnt-1].heightS;
						}
						balloon.y = mPaddingTop + posY_N + ballonStartPos;
						
						balloon.posY_N = mPaddingTop + posY_N;
						balloon.posY_S = mPaddingTop + posY_S;
						
						vecBalloon.push(balloon);
						scrollCont.addChild(balloon);
					}
					else 
					{
						var strTrs:String = tDataTrs.dlg;
						var mode:int = (AppSettings.CONV_INOF_STR == tDataOri.spk) ? 2 : (tDataOri.spk == AppSettings.CONV_USER_STR) ? 0 : 1;
						balloon = new SpeechBalloon(idxChapter, idxPage, tDataOri.dlg, strTrs, mode, tDataOri.sound, AppSettings.IS_IMAGE_TEXT);
						
						//x 결정하기.
						balloon.x = (AppSettings.width - balloon.width) >> 1;
						
						//y 결정하기.
						if(cnt > 0)
						{
							posY_N = vecBalloon[cnt-1].posY_N + vecBalloon[cnt-1].heightN;
							posY_S = vecBalloon[cnt-1].posY_S + vecBalloon[cnt-1].heightS;
						}
						balloon.y = mPaddingTop + posY_N + ballonStartPos;						
						balloon.posY_N = mPaddingTop + posY_N;
						balloon.posY_S = mPaddingTop + posY_S;
						
						balloon.seqence = cnt;
						vecBalloon.push(balloon);
						scrollCont.addChild(balloon);
					}
					cnt++;
				}
			}
			
			var tempBalloon:SpeechBalloon = new SpeechBalloon(idxChapter, idxPage, "\n\n\n\n", "");
			tempBalloon.y = mPaddingTop + vecBalloon[vecBalloon.length-1].y + vecBalloon[vecBalloon.length-1].getVisibleHeight();
			tempBalloon.visible = false;
			tempBalloon.touchable = false;
			vecBalloon.push(tempBalloon);
			scrollCont.addChild(tempBalloon);
			
			lenVecBalloon = vecBalloon.length;
		}
		
		private function onResizeOffset(event:Event, offset:int):void
		{
			trace("ConversationPage - onResizeOffset: " + offset);
			TweenMax.to(togglePlay, 0.3, {y: togglePlay.y - offset});
			TweenMax.to(bottomBar, 0.3, {y: bottomBar.y - offset});
		}
		
		private function onSelectBallon(event:Event, seq:int):void
		{
			scrollCont.hasElasticEdges = true;
			togglePlay.isSelected = false;
			currentSeq = seq;
			setEffectCurrentBalloon();
			onSelectConvItem(event, seq, onCompletePlay);
			
			function onCompletePlay():void
			{
				setEffectCurrentBalloon(true);
			}
		}
		
		private function onTriggredBtnPlay():void
		{
			if(togglePlay.isSelected == true)
			{
				scrollCont.hasElasticEdges = true;
				SoundManager.stop();
				setEffectCurrentBalloon(true);
				return;
			}
			var maxLen:int = vecBalloon.length-1;
			
			onSelectConvItem(null, currentSeq, onCompletePlay);
			scrollCont.scrollToPosition(0, vecBalloon[currentSeq].y - 20,0.2);
			scrollCont.hasElasticEdges = false;
			
			//setEffect
			setEffectCurrentBalloon();
			
			function onCompletePlay():void
			{
				currentSeq++;
				while (-1 == vecBalloon[currentSeq].mode)
					currentSeq++;
				
				setEffectCurrentBalloon();
				scrollCont.validate();
				
				if(maxLen > currentSeq)
				{
					scrollCont.scrollToPosition(0, vecBalloon[currentSeq].y - 20,0.2);
					onSelectConvItem(null, currentSeq, onCompletePlay);
				}
				else 
				{
					currentSeq = 0;
					scrollCont.hasElasticEdges = true;
					togglePlay.isSelected = false;
					setEffectCurrentBalloon(true);
				}
			}
		}
		
		override public function deactivate():void
		{
			super.deactivate();
			
			SoundManager.stop();
			currentSeq = 0;
			scrollCont.hasElasticEdges = true;
			togglePlay.isSelected = false;
			setEffectCurrentBalloon(true);
		}
		
		private function setEffectCurrentBalloon(removeAll:Boolean = false):void
		{
			for(var i:int=0; i<vecBalloon.length; ++i)
			{
				if(removeAll)
				{
					vecBalloon[i].alpha = 1.0;
				}
				else if(i==currentSeq)
				{
					vecBalloon[i].alpha = 1.0;
				}
				else
				{
					vecBalloon[i].alpha = 0.4;
				}
			}
		}
		
		private function onSelectConvItem(event:Event, seq:int, onCompleteSoundPlay:Function):void
		{
			currentSeq = seq;
						
			SoundManager.stop();
			
			var path:String = "data/sound/chapter" + idxChapter + "/" + vecBalloon[seq].sound + ".mp3";
			SoundManager.play(path, onCompleteSoundPlay);
		}
		
		private function expandSpeechBalloon(idx:int):void
		{
			currentBallonTimeline = vecBalloon[idx].expand();
			scrollCont.readjustLayout()
		}
		
		private function collapseSpeechBalloon(idx:int):void
		{
			currentBallonTimeline = vecBalloon[idx].callapse();
		}
				
		private function onCompleteTweenBallon():void
		{
			scrollCont.readjustLayout();
		}
		
		private function onEnterFrameContConv():void
		{
			if(currentBallonTimeline && currentBallonTimeline.isActive())
			{
				for(var i:int= 1; i<lenVecBalloon; ++i)
				{
					vecBalloon[i].y = int(vecBalloon[i-1].y + vecBalloon[i-1].getVisibleHeight() + 20);					
				}
			}
		}		
		
		private function onScroll():void
		{
			scrollCont.readjustLayout();
		}
		
		private function onChangeToggleTrans(event:Event, info:Object):void
		{
			var idx:int = info[0];
			var isSelected:Boolean = info[1];
			
			if(isSelected)
			{
				expandSpeechBalloon(idx);
			}
			else
			{
				collapseSpeechBalloon(idx);
			}
		}		
		
		override public function dispose():void
		{
			SoundManager.stop();
			this.removeEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameContConv);
			
			super.dispose();
		}
	}
}