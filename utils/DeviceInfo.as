package utils
{
	import flash.desktop.NativeApplication;

	public class DeviceInfo
	{
		//개발용 버전
		private static var _version:String = null;
		
		//마켓용 버전
		private static var _versionLabel:String = null;
		
		private static var _appId:String = null;

		public static function get version():String
		{
			if(_version == null)
			{
				var descriptor:XML = NativeApplication.nativeApplication.applicationDescriptor;
				var ns:Namespace = descriptor.namespace();
				_version = descriptor.ns::versionNumber;
			}
			
			return _version;
		}
		
		public static function get versionLabel():String
		{
			if(_versionLabel == null)
			{
				var descriptor:XML = NativeApplication.nativeApplication.applicationDescriptor;
				var ns:Namespace = descriptor.namespace();
				_versionLabel = descriptor.ns::versionLabel;
			}
			
			return _versionLabel;
		}
		
		public static function get appId():String
		{
			if(_appId == null)
			{
				var descriptor:XML = NativeApplication.nativeApplication.applicationDescriptor;
				var ns:Namespace = descriptor.namespace();
				_appId = descriptor.ns::id;
			}
			
			return _appId;
		}
	}
}