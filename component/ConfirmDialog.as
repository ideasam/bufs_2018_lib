package component
{
	import flash.geom.Rectangle;
	
	import feathers.controls.Button;
	import feathers.controls.Label;
	import feathers.core.PopUpManager;
	import feathers.layout.HorizontalAlign;
	import feathers.skins.ImageSkin;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextFormat;
	
	import theme.CustomTheme;
	
	public class ConfirmDialog extends Sprite
	{
		private var label:Label;
		private var _btnConfirm	:Button;
		private var _btnCancel:Button;
		
		private var _yesCallback:Function;
		
		public function ConfirmDialog(text:String, yesCallback:Function)
		{
			super();
			
			_yesCallback = yesCallback;
			drawUI(text);
		}
		
		private function drawUI(text:String):void
		{
			var _bg:ImageSkin = new ImageSkin(Root.assets.getTexture("StBgResult"));
			_bg.scale9Grid = new Rectangle(20,20,10,10); 
			_bg.width = 480;
			_bg.height = 120+150;
			//_bg.color = 0xd0d0d0;
			_bg.alpha = 0.8;
			this.addChild(_bg);
			
			label = new Label();
			label.x = 15;
			label.y = 20;
			label.fontStyles = new TextFormat(CustomTheme.FONT_NAME, 24, 0x000000, HorizontalAlign.CENTER, "center");
			label.width = 450;
			label.height = 50+150;
			label.touchable = false;
			this.addChild( label );
			
			label.text = text;
			
			var btnCancelSkin:Image = new ImageSkin(Root.assets.getTexture("StBgResult"));
			btnCancelSkin.color = 0xa0a0a0;
			btnCancelSkin.scale9Grid = new Rectangle(20,20,10,10);
			
			var btnConfirmSkin:Image = new ImageSkin(Root.assets.getTexture("StBgResult"));
			btnConfirmSkin.color = 0xd0a0a0;
			btnConfirmSkin.scale9Grid = new Rectangle(20,20,10,10);
			
			_btnCancel = new Button;
			_btnCancel.defaultSkin = btnCancelSkin;
			_btnCancel.labelOffsetY = -4;
			_btnCancel.label = "취 소";
			_btnCancel.width = 180;
			_btnCancel.minTouchWidth = 150;
			_btnCancel.minTouchHeight = 100;
			this.addChild(_btnCancel);
			
			_btnCancel.fontStyles = new TextFormat(CustomTheme.FONT_NAME, 24, 0xffffff, HorizontalAlign.CENTER, "center");
			
			_btnCancel.x = 40;
			_btnCancel.y = 200;
			
			_btnCancel.addEventListener(Event.TRIGGERED, onTriggeredCancel);
			
			_btnConfirm = new Button;
			_btnConfirm.defaultSkin = btnConfirmSkin;
			_btnConfirm.labelOffsetY = -4;
			_btnConfirm.label = "확 인";
			_btnConfirm.width = 180;
			_btnConfirm.minTouchWidth = 150;
			_btnConfirm.minTouchHeight = 100;
			this.addChild(_btnConfirm);
			
			_btnConfirm.fontStyles = new TextFormat(CustomTheme.FONT_NAME, 24, 0xffffff, HorizontalAlign.CENTER, "center");
			_btnConfirm.x = 40+180+40//(_bg.width - _btnConfirm.width) >> 1;
			_btnConfirm.y = 200;
			
			_btnConfirm.addEventListener(Event.TRIGGERED, onTriggeredConfirm);
		} 
		
		private function onTriggeredConfirm():void
		{
			_yesCallback();
			PopUpManager.removePopUp(this);
		}
		private function onTriggeredCancel():void
		{
			PopUpManager.removePopUp(this);
		}
	}
}