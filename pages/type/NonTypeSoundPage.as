package pages.type
{
	import com.greensock.TweenMax;
	
	import flash.net.URLRequest;
	
	import feathers.events.FeathersEventType;
	
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;

	public class NonTypeSoundPage extends NonTypePage
	{
		public function NonTypeSoundPage()
		{
			hasPlayerDock = true;
			super();
		}
		
		override public function loadContent():void
		{
			//mPlayerDock.setSound(new URLRequest("data/sound/ch"+idxChapter +"_"+idxPage+".mp3").url);
			
			//mPlayerDock.setSource(new URLRequest("data/sound/ch"+idxChapter +"_"+idxPage+".mp3").url);
			
			scrollCont.addEventListener(FeathersEventType.SCROLL_START, onScrollStart);
			scrollCont.addEventListener(TouchEvent.TOUCH, onTouchScrollCont);
			
			super.loadContent();
		}
		
		override public function activate():void
		{
			super.activate();
			
			mPlayerDock.setSound(new URLRequest("data/sound/ch"+idxChapter +"_"+idxPage+".mp3").url);
		}
		
		private function onScrollStart():void
		{
			TweenMax.to(mPlayerDock, 0.2, {alpha:0});
		}
		
		private function onTouchScrollCont(touchEvent:TouchEvent):void
		{
			var touch:Touch = touchEvent.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if(touch.tapCount == 1 && !scrollCont.isScrolling)
				{
					if(mPlayerDock.alpha == 0)
					{
						TweenMax.to(mPlayerDock, 0.2, {alpha:1});
						mPlayerDock.touchable = true;
					}
					else
					{
						TweenMax.to(mPlayerDock, 0.2, {alpha:0});
						mPlayerDock.touchable = false;
					}
				}
			}
		}
		
		override public function unloadContent():void
		{
			super.unloadContent();
		}
	}
}