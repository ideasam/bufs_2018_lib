package pages.type
{
	import com.greensock.TweenMax;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.DataLoader;
	import com.greensock.loading.LoaderMax;
	
	import flash.events.KeyboardEvent;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.ui.Keyboard;
	
	import app.AppSettings;
	
	import component.RectItem;
	
	import feathers.controls.Button;
	import feathers.controls.DecelerationRate;
	import feathers.controls.ImageLoader;
	import feathers.controls.ScrollContainer;
	import feathers.events.FeathersEventType;
	
	import pages.PageContent;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.filters.GlowFilter;
	
	public class GrammarPage extends BasePage
	{
		private var _loaderQueue:LoaderMax = null;
		private var _dataLoaderJsonRect:DataLoader = null;
		
		private var imgLoader:ImageLoader = null;
		private var imgSubLoader:ImageLoader = null;
		
		private var scrollCont:ScrollContainer;
		
		private var btnSubClose:Button;
		
		private var oPosEndInd:int = 0;
		
		private var prevScrollPos:int=0;
		
		public function GrammarPage()
		{
			this.hasPlayerDock = true;
			this.hasLeftShadow = true;
			
			super();
		}
		
		override public function draw():void
		{
			scrollCont = new ScrollContainer();
			scrollCont.width = AppSettings.width;
			scrollCont.height = AppSettings.height;
			this.addChild(scrollCont);
			
			scrollCont.decelerationRate = DecelerationRate.FAST;
			scrollCont.minimumDragDistance = 0.2;
			
			imgLoader = new ImageLoader();
			scrollCont.addChild(imgLoader);
			
			quadBg = new Quad(AppSettings.width, AppSettings.height, 0x000000);
//			quadBg.alignPivot();
//			quadBg.x = AppSettings.width >>1;
//			quadBg.y = AppSettings.height >>1;
			quadBg.touchable =false;
			quadBg.visible = false;
			//quadBg.alpha = 0.08;
			this.addChild(quadBg);
			
			subPageCont = new Sprite();
			subPageCont.visible = false;
			subPageCont.touchable = false;
			
			imgSubLoader = new ImageLoader();
			subPageCont.addChild(imgSubLoader);
			
			btnSubClose = new Button();
			btnSubClose.x = AppSettings.width - 120;
			btnSubClose.y = 10;
			btnSubClose.defaultSkin = new Image(Root.assets.getTexture("btnExitN"));
			this.addChild(btnSubClose);
			btnSubClose.visible = false;
			
			btnSubClose.addEventListener(Event.TRIGGERED, onCloseSubPage);
			
			scrollCont.addChild(subPageCont);
			super.draw();
			
			mPlayerDock.visible = false;
		}
		
		
		override public function loadContent():void
		{
			imgLoader.addEventListener(Event.COMPLETE, onCompleteLoad);
			
			if(!imgLoader.source) imgLoader.source = new URLRequest("data/img/chapter"+idxChapter+"/page"+idxPage+".png").url;
			
			if(!_dataLoaderJsonRect)
			{
				_dataLoaderJsonRect = new DataLoader("data/rect/ch"+idxChapter+"_"+idxPage+".txt", {name:TAG_RECT});
				_loaderQueue = new LoaderMax({name:"mainQueue", onComplete:completeHandler});
				_loaderQueue.append(_dataLoaderJsonRect);
				_loaderQueue.load();
			}
			super.loadContent();
		}
		
		private var prevTouchIdx:int = -1;
		private var vecSoundItem:Vector.<RectItem> = new Vector.<RectItem>;
		
		private function completeHandler(event:LoaderEvent):void 
		{
			var strJsonRect:String = LoaderMax.getContent(TAG_RECT);
			
			if(!strJsonRect) return;
			
			scrollCont.addChildAt(rectEffect,0);
			
			var jsData:Object = JSON.parse(strJsonRect);
			
			var len:int = jsData.rects.length;
			for(var i:int=0; i<len;++i)
			{
				vecSoundItem.push(new RectItem(new Rectangle(jsData.rects[i].x, jsData.rects[i].y, jsData.rects[i].width,jsData.rects[i].height)));
			}
			
			scrollCont.addEventListener(TouchEvent.TOUCH, onTouchCont);
		}
		
		private function onTouchCont(event:TouchEvent):void
		{
			if(scrollCont.isScrolling || PageContent.isContentScrolling) return;
			
			var touch:Touch = event.getTouch(imgLoader);
			var touchSub:Touch = event.getTouch(imgSubLoader, TouchPhase.ENDED);
			
			if(touch)
			{
				switch(touch.phase)
				{
					case TouchPhase.BEGAN:
						prevTouchIdx = getTouchAreaIdx();
						break;
					
					case TouchPhase.ENDED:
						
						var curTouchIdx:int = getTouchAreaIdx();
						if(prevTouchIdx > -1 && curTouchIdx>-1)
						{
							rectEffect.filter = null;
							
							rectEffect.x = vecSoundItem[curTouchIdx].bounds.x;
							rectEffect.y = vecSoundItem[curTouchIdx].bounds.y;
							rectEffect.width = vecSoundItem[curTouchIdx].bounds.width;
							rectEffect.height = vecSoundItem[curTouchIdx].bounds.height;
							
							rectEffect.filter = new GlowFilter(0x29CDC8,0.8,8);
							rectEffect.alpha = 0;
							rectEffect.visible = true;
							
							//showSubPage(curTouchIdx);
							
							//TweenMax.to(rectEffect, 0.2, {alpha:1, repeat:1, onComplete:function():void{
							TweenMax.to(rectEffect, 0.5, {alpha:1, onComplete:function():void{
								showSubPage(curTouchIdx);
							}});
							//
						}
						
						prevTouchIdx = -1;
						break;
				}
			}
			
			if(touchSub)
			{
				if(isSubLoadComplete && !scrollCont.isScrolling)
				{
					if(mPlayerDock.alpha == 0) TweenMax.to(mPlayerDock, 0.2, {alpha:1});
					else TweenMax.to(mPlayerDock, 0.2, {alpha:0});
				}
			}
			
			function getTouchAreaIdx():int
			{
				var len:int =vecSoundItem.length;
				for (var i:int=0; i<len; ++i)
				{
					if(vecSoundItem[i].bounds.containsPoint(touch.getLocation(imgLoader)))
					{
						return i;
					}
				}
				return -1;
			}
		}
		
		private var subPageCont:Sprite = null;
		private var quadBg:Quad;
		private var isSubLoadComplete:Boolean = false;
		
		private function showSubPage(idx:int):void
		{
			dispatchEventWith(BasePage.TOGGLE_BTN_ARROW, true, false);
			
			if(leftShadow) leftShadow.visible = false;
			
			prevScrollPos = scrollCont.verticalScrollPosition;
			
			scrollCont.verticalScrollPosition = 0;
			
			imgSubLoader.addEventListener(Event.COMPLETE, onCompleteLoadSub);
			//scrollCont.removeEventListener(TouchEvent.TOUCH, onTouchCont);
			quadBg.alpha = 0;
			quadBg.visible = true;
			//quadBg.scaleY = 0.2;
			
			TweenMax.to(quadBg, 0.3, {scaleY:1, alpha:0.1});
			
			btnSubClose.visible = true;
			
			subPageCont.visible = true;
			subPageCont.touchable = true;
			
			imgLoader.removeFromParent();
			imgLoader.visible = false;
			imgLoader.touchable = false;
			
			imgSubLoader.source = new URLRequest("data/img/chapter"+idxChapter+"/grammar/g"+idx+".png").url;
			imgSubLoader.touchable =true;
			
			mPlayerDock.setSound(new URLRequest("data/sound/grammar/ch"+idxChapter +"_g"+idx+".mp3").url);
			
			rectEffect.visible = false;
			endIndicator.visible = false;
			//리소스 없어서 주석
			mPlayerDock.visible = true;
			
			function onCompleteLoadSub():void
			{
				
				imgSubLoader.removeEventListener(Event.COMPLETE, onCompleteLoadSub);
				imgSubLoader.validate();
								
				imgSubLoader.alpha = 0;
				endIndicator.y = imgSubLoader.y + imgSubLoader.height
				
				scrollCont.readjustLayout();
				
				Starling.current.nativeStage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
				scrollCont.addEventListener(FeathersEventType.SCROLL_START, onScrollStart);
				
				isSubLoadComplete = true;
				
				TweenMax.to(imgSubLoader, 0.25, {alpha:1, scaleY:1});
			}
		}
		
		protected function onKeyDown(event:KeyboardEvent):void
		{
			if(event.isDefaultPrevented())
			{
				//someone else already handled this one
				return;
			}
			if(event.keyCode == Keyboard.BACK || Keyboard.BACKSPACE)
			{
				Starling.current.nativeStage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
				event.preventDefault();
				
				onCloseSubPage();
			}
		}
		
		private function onScrollStart():void
		{
			TweenMax.to(mPlayerDock, 0.2, {alpha:0});
		}
		
		private function onCloseSubPage():void
		{
			isSubLoadComplete = false;
			
			scrollCont.removeEventListener(FeathersEventType.SCROLL_START, onScrollStart);
			
			dispatchEventWith(BasePage.TOGGLE_BTN_ARROW, true, true);
			
			mPlayerDock.stop();
			endIndicator.visible = true;
			
			if(leftShadow) leftShadow.visible = true;
			
			//quadBg.visible = false;
			
			TweenMax.to(quadBg, 0.2, {alpha:0});
			//TweenMax.to(imgSubLoader, 0.2, {alpha:0});
			
			btnSubClose.visible = false;
			
			subPageCont.visible = false;
			subPageCont.touchable = false;
			
			scrollCont.addChildAt(imgLoader,1);
			
			imgLoader.visible = true;
			imgLoader.touchable = true;
			
			imgLoader.alpha = 0;
			TweenMax.to(imgLoader, 0.2, {alpha:1});
			
			imgSubLoader.source = null;
			imgSubLoader.touchable =false;
			imgSubLoader.validate();
			
			mPlayerDock.visible = false;
			mPlayerDock.alpha = 1;
			
			scrollCont.verticalScrollPosition = prevScrollPos;
			
			endIndicator.y = oPosEndInd;
			scrollCont.readjustLayout();
		}
		
		private function onCompleteLoad():void
		{
			imgLoader.removeEventListener(Event.COMPLETE, onCompleteLoad);
			imgLoader.validate();
			
			imgLoader.visible = true;
			
			oPosEndInd = this.endIndicator.y = imgLoader.y + imgLoader.height+20;
			scrollCont.addChild(endIndicator);
		}
		
		override public function unloadContent():void
		{
			super.unloadContent();
			
			onCloseSubPage();
			
			imgLoader.source = null;
			imgSubLoader.source = null;
		}
	}
}