package pages.type
{
	import flash.net.URLRequest;
	
	import app.AppSettings;
	
	import feathers.controls.ImageLoader;
	import feathers.controls.ScrollContainer;

	public class Sentence2Page extends BasePage
	{
		private var imgLoader:ImageLoader = null;
		private var scrollCont:ScrollContainer;
		
		public function Sentence2Page()
		{
			super();
		}
		
		override public function draw():void
		{
			scrollCont = new ScrollContainer();
			scrollCont.minimumDragDistance = 0.2;
			scrollCont.width = AppSettings.width;
			scrollCont.height = AppSettings.height;
			this.addChild(scrollCont);
			
			imgLoader = new ImageLoader();
			scrollCont.addChild(imgLoader);
			
			super.draw();
		}
		
		override public function loadContent():void
		{
			imgLoader.source = new URLRequest("data/img/chapter"+idxChapter+"/page"+idxPage+".png").url;
			
			super.loadContent();
		}
		
		override public function unloadContent():void
		{
			super.unloadContent();
			
			imgLoader.source = null;
		}
	}
}