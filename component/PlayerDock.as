package component
{
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
	import flash.net.URLRequest;
	
	import app.AppSettings;
	
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.Slider;
	import feathers.controls.ToggleButton;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalAlign;
	import feathers.media.MediaTimeMode;
	import feathers.media.PlayPauseToggleButton;
	import feathers.media.SeekSlider;
	import feathers.media.SoundPlayer;
	import feathers.media.TimeLabel;
	
	import pages.PageContent;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextFormat;
	import starling.utils.SystemUtil;
	
	import theme.CustomTheme;
	
	public class PlayerDock extends Sprite
	{
		public static const PAUSE_AT_END:String = "pause_at_end";
		private var bg:Image;
		private var slider:Slider;
		
		private var _isDialogMode:Boolean =false;
		private var _currentSoundSeq:int = -1;
		private var _tillTime:Number=0;
		
		private var vecSoundItem:Vector.<RectItem> = null;
		
		private var seekSlider:SeekSlider;
		private var playToggle:PlayPauseToggleButton;
		private var soundPlayer:SoundPlayer = null;
		private var timeLabel:Label;
		
		private var _soundSource:Object = null;
		
		public function PlayerDock()
		{
			super();
			
			drawUI();
		}
		
		private function drawUI():void
		{
			bg = new Image(Root.assets.getTexture("footer/bg"));
			bg.alpha = 0.7;
			this.addChild(bg);
				
			playToggle = new PlayPauseToggleButton();
			playToggle.defaultSkin = new Image(Root.assets.getTexture("footer/play"));
			playToggle.defaultSelectedSkin = new Image(Root.assets.getTexture("footer/pause"));
			
			var layout:HorizontalLayout = new HorizontalLayout();
			layout.verticalAlign = VerticalAlign.MIDDLE;
			layout.paddingLeft = 30;
			layout.gap =20;
			
			soundPlayer = new SoundPlayer();
			soundPlayer.height = bg.height;
			soundPlayer.layout = layout;
			soundPlayer.autoPlay = false;
			this.addChild(soundPlayer);
			
			soundPlayer.addChild(playToggle);
			
			seekSlider = new SeekSlider();
			seekSlider.minimumPadding =-10;
			seekSlider.maximumPadding = -10;
			seekSlider.width = 340;
			soundPlayer.addChild(seekSlider);
			
			var label:TimeLabel = new TimeLabel();
			label.fontStyles = new TextFormat(CustomTheme.FONT_NOTO_MEDIUM, 24, 0xffffff, HorizontalAlign.CENTER);
			label.displayMode = MediaTimeMode.CURRENT_AND_TOTAL_TIMES;
			label.delimiter = "   ";
			label.paddingLeft =10;
			soundPlayer.addChild( label );
			
			soundPlayer.addEventListener(Event.COMPLETE, onCompletePlaySound);
		}
		
		private function onCompletePlaySound():void
		{
			if(SystemUtil.isDesktop != true) NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL;
			removeEventListener(Event.ENTER_FRAME, onEnterFramePartCheck);
			dispatchEventWith(PAUSE_AT_END, false);
		}
		
		public function setSource(source:Object):void
		{
			_soundSource = source;
		}
		
		public function setSound(source:Object):void
		{
			if(SystemUtil.isDesktop != true) NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL;
			soundPlayer.stop();
			soundPlayer.soundSource = null;
			soundPlayer.soundSource = source;
		}
		
		public function setSoundItem(soundItems:Vector.<RectItem>):void
		{
			vecSoundItem = null;
			vecSoundItem = soundItems;
		}
		
		public function play():void
		{
			if(SystemUtil.isDesktop != true) NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;
			
			trace("soundPlayer.soundSource",soundPlayer.soundSource);
			if(!soundPlayer.soundSource)
			{
				trace("!1");
				soundPlayer.soundSource = _soundSource;
			}
			soundPlayer.play();
		}
		
		public function playFrom(idx:int):void
		{
			var time1:int = vecSoundItem[idx].min*60*1000 + vecSoundItem[idx].sec*1000 + vecSoundItem[idx].mil;
			soundPlayer.seek(time1/1000);
			soundPlayer.play();
		}
		
		public function pause():void
		{
			removeEventListener(Event.ENTER_FRAME, onEnterFramePartCheck);
			soundPlayer.pause();
			if(SystemUtil.isDesktop != true) NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL;
		}
		
		public function stop():void
		{
			removeEventListener(Event.ENTER_FRAME, onEnterFramePartCheck);
			soundPlayer.stop();
			soundPlayer.soundSource = null;
			if(SystemUtil.isDesktop != true) NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL;
		}
		
		
		public function playPart(sequence:int, isDialogMode:Boolean = false):void
		{
			_isDialogMode = isDialogMode;
			_currentSoundSeq = sequence;
			
//			time1 = vecSoundItem[sequence].min*60*1000 + vecSoundItem[sequence].sec*1000 + vecSoundItem[sequence].mil;
//			if (sequence+1 < vecSoundItem.length)
//				time2 = vecSoundItem[sequence+1].min*60*1000 + vecSoundItem[sequence+1].sec*1000 + vecSoundItem[sequence+1].mil;
//			
//			tillTime = time2/1000;
			
			if(!soundPlayer.soundSource) soundPlayer.soundSource = _soundSource;
			
			var times:Array = calcTime(sequence); 
			_tillTime = times[1];
			
			pause();
			
			
			addEventListener(Event.ENTER_FRAME, onEnterFramePartCheck);
			if(SystemUtil.isDesktop != true) NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;
			
			soundPlayer.seek(times[0]);
			soundPlayer.play();
		}
		
		private function calcTime(sequence:int):Array
		{
			var time1:Number = 0;
			var time2:Number = 0;
			
			time1 = vecSoundItem[sequence].min*60*1000 + vecSoundItem[sequence].sec*1000 + vecSoundItem[sequence].mil;
			
			if (sequence+1 < vecSoundItem.length)
				time2 = vecSoundItem[sequence+1].min*60*1000 + vecSoundItem[sequence+1].sec*1000 + vecSoundItem[sequence+1].mil;
			
			time1 = time1/1000;
			time2 = time2/1000;
			
			return [time1, time2];
		}
		
		protected function onEnterFramePartCheck(event:Event):void
		{
			if(_tillTime == 0)
			{
				if(soundPlayer.currentTime >= soundPlayer.totalTime)
				{
					soundPlayer.pause();
					removeEventListener(Event.ENTER_FRAME, onEnterFramePartCheck);
					dispatchEventWith(PAUSE_AT_END, false);
				}
			}
			else if(soundPlayer.currentTime >= _tillTime) //else if(soundPlayer.currentTime >= _tillTime+0.3)
			{
				soundPlayer.pause();
				removeEventListener(Event.ENTER_FRAME, onEnterFramePartCheck);
				
				dispatchEventWith(PAUSE_AT_END, false);
				
				if(_isDialogMode)
				{
					_currentSoundSeq++;
					if(_currentSoundSeq > vecSoundItem.length-1)
					{
						
					}
					else
					{
						addEventListener(Event.ENTER_FRAME, onEnterFramePartCheck);
						if(SystemUtil.isDesktop != true) NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;
						
						var times:Array = calcTime(_currentSoundSeq);
						_tillTime = times[1];
						
						soundPlayer.seek(times[0]);
						soundPlayer.play();
						
						dispatchEventWith(Event.CHANGE, false, _currentSoundSeq);
					}
				}	
			}
		}
		
	}
}