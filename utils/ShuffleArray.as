package utils
{
	public class ShuffleArray
	{
		public static function setArray_A($array:Array):Array
		{
			var len:int = $array.length;
			
			for(var i:int =0; i < len ; ++i)
			{
				var randVal:int = Math.floor(Math.random() *i);
				var temp:int = $array[randVal];
				$array[randVal] = $array[i];
				$array[i] = temp;
			}
			
			return $array; 
		}
		
		public static function forObject($array:Array):Array
		{
			var len:int = $array.length;
			
			for(var i:int =0; i < len ; ++i)
			{
				var randVal:int = Math.floor(Math.random() *i);
				var temp:Object = $array[randVal];
				$array[randVal] = $array[i];
				$array[i] = temp;
			}
			
			return $array; 
		}
		
		public static function setArray($array:Array):Array
		{
			var result:Array=[];
			var len:int = $array.length;
			for(var i:int =0; i < len; ++i)
			{
				var randVal:int = Math.floor(Math.random() *$array.length);
				result.push($array.splice(randVal,1));
			}
			return result;
		}
		
		
	}
}