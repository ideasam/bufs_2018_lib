package component.quiz
{
	import flash.errors.IllegalOperationError;
	
	import feathers.controls.Button;
	import feathers.layout.HorizontalAlign;
	
	import pages.type.QuizPage;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	import theme.CustomTheme;
	
	public class ItemOption extends Sprite
	{
		private var _page:QuizPage = null;
		
		private var _idGroup:int = -1;
		private var _idQuiz:int = -1;
		private var _idOption:int = -1;
		public var _isSelected:Boolean = false;
		public var _isCorrect:Boolean = false;
		private var _txtOption:String = "";
		private var _btnOption:Button = null;
		
		public function ItemOption() { super(); }
		public function get isSelected():Boolean { return _isSelected; }
		public function get isCorrect():Boolean { return _isCorrect; }
		
		public function create(page:QuizPage, idGroup:int, idQuiz:int, idOption:int, dataQuiz:Object, width:Number, xPos:int, yPos:int):int
		{
			_page = page;
			_idGroup = idGroup;
			_idQuiz = idQuiz;
			_idOption = idOption;
			_txtOption = dataQuiz.options[idOption];
			_isCorrect = ((_idOption+1) == dataQuiz.answer as int) ? true : false;
			
			_btnOption = new Button();
			_btnOption.scaleWhenDown = 0.98;
			_btnOption.wordWrap = true;
			_btnOption.touchable = true;
			_btnOption.fontStyles = CustomTheme.fsDark.clone();
			_btnOption.fontStyles.size = 22;
			_btnOption.horizontalAlign = HorizontalAlign.LEFT;
			_btnOption.label = makeQuizNum(idOption, false) + " " + _txtOption;
//			_btnOption.addEventListener(Event.TRIGGERED, onTriggered);
			
			//가로 모드
			if (dataQuiz.horizontal)
			{
				_btnOption.width = int(width/dataQuiz.options.length);
				_btnOption.x = xPos + _btnOption.width*idOption;
				_btnOption.y = yPos;
				_page.scrollCont.addChild(_btnOption);
				_btnOption.validate();
				
				if (idOption == dataQuiz.options.length - 1)
					yPos += _btnOption.height + 15;
			}
			//세로 모드
			else
			{
				_btnOption.width = width;
				_btnOption.x = xPos;
				_btnOption.y = yPos;
				_page.scrollCont.addChild(_btnOption);
				_btnOption.validate();
				
				yPos += _btnOption.height + 15;
			}
			
			return yPos;
		}
		
		private function makeQuizNum(idx:int, isSelected:Boolean, isWrong:Boolean=false):String
		{
			var strQuizNum:String = "";
			if (isWrong)
			{
				switch(idx) {
					case 0: strQuizNum = '❶'; break;
					case 1: strQuizNum = '❷'; break;
					case 2: strQuizNum = '❸'; break;
					case 3: strQuizNum = '❹'; break;
					case 4: strQuizNum = '❺'; break;
					default: throw IllegalOperationError("makeQuizNum: unexpected idx(" + idx + ")");
				}				
				strQuizNum = "<font color='#ff0000'>" + strQuizNum + "</font>";
			}
			else
			{
				if (isSelected)
				{
					switch(idx) {
						case 0: strQuizNum = '❶'; break;
						case 1: strQuizNum = '❷'; break;
						case 2: strQuizNum = '❸'; break;
						case 3: strQuizNum = '❹'; break;
						case 4: strQuizNum = '❺'; break;
						default: throw IllegalOperationError("makeQuizNum: unexpected idx(" + idx + ")");
					}
				}
				else
				{
					switch(idx) {
						case 0: strQuizNum = '①'; break;
						case 1: strQuizNum = '②'; break;
						case 2: strQuizNum = '③'; break;
						case 3: strQuizNum = '④'; break;
						case 4: strQuizNum = '⑤'; break;
						default: throw IllegalOperationError("makeQuizNum: unexpected idx(" + idx + ")");
					}
				}
			}
			return strQuizNum;
		}
		
		public function activate():void
		{
			_btnOption.addEventListener(Event.TRIGGERED, onTriggered);
		}
		
		public function deactivate():void
		{
			_btnOption.removeEventListener(Event.TRIGGERED, onTriggered);
		}
		
		public function reset():void
		{
			_isSelected = false;
			_btnOption.label = makeQuizNum(_idOption, _isSelected) + " " + _txtOption;
		}
		
		public function highlightWrong():void
		{
			_btnOption.label = makeQuizNum(_idOption, false, true) + " " + _txtOption;
		}
		
		private function onTriggered(event:Event):void
		{
			if (_page)
			{
				_isSelected = true;
				_btnOption.label = makeQuizNum(_idOption, _isSelected) + " " + _txtOption;
				trace("dispatchEventWidth - " + QuizPage.ON_TAB_OPTION + _page.idxPage);
				_page.dispatchEventWith(QuizPage.ON_TAB_OPTION + _page.idxPage, false, {"idGroup":_idGroup, "idQuiz":_idQuiz, "idOption":_idOption});
			}
			else
			{
				throw new Error("_page is not ready!");
			}
		}
	}
}