package component
{
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	
	import flash.errors.IllegalOperationError;
	import flash.geom.Rectangle;
	
	import app.AppSettings;
	
	import feathers.controls.BasicButton;
	import feathers.controls.Button;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.ToggleButton;
	import feathers.layout.HorizontalAlign;
	import feathers.skins.ImageSkin;
	
	import pages.data.StudyInfo;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	import theme.CustomTheme;
	
	public class SpeechBalloon extends Sprite
	{
		private var timelineExpansion:TimelineLite = null;
		
		private var _kor:String;
		private var _trans:String;
		
		private var _posY_N:int =  0;
		private var _posY_S:int =  0;
		
		private var bg:ImageSkin;
		private var imgLabel0:ImageLoader;;
		private var label0:Label;
		private var label1:Label;
		
		private var toggleTrans:ToggleButton;
		private var btnInfo:BasicButton;
		
		private var lineSplitter:Image;
		
		/**
		 * 토글 전 
		 */
		public var heightN:int=0;
		/**
		 * 토글 후
		 */
		public var heightS:int=0;
		
		//대화 순서
		public var seqence:int = 0;
		
		public var animationName:String;
		public var sound:String = null;
		/**
		 *  0 = 유저(왼쪽)
		 *  1 = 상대방(오른쪽)
		 */
		public var mode:int = -1;
		private var btnBG:Button;
		
		//이미지 파일 이름
		public var imgNames:Array;
		
		public function SpeechBalloon(idxChapter:int, idxPage:int, txt0:String, txt1:String = "", mode:int=0, sound:String ="", isImageType:Boolean = false)
		{
			super();
			
			this.sound = sound;
			this.mode = mode;
			
			if (-1 == mode)
			{
				this.createSplitter();
			}
			else
			{
				this.create(idxChapter, idxPage, txt0, txt1, mode, sound, isImageType)
			}
		}
		
		protected function createSplitter():void
		{
			lineSplitter = new Image(Root.assets.getTexture("textConversation/dotSplitter"));
			lineSplitter.x = (AppSettings.width - lineSplitter.width) >> 1;
			lineSplitter.height = 1.5;
			lineSplitter.alpha = 0.5;
			
			this.addChild(lineSplitter);
		}
		
		protected function create(idxChapter:int, idxPage:int, txt0:String, txt1:String, mode:int, sound:String, isImageType:Boolean):void
		{
			var minW:int = 380;
			var minH:int = 80;
			
			bg = new ImageSkin(Root.assets.getTexture("textConversation/speechBox"+mode));
			bg.scale9Grid = new Rectangle(35,30,431,22);
			
			btnBG = new Button();
			btnBG.defaultSkin = bg;
			btnBG.scaleWhenDown = 0.98;
			btnBG.validate();
			this.addChild(btnBG);
			
			toggleTrans = new ToggleButton();
			toggleTrans.defaultSkin = new Image(Root.assets.getTexture("textConversation/btnTransS"));
			toggleTrans.defaultSelectedSkin = new Image(Root.assets.getTexture("textConversation/btnTransN"));
			toggleTrans.validate();
			this.addChild(toggleTrans);
			
			label0 = new Label();
			label0.paddingTop = 20;
			label0.paddingLeft = 20;
			label0.paddingRight = 20;
			label0.paddingBottom = 0;
			
			label0.text = txt0 + "\n\n";
			label0.wordWrap = true;
			label0.touchable = false;
			label0.width = btnBG.width;
			label0.fontStyles = CustomTheme.fsConversationPageOrigin.clone();
			label0.validate();
			btnBG.addChild(label0);
			
			if (isImageType)
			{
				//isImageType이 true라면 txt0는 sound 이름임
				var path:String =
					"data/img/chapter" + idxChapter +
					"/page" + idxPage + "/" + sound + ".png";
				imgLabel0 = new ImageLoader();
				imgLabel0.x = 0;
				imgLabel0.y = 20;
				imgLabel0.visible = true;
				btnBG.addChild(imgLabel0);
				
				ExtPicLoader.loadTexture(path, onCompleteLoad);
			}
			
			label1 = new Label();
			label1.paddingTop = 0;
			label1.paddingLeft = 20;
			label1.paddingRight = 20;
			label1.paddingBottom = 20;
			
			label1.text = txt1;
			label1.wordWrap = true;
			label1.touchable = false;
			label1.width = btnBG.width;
			label1.fontStyles = CustomTheme.fsConversationPageTrans.clone();
			label1.validate();
			btnBG.addChild(label1);
			
			label1.alpha = 0;
			label1.visible = false;
			label1.y = label0.height - 5;
			
			switch(mode)
			{
				case 0:
					label0.paddingLeft = 30;
					label1.paddingLeft = 30;
					if (isImageType) imgLabel0.x = 10;
					btnBG.x = 0;
					toggleTrans.x = btnBG.width + (toggleTrans.width*0.1);
					break;
				case 1:
					label0.paddingRight = 30;
					label1.paddingRight = 30;
					btnBG.x = toggleTrans.width;
					break;
				case 2:
					btnBG.x = 0;
					label0.fontStyles.horizontalAlign = HorizontalAlign.CENTER;
					label1.fontStyles.horizontalAlign = HorizontalAlign.CENTER;
					toggleTrans.x = btnBG.width - (toggleTrans.width + 5);
					toggleTrans.y = 5;
					break;
				
				default:
					throw IllegalOperationError("SpeechBalloon: Invalid mode");
					break;
			}
						
			label0.x = (btnBG.width-label0.width) >> 1;
			label1.x = (btnBG.width-label1.width) >> 1;
			
			label0.validate();
			label1.validate();
			
			heightN = label0.height - 5;
			heightS = label0.height + label1.height + 5;
			
			btnBG.height = heightN;					
			
			toggleTrans.addEventListener(Event.CHANGE, onChangeToggle);
			btnBG.addEventListener(Event.TRIGGERED, onTriggerdBtn);
		}
		
		private function onCompleteLoad(texture:Texture):void
		{
			if(imgLabel0.source)
			{
				(imgLabel0.source as Texture).dispose();
			}
			imgLabel0.source = texture;
		}
		
		private function onCopmleteExpansion():void
		{
			
		}
		
		private function onReverseExpansion():void
		{
			label1.visible = false;
		}
		
		private function onChangeToggle():void
		{
			this.dispatchEventWith(Event.CHANGE, true, [seqence, toggleTrans.isSelected]);
		}
		
		private function onTriggerdBtn():void
		{
			this.dispatchEventWith(Event.SELECT, true, seqence); 
		}
		
		public function expand():TimelineLite
		{
			if (null == timelineExpansion)
			{
				timelineExpansion = new TimelineLite({onComplete:onCopmleteExpansion, onReverseComplete:onReverseExpansion});
				timelineExpansion.add([
					TweenLite.to(btnBG, 0.3, {height:heightS}),
					TweenLite.to(label1, 0.3, {delay:0.2, alpha:1})
				], 0, "expansion");
			}
			label1.visible = true;
			timelineExpansion.play();	
			
			return timelineExpansion;
		}
		
		public function callapse():TimelineLite
		{
			timelineExpansion.reverse();
			return timelineExpansion;
		}
		
		public function getVisibleHeight():int
		{
			if (btnBG)
			{
				btnBG.validate();
				return btnBG.height;
			}
			else
			{
				return lineSplitter.height;
			}
		}
		
		public function get posY_N():int
		{
			return _posY_N;
		}

		public function set posY_N(value:int):void
		{
			_posY_N = value;
		}

		public function get posY_S():int
		{
			return _posY_S;
		}

		public function set posY_S(value:int):void
		{
			_posY_S = value;
		}
	}
}