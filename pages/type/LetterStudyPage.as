package pages.type
{
	import flash.geom.Rectangle;
	
	import app.AppSettings;
	
	
	import feathers.controls.BasicButton;
	import feathers.controls.Button;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.ScrollContainer;
	import feathers.controls.ScrollPolicy;
	import feathers.controls.ToggleButton;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.core.ITextRenderer;
	import feathers.core.ToggleGroup;
	import feathers.events.FeathersEventType;
	import feathers.layout.HorizontalLayout;
	import feathers.text.BitmapFontTextFormat;
	
	import pages.type.BasePage;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.text.TextFormat;
	import starling.utils.Align;
	
	import theme.CustomTheme;
	
	import component.WordBoard;
	
	/**
	 * @author everseen
	 */
	public class LetterStudyPage extends BasePage
	{
		public static const HIRAGANA:String = "hiragana";
		public static const KATAKANA:String = "katakana";
		
		public static const BOTTOM_LAYOUT_HIEGHT:int = 410;
		
		//hiragana, katakana
		public static var current_hk_mode:String = "hiragana";
		public static var selected_word_index:int = 0;
		
		private var btnClose:Button;
		private var btnAllLetterList:BasicButton;
		private var btnEx:Button;
		
		private var letterToggleGroup:ToggleGroup;
		
		private var contBottomScroller:ScrollContainer;
		private var contBottomBar:Sprite;
		
		private var currentPage:int = 0;
		private var vecInLineLetterGroup:Vector.<LayoutGroup>;
		private var vecTextToggleBtn:Vector.<ToggleButton>;
		private var toggleStroke:ToggleButton;
		private var toggleWrite:ToggleButton;
		private var toggleHK:ToggleButton;
		private var wordBorad:WordBoard;
		
		// private var toggleGroupSW:ToggleGroup;
		private var jsonData:Object;
		private var btn_previous:BasicButton;
		private var btn_next:BasicButton;
		
		public function LetterStudyPage()
		{
			
		}
		
		override public function loadContent():void
		{
			super.loadContent();
			initialize();
			//			draw();
		}
		
		override public function unloadContent():void
		{
			wordBorad.clearDrawing();			
			super.unloadContent();
		}
		
		protected function initialize():void
		{
			//mid
			var mlayout:HorizontalLayout = new HorizontalLayout();
			mlayout.paddingLeft = 57.5;
			mlayout.paddingRight = 57.5;
			mlayout.paddingTop = 32;
			mlayout.gap = 57.5*2;
			
			contBottomScroller = new ScrollContainer();
			contBottomScroller.backgroundSkin = new Image(Root.assets.getTexture("letter/lettterBg"));
			contBottomScroller.layout = mlayout;
			contBottomScroller.width = AppSettings.width;
			
			contBottomScroller.snapToPages = true;
			contBottomScroller.horizontalScrollPolicy = ScrollPolicy.ON;
			contBottomScroller.verticalScrollPolicy = ScrollPolicy.OFF;
			this.addChild(contBottomScroller);
			contBottomScroller.validate();
			
			addLetterButtons();
			
			// bottom
			contBottomBar = new Sprite();
			this.addChild(contBottomBar);
			
			var imgBottomBar:Image = new Image(Root.assets.getTexture("letter/bottomBar"));
			contBottomBar.addChild(imgBottomBar);
			
			btnAllLetterList = new BasicButton();
			btnAllLetterList.defaultSkin = new Image(Root.assets.getTexture("letter/btnMenu"));
			btnAllLetterList.minTouchWidth = btnAllLetterList.minTouchHeight = 92;
			btnAllLetterList.x = 20;
			btnAllLetterList.y = 34;
			contBottomBar.addChild(btnAllLetterList);
			
			btnEx = new Button();
			btnEx.defaultSkin = new Image(Root.assets.getTexture("letter/btnPracticeN"));
			btnEx.downSkin = new Image(Root.assets.getTexture("letter/btnPracticeS"));
			btnEx.x = 408;
			btnEx.y = 18;
			contBottomBar.addChild(btnEx);
			
			contBottomBar.y = AppSettings.height - contBottomBar.height;
			contBottomScroller.y = contBottomBar.y - contBottomScroller.height + 5;
			
			btn_previous = new BasicButton();
			btn_previous.defaultSkin = new Image(Root.assets.getTexture("letter/btnPrevious"));
			btn_previous.minTouchWidth = 40;
			btn_previous.minTouchHeight = 120;
			btn_previous.x = 14;
			this.addChild(btn_previous);
			
			btn_next = new BasicButton();
			btn_next.defaultSkin = new Image(Root.assets.getTexture("letter/btnNext"));
			btn_next.minTouchWidth = 40;
			btn_next.minTouchHeight = 120;
			btn_next.x = 600;
			this.addChild(btn_next);
			
			btn_previous.y = btn_next.y = contBottomScroller.y + 80;
			
			//상단공간
			//min top padding  = 25;
			var availTopHeight:int = AppSettings.height - BOTTOM_LAYOUT_HIEGHT - 30;
			
			wordBorad = new WordBoard(availTopHeight);
			this.addChild(wordBorad);
			wordBorad.y = 60+(availTopHeight - wordBorad.matHeight) >>1;
			wordBorad.maxBound = new Rectangle(50,wordBorad.y + 45, 540, wordBorad.matHeight-100);
			//toggles
			var toggleCont:Sprite = new Sprite();
			toggleCont.y = wordBorad.y + wordBorad.matHeight + ((contBottomScroller.y - (wordBorad.y + wordBorad.matHeight) - 100) >>1) - 20;
			this.addChild(toggleCont);
			
			//toggleGroupSW = new ToggleGroup();
			//toggleGroupSW.isSelectionRequired = true;
			
			toggleStroke = new ToggleButton();
			//toggleArrow.toggleGroup = toggleGroupSW;
			toggleStroke.x = 45;
			toggleStroke.defaultSkin = new Image(Root.assets.getTexture("letter/btnStrokeOrderN"));
			toggleStroke.defaultSelectedSkin = new Image(Root.assets.getTexture("letter/btnStrokeOrderS"));
			toggleCont.addChild(toggleStroke);
			
			toggleWrite = new ToggleButton();
			//toggleWrite.toggleGroup = toggleGroupSW;
			toggleWrite.x = 160;
			toggleWrite.defaultSkin = new Image(Root.assets.getTexture("letter/btnWriteN"));
			toggleWrite.defaultSelectedSkin = new Image(Root.assets.getTexture("letter/btnWriteS"));
			toggleCont.addChild(toggleWrite);
			
			toggleHK = new ToggleButton();
			toggleHK.x = 275;
			toggleHK.defaultSkin = new Image(Root.assets.getTexture("letter/btnHiragana"));
			toggleHK.defaultSelectedSkin = new Image(Root.assets.getTexture("letter/btnKatakana"));
			toggleCont.addChild(toggleHK);
			
			btnClose = new Button();
			//			btnClose.styleNameList.add(CustomTheme.STYLE_NAME_CLOSE_BUTTON);
			btnClose.x = 585;
			btnClose.y = 24;
			this.addChild(btnClose);
			
			toggleHK.isSelected = current_hk_mode == HIRAGANA ? false : true;
			wordBorad.changeHKMode(!toggleHK.isSelected);
			// events
			//			this.backButtonHandler = onTriggeredBtnClose;
			
			btnAllLetterList.addEventListener(Event.TRIGGERED, onTriggeredBtnLetterList);
			btnEx.addEventListener(Event.TRIGGERED, onTriggeredBtnEx);
			
			btn_next.addEventListener(Event.TRIGGERED, onTriggeredBtnNext);
			btn_previous.addEventListener(Event.TRIGGERED, onTriggeredBtnPrev);
			
			toggleStroke.addEventListener(Event.CHANGE, toggleStrokeMode);
			toggleWrite.addEventListener(Event.CHANGE, toggleWriteMode);
			toggleHK.addEventListener(Event.CHANGE, onChangeToggleHK);
			//letterToggleGroup.isSelectionRequired = false;
			letterToggleGroup.selectedIndex = selected_word_index;
			letterToggleGroup.isSelectionRequired = true;
			
			toggleStroke.isSelected = false;
			toggleWrite.isSelected = false;
			
			if(AppSettings.isTablet)
			{
				toggleCont.scale = 0.75;
				toggleCont.y = toggleCont.y + 12;
			}
			
			changeVisibleLetterGroup();
		}
		
		private function onTriggeredBtnPrev(event:Event):void
		{
			if(!contBottomScroller.isScrolling) contBottomScroller.scrollToPageIndex(currentPage-1, 0);
		}
		
		private function onTriggeredBtnNext(event:Event):void
		{
			if(!contBottomScroller.isScrolling) contBottomScroller.scrollToPageIndex(currentPage+1, 0);
		}
		
		private function onTriggeredBtnEx(event:Event):void
		{
			wordBorad.clearDrawing();
			//			this.dispatchEventWith(ViewControl.SHOW_LETTER_EX_VIEW);
		}
		
		private function toggleWriteMode():void
		{
			wordBorad.toggleWriteMode(toggleWrite.isSelected);
		}
		
		private function toggleStrokeMode(event:Event):void
		{
			wordBorad.toggleStrokeMode(toggleStroke.isSelected);
		}
		
		private function onChangeToggleHK(event:Event):void
		{
			current_hk_mode = !toggleHK.isSelected ? "hiragana" : "katakana";
			wordBorad.changeHKMode(!toggleHK.isSelected);
			
			var len:int = 0;
			var cnt:int = 0 ;
			for(var k:int=0; k<4; ++k)
			{
				var tmpData:Array = jsonData["t"+k];
				len = tmpData.length;
				
				for(var i:int=0; i<len; ++i)
				{
					onChangeTextInLine(tmpData[i]);
				}
			}
			
			function onChangeTextInLine(data:Object):void
			{
				var mlen:int = data.length;
				for(var j:int=0; j<mlen; ++j)
				{
					vecTextToggleBtn[cnt].label = current_hk_mode == HIRAGANA ? data[j].jp : data[j].ka;
					cnt++;
				}
			}
			
			var selItem:ToggleButton = letterToggleGroup.selectedItem as ToggleButton;
			var selLabel:String = selItem.label;
			
			var selData:Array = String(selItem.name).split("#");
			var selPrn:String = selData[1];
			
			wordBorad.setText(selLabel,selPrn);
		}
		
		private function addLetterButtons():void
		{
			vecTextToggleBtn = new Vector.<ToggleButton>();
			vecInLineLetterGroup = new Vector.<LayoutGroup>();
			letterToggleGroup = new ToggleGroup();
			letterToggleGroup.isSelectionRequired = false;
			letterToggleGroup.selectedIndex = -1;
			
			jsonData = Root.assets.getObject("letters_k");
			//
			//560;
			var len:int = 33;
			var inLineCont:LayoutGroup = null;
			
			var hasTwoIndexes:Array = [2,3];
			var pageIndex:int = 0;
			
			for(var k:int=0; k<4; ++k)
			{
				var hasTwoLetters:Boolean = hasTwoIndexes.indexOf(k) > -1 ? true : false;
				var tmpData:Array = jsonData["t"+k];
				len = tmpData.length;
				
				for(var i:int=0; i<len; ++i)
				{
					var prvPageIdx:int = currentPage - 1;
					var nextPageIdx:int = currentPage + 1;
					
					inLineCont = createInLineLetterContainer(tmpData[i], pageIndex,hasTwoLetters);
					vecInLineLetterGroup.push(inLineCont);
					contBottomScroller.addChild(inLineCont);
					
					if(currentPage == i || prvPageIdx == i || nextPageIdx == i) 
					{	inLineCont.visible = true;	}
					else 
					{	inLineCont.visible = false;	}
					
					pageIndex++;
				}
			}
			
			letterToggleGroup.addEventListener(Event.CHANGE, letterToggleGroupChangeHandler);
			
			contBottomScroller.addEventListener(FeathersEventType.SCROLL_START, onStartScorllBottomScroller);
			contBottomScroller.addEventListener(FeathersEventType.SCROLL_COMPLETE, onCompleteScorllBottomScroller);			
		}
		
		private function onEnterframeOnScroll(event:EnterFrameEvent):void
		{
			var mPage:int = (contBottomScroller.horizontalScrollPosition+320)/AppSettings.width; 
			
			if(mPage != currentPage)
			{
				currentPage = mPage;
				changeVisibleLetterGroup(currentPage);
			}
		}
		
		private function changeVisibleLetterGroup(forceCurPage:int=-1):void
		{
			var mCurrentPage:int = forceCurPage < 0 ? contBottomScroller.horizontalPageIndex:forceCurPage;
			var prvPageIdx:int = mCurrentPage - 1;
			var nextPageIdx:int = mCurrentPage + 1;
			
			var len:int = vecInLineLetterGroup.length;
			
			for(var i:int=0; i<len; ++i)
			{
				if(mCurrentPage == i || prvPageIdx == i || nextPageIdx == i) 
				{	
					vecInLineLetterGroup[i].visible = true;
				}
				else 
				{	
					vecInLineLetterGroup[i].visible = false;	
				}	
			}
			
			//mCurrentPage = currentPage;
			//currentPage = 
		}
		
		private function onStartScorllBottomScroller(event:Event):void
		{
			contBottomScroller.removeEventListener(EnterFrameEvent.ENTER_FRAME, onEnterframeOnScroll);
			contBottomScroller.addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterframeOnScroll);
			changeVisibleLetterGroup();
		}
		
		private function onCompleteScorllBottomScroller(event:Event):void
		{
			contBottomScroller.removeEventListener(EnterFrameEvent.ENTER_FRAME, onEnterframeOnScroll);
			changeVisibleLetterGroup();
		}
		
		private function letterToggleGroupChangeHandler(event:Event):void
		{
			var group:ToggleGroup = ToggleGroup( event.currentTarget );
			
			var selLabel:String = group.selectedItem["label"];
			
			var selData:Array = String(group.selectedItem.name).split("#");
			var selPrn:String = selData[1];
			
			wordBorad.clearDrawing();
			
			currentPage = selData[0];
			
			contBottomScroller.scrollToPageIndex(selData[0], 0);
			
			wordBorad.setText(selLabel,selPrn);
			
			
			if(currentPage > 10)
			{
				toggleStroke.touchable = false;
				toggleStroke.visible = false;
				
				toggleWrite.x = 45;
				toggleHK.x = 160;
			}
			else
			{
				toggleStroke.touchable = true;
				toggleStroke.visible = true;
				
				toggleStroke.x = 45;
				toggleWrite.x = 160;
				toggleHK.x = 275;
			}
		}
		
		private function createInLineLetterContainer(data:Array, pageIndex:int, hasTwoLetters:Boolean = false):LayoutGroup
		{
			var tfmN:TextFormat = new TextFormat(CustomTheme.FONT_HINDI, 72, 0x6FA8E0);
			tfmN.bold = true;
			var tfmS:TextFormat = new TextFormat(CustomTheme.FONT_HINDI, 72, 0xffffff);
			tfmS.bold = true;
			
			var layout:HorizontalLayout = new HorizontalLayout();
			
			if(!hasTwoLetters)
			{
				layout.gap = 15;
			}
			else
			{
				tfmN.letterSpacing = -12;
				tfmS.letterSpacing = -12;
				layout.gap = -20;
			}
			
			var group:LayoutGroup = new LayoutGroup();
			group.layout = layout;
			group.width = 525;
			
			var len:int = data.length;
			var prTfm:TextFormat = CustomTheme.largeHindiStyles.clone();
			prTfm.color = 0x6FA8E0;
			prTfm.size = 20;
			
//			var bformat:BitmapFontTextFormat = new BitmapFontTextFormat( "bmfont" );
//			bformat.size = 74;
//			bformat.color = 0x6FA8E0;
//			bformat.align = Align.CENTER;
//			
//			var b_selformat:BitmapFontTextFormat = new BitmapFontTextFormat( "bmfont" );
//			b_selformat.size = 74;
//			b_selformat.color = 0xffffff;
//			b_selformat.align = Align.CENTER;
			
			var strPr:String =""; 
			
			for(var i:int=0; i<len; ++i)
			{
				var toggleBtn:ToggleButton = new ToggleButton();
				toggleBtn.toggleGroup = letterToggleGroup;
				
				toggleBtn.labelOffsetY = -20;
				toggleBtn.selectedFontStyles = tfmS;
				toggleBtn.defaultSkin = new Image(Root.assets.getTexture("letter/btnWordN"));
				toggleBtn.defaultSelectedSkin = new Image(Root.assets.getTexture("letter/btnWordS"));
				group.addChild(toggleBtn);
				vecTextToggleBtn.push(toggleBtn);
				
				if(data[i].jp)
				{
					toggleBtn.label = current_hk_mode == HIRAGANA ? data[i].jp : data[i].ka;
					toggleBtn.fontStyles = tfmN;
					toggleBtn.name = pageIndex + "#" +data[i].pr;
					
					if(hasTwoLetters)
					{
						toggleBtn.labelOffsetX = -5;
						toggleBtn.width = 140;
					}
					toggleBtn.validate();
					
					strPr = data[i].pr;
					
//					if(strPr == "wo") strPr = "o";
//					
//					if(strPr == "ri" || strPr == "ta" || strPr == "da")
//					{
//						if(strPr == "ta") toggleBtn.labelOffsetX = -5;
//						else if(strPr == "da") toggleBtn.labelOffsetX = 2;
//						else toggleBtn.labelOffsetX = -15;
//						
//						toggleBtn.labelOffsetY = -15;
//						toggleBtn.labelFactory = function():ITextRenderer
//						{
//							var textRenderer:BitmapFontTextRenderer = new BitmapFontTextRenderer();
//							textRenderer.textFormat = bformat;
//							textRenderer.selectedTextFormat = b_selformat;
//							
//							return textRenderer;
//						};
//					}
//					else
					{
						toggleBtn.fontStyles = tfmN;
						toggleBtn.selectedFontStyles = tfmS;
					}
					
					
					var prLabel:Label = new Label();
					prLabel.touchable = false;
					prLabel.width = toggleBtn.width;
					prLabel.fontStyles = prTfm;
					prLabel.text = "["+strPr+ "]";
					prLabel.y = 85;
					prLabel.name = "prLable";
					toggleBtn.addChild(prLabel);
					
					toggleBtn.addEventListener(Event.CHANGE, function(event:Event):void{
						
						var targetBtn:ToggleButton = ToggleButton(event.currentTarget);
						var targetLabel:Label = targetBtn.getChildByName("prLable") as Label;
						
						var prSelTfm:TextFormat = CustomTheme.largeHindiStyles.clone();
						prSelTfm.size = 20;
						
						if(!targetBtn.isSelected)
						{
							targetLabel.fontStyles = prTfm;
						}
						else
						{
							targetLabel.fontStyles = prSelTfm;
						}
					});
				}
				else
				{
					toggleBtn.visible = false;
				}
			}
			return group;
		}
		
		private function onTriggeredBtnLetterList(event:Event):void
		{
			Root.showLoadingSpinner(true);
			wordBorad.clearDrawing();
			
			selected_word_index = letterToggleGroup.selectedIndex;
			btnAllLetterList.removeEventListener(Event.TRIGGERED, onTriggeredBtnLetterList);
			//			this.dispatchEventWith(ViewControl.SHOW_LETTER_LIST_VIEW);
		}
	}
}