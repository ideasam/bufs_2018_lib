package pages.type
{
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	
	import app.AppSettings;
	import app.LocalSavedData;
	
	import component.ScrollSprite;
	import component.quiz.ItemQuiz;
	
	import feathers.controls.Button;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.skins.ImageSkin;
	
	import net.NetManager;
	
	import starling.display.Image;
	import starling.events.Event;
	
	import theme.CustomTheme;
	
	public class QuizPage extends BasePage
	{
		private var imgLoader:ImageLoader = null;
		private var btnSend:Button = null;
		private var yPos:int = 0;
		private var _dataGroup:Object;
		private var _groupWidth:int = 0;
		private var _labelGroupWidth:int = 0;
		private var _labelQuizWidth:int = 0;
		public static const ON_TAB_OPTION:String = "onTabOption";
		
		private var vecQuizzes:Vector.<ItemQuiz> = new Vector.<ItemQuiz>;
		private var jsonResults:Object = [];
		
		public function QuizPage()
		{
			super();
		}
		
		override public function loadContent():void
		{
			super.loadContent();
			
			_groupWidth = int(AppSettings.width * 0.9);
			_labelGroupWidth = int(AppSettings.width * 0.84);
			_labelQuizWidth = int(AppSettings.width * 0.80);
			
			imgLoader = new ImageLoader();
			imgLoader.source = new URLRequest("data/img/chapter"+idxChapter+"/page"+idxPage+".png").url;
			imgLoader.addEventListener(Event.COMPLETE, onTitleImageLoadComplete);
			scrollCont.addChild(imgLoader);
		}
		
		override public function unloadContent():void
		{
			removeChild(imgLoader);
			imgLoader.source = null;
			this.removeEventListener(ScrollSprite.ON_RESIZE_OFFSET, onResizeOffset);
			super.unloadContent();
		}
		
		override public function activate():void
		{
			trace("activate: Event.TRIGGERED, event: " + ON_TAB_OPTION + idxPage);
			this.addEventListener(ON_TAB_OPTION + idxPage, onTabOption);
			for(var i:int = 0; i < vecQuizzes.length; i++)
				vecQuizzes[i].activate();
		}
		
		override public function deactivate():void
		{
			trace("deactivate: Event.TRIGGERED, event: " + ON_TAB_OPTION + idxPage);
			this.removeEventListener(ON_TAB_OPTION + idxPage, onTabOption);
			for(var i:int = 0; i < vecQuizzes.length; i++)
				vecQuizzes[i].deactivate();
		}
		
		public function onTabOption(event:Event, data:Object):void
		{
			trace("QuizPage.ON_TAB_OPTION: idxPage:" + idxPage + " group:" + data.idGroup + ", quiz:" + data.idQuiz + ", option:" + data.idOption);			
			redrawOptions(data);
			redrawBtnSend(data);
		}
		
		public function redrawOptions(data:Object):void
		{
			for (var i:int = 0; i < vecQuizzes.length; i++)
			{
				var curItemQuiz:ItemQuiz = vecQuizzes[i];
				if (curItemQuiz.idGroup == data.idGroup && curItemQuiz.idQuiz == data.idQuiz)
				{
					curItemQuiz.isCheck = true;
					for (var j:int = 0; j < curItemQuiz.vecOptions.length; j++)
					{
						if (data.idOption != j)
							curItemQuiz.vecOptions[j].reset();
					}
					break;
				}
			}
		}
		
		public function redrawBtnSend(data:Object):void
		{
			var isbtnSendEnabled:Boolean = true;
			for (var i:int = 0; i < vecQuizzes.length; i++)
			{
				var curItemQuiz:ItemQuiz = vecQuizzes[i];
				if (false == curItemQuiz.isCheck)
				{
					isbtnSendEnabled = false;
				}
			}	
			btnSend.isEnabled = isbtnSendEnabled;
		}
		
		override public function draw():void
		{
			super.draw();
			drawQuiz(idxChapter, idxPage);
			
			btnSend = new Button();
			btnSend.defaultSkin = new Image(Root.assets.getTexture("quiz/btnSendN"));
			btnSend.disabledSkin = new Image(Root.assets.getTexture("quiz/btnSendD"));
			btnSend.downSkin = new Image(Root.assets.getTexture("quiz/btnSendS"));
			btnSend.addEventListener(Event.TRIGGERED, onTriggered);
			btnSend.validate();
			btnSend.x = (AppSettings.width - btnSend.width) >> 1;
			btnSend.y = yPos + (btnSend.height >> 1);
			btnSend.isEnabled = false;
			scrollCont.addChild(btnSend);
			
			var imgAlpha:Image = new Image(Root.assets.getTexture("alphaBox"));
			imgAlpha.y = btnSend.y + btnSend.height * 1.5;
			scrollCont.addChild(imgAlpha);
		}
		
		private function onTriggered(event:Event):void
		{			
			for (var i:int = 0; i < vecQuizzes.length; i++)
			{
				var curItemQuiz:ItemQuiz = vecQuizzes[i];
				for (var j:int = 0; j < curItemQuiz.vecOptions.length; j++)
				{
					if (!jsonResults[curItemQuiz.idGroup])
					{
						jsonResults[curItemQuiz.idGroup] = [];
					}
					
					if (curItemQuiz.vecOptions[j].isSelected && curItemQuiz.vecOptions[j].isCorrect)
					{
						jsonResults[curItemQuiz.idGroup][curItemQuiz.idQuiz] = 1;
						break;
					}
					else
					{
						jsonResults[curItemQuiz.idGroup][curItemQuiz.idQuiz] = 0;
					}
				}
			}
			
			NetManager.sendQuizResults(
				AppSettings.SUBJECT_CODE,
				LocalSavedData.getUserData().uid,
				String(idxChapter),
				String(idxPage-1),
				JSON.stringify(jsonResults), onCompleteSend, onFailSend);
		}
		
		private function onCompleteSend():void
		{
			for (var i:int = 0; i < vecQuizzes.length; i++)
			{
				var curItemQuiz:ItemQuiz = vecQuizzes[i];
				
				if (1 == jsonResults[i])
				{
					curItemQuiz.showMarking(true);
				}
				else
				{
					curItemQuiz.showMarking(false);
				}
				
				for (var j:int = 0; j < curItemQuiz.vecOptions.length; j++)
				{
					curItemQuiz.vecOptions[j].touchable = false;
				}
			}
			btnSend.isEnabled = false;
		}
		
		private function onFailSend(event:*=null):void
		{
			trace("onFailSend: " );	
		}
		
		public function onTitleImageLoadComplete( evt:Event ):void
		{
			draw();
		}
		
		private function onResizeOffset(event:Event, offset:int):void
		{
			trace("QuizPage - onResizeOffset: " + offset);
		}
		
		private function drawQuiz(idxChapter:int, idxPage:int):void
		{
			var json:Object = Root.assets.getObject("quiz");
			var dataChapter:Object = json["chapter"+idxChapter];
			
			yPos = imgLoader.originalSourceHeight;
			
			for(var i:int = 0; i < dataChapter.pages.length; i++)
			{
				var dataPage:Object = dataChapter.pages[i];
				if(dataPage.index == idxPage)
				{
					var cnt:int = 0;
					for (var j:int = 0; j < dataPage.groups.length; j++)
					{
						_dataGroup = dataPage.groups[j];
						itemGroupCreate(_dataGroup, cnt);
						if (j != dataPage.groups.length - 1)
							yPos += 20;
						cnt++;
					}
				}
			}
			scrollCont.readjustLayout();
		}
		
		private function itemGroupCreate(dataGroup:Object, idGroup:int):void
		{
			var bgGrouop:ImageSkin = new ImageSkin(Root.assets.getTexture("quiz/groupBox"));
			bgGrouop.scale9Grid = new Rectangle(20,20,10,10);
			bgGrouop.width = _groupWidth;
			bgGrouop.x = (AppSettings.width - _groupWidth) >> 1;
			bgGrouop.y = yPos;
			bgGrouop.alpha = 0.8;
			scrollCont.addChild(bgGrouop);
			
			yPos += 30;
			var labelGroup:Label = new Label();
			labelGroup.wordWrap = true;
			labelGroup.width = _labelGroupWidth;
			labelGroup.fontStyles = CustomTheme.fsDark.clone();
			labelGroup.x = (AppSettings.width - _labelGroupWidth) >> 1;
			labelGroup.y = yPos;
			labelGroup.text = (idGroup + 1) + ". " + dataGroup.title;
			labelGroup.touchable = false;
			
			scrollCont.addChild(labelGroup);
			labelGroup.validate();
			
			yPos += labelGroup.height + 25;
			for (var idQuiz:int = 0; idQuiz < dataGroup.quizzes.length; idQuiz++)
			{
				var dataQuiz:Object = dataGroup.quizzes[idQuiz];
				vecQuizzes.push(new ItemQuiz());
				yPos = vecQuizzes[vecQuizzes.length-1].create(this, idGroup, idQuiz, dataQuiz, _labelQuizWidth, yPos);				
			}
			bgGrouop.height = yPos - bgGrouop.y;
		}
	}
}