package net
{
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import starling.utils.SystemUtil;
	
	import utils.DeviceInfo;
	
	public class NetManager
	{
//		public static var URL:String = "http://admin.dseas.kr:9090/";
		public static var URL:String = "http://52.79.55.33:9090/";
		
		public function NetManager()
		{
		}
		
		public static function login(subjectCode:String, uid:String, upass:String, onComplete:Function, onError:Function):void
		{
			var url:String = NetManager.URL+"login";
			
			var request:URLRequest = new URLRequest(url);
			var requestVars:URLVariables = new URLVariables();
			
			requestVars.data = '{"subjectCode":"'+subjectCode+'", "id":"' + uid + '", "email":"' + upass + '"}';
			//requestVars.data = '{"subject":"'+subjectCode+'", "query":{"_id":"' + uid + '","email":"' + upass + '"}}';
			request.data = requestVars;
			request.method = URLRequestMethod.POST;
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader = new URLLoader();
			urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
			urlLoader.addEventListener(Event.COMPLETE, receiveJsonLoginResult, false, 0, true);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, receiveJsonLoginError, false, 0, true);
			
			for (var prop:String in requestVars) {
				trace("Sent: " + prop + " is: " + requestVars[prop]);
			}
			
			try {
				urlLoader.load(request);
			}
			catch (e:Error)
			{
				//txtMessage.text = e.message;
			}
			
			function receiveJsonLoginResult(event:Event):void
			{
				trace("receiveJsonLoginResult:", event.target.data);
				if ("success" == event.target.data) onComplete();
				else onError();//txtMessage.text = "로그인 정보를 확인하십시오.";
			}
			
			function receiveJsonLoginError(event:IOErrorEvent):void
			{
				//txtMessage.text = "receiveJsonLoginError: " + event.errorID.toString() + ", " + event.target.data;
				onError("receiveJsonLoginError: " + event.errorID.toString() + ", " + event.target.data);
			}
		}
		
		/**
		/saveanswer
		data = {"query":{"subjectCode":"vie130","id":"19991999","ch":0,"no":0}, "answer":"this is answer" }
			// 저장하고 리턴값  - 'success' or 'failure'
		**/
		
		public static function sendQuizResults(subjectCode:String, uid:String, ch:String, page:String, results:Object, onComplete:Function, onFail:Function):void
		{
			if(SystemUtil.isDesktop != true)
			{
				var request:URLRequest = new URLRequest(URL +"savequizresults");
				var requestVars:URLVariables = new URLVariables();
				
				requestVars.data = 
					'{ "query":{"subjectCode":"'+subjectCode+'", "id":"' + uid + '", "ch":'+ch+', "page":'+page+'}' +
					', "results":'+results+
					'}';
				
				request.data = requestVars;
				request.method = URLRequestMethod.POST;
				
				var urlLoader:URLLoader = new URLLoader();
				urlLoader = new URLLoader();
				urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
				urlLoader.addEventListener(Event.COMPLETE, onCompleteUpdateTestResult, false, 0, true);
				urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onError, false, 0, true);
				
				for (var prop:String in requestVars) {
					trace("Sent: " + prop + " is: " + requestVars[prop]);
				}
				
				try {
					urlLoader.load(request);
				}
				catch (e:Error)
				{
					onFail();
				}
				
				function onCompleteUpdateTestResult(event:*):void
				{
					//trace("onCompleteGetTestUserAnswer:", event.target.data);
					if(event.target.data == "success") onComplete();
					else onFail();
				}
			}
		}
		
		public static function updateTestResult(uid:String, ch:String, no:String, answer:String, onComplete:Function, onFail:Function):void
		{
			if(SystemUtil.isDesktop != true)
			{
				var appId:String = DeviceInfo.appId;
				var subjectCode:String = appId.substring(appId.indexOf("bufs")+5);
				
				var request:URLRequest = new URLRequest(URL +"saveanswer");
				var requestVars:URLVariables = new URLVariables();
				
				requestVars.data = 
					'{ "query":{"subjectCode":"'+subjectCode+'", "id":"' + uid + '", "ch":'+ch+', "no":'+no+'}' +
					', "answer":"'+answer+'"'+	
					'}';
				
				request.data = requestVars;
				request.method = URLRequestMethod.POST;
				
				var urlLoader:URLLoader = new URLLoader();
				urlLoader = new URLLoader();
				urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
				urlLoader.addEventListener(Event.COMPLETE, onCompleteUpdateTestResult, false, 0, true);
				urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onError, false, 0, true);
				
				for (var prop:String in requestVars) {
					trace("Sent: " + prop + " is: " + requestVars[prop]);
				}
				
				try {
					urlLoader.load(request);
				}
				catch (e:Error)
				{
					onFail();
				}
				
				function onCompleteUpdateTestResult(event:*):void
				{
					//trace("onCompleteGetTestUserAnswer:", event.target.data);
					if(event.target.data == "success") onComplete();
					else onFail();
				}
			}
			else
			{
				try
				{
					ExternalInterface.addCallback("fromJS_saveanswer", fromJS_saveanswer);
				} 
				catch(error:Error) 
				{
					navigateToURL(new URLRequest("javascript:alert('Fail');"));
					
					onFail();
				}
				
				ExternalInterface.call("toJavascriptSaveAnswer", uid, ch, no, answer);
			}
			
			function fromJS_saveanswer(text:String):void 
			{
				trace("fromJS_saveanswer:", text);
				
				if(text == "success" || text == '"success"')
				{
					onComplete();
				}
				else 
				{
					onFail();
				}
			}
		}
		
		//data = {"subjectCode":"vie130","id":"19991999","ch":0}
		public static function getTestUserAnswer(uid:String, ch:String, onComplete:Function, onFail:Function):void
		{
			if(SystemUtil.isDesktop != true)
			{
				var appId:String = DeviceInfo.appId;
				var subjectCode:String = appId.substring(appId.indexOf("bufs")+5);
				
				var request:URLRequest = new URLRequest(URL +"getanswer");
				var requestVars:URLVariables = new URLVariables();
				
				requestVars.data = 
					'{ "subjectCode":"'+subjectCode+'", "id":"' + uid + '", "ch":'+ch+'}';
				
				request.data = requestVars;
				request.method = URLRequestMethod.POST;
				
				var urlLoader:URLLoader = new URLLoader();
				urlLoader = new URLLoader();
				urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
				urlLoader.addEventListener(Event.COMPLETE, onCompleteGetTestUserAnswer, false, 0, true);
				urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onError, false, 0, true);
				
				for (var prop:String in requestVars) {
					trace("Sent: " + prop + " is: " + requestVars[prop]);
				}
				
				try {
					urlLoader.load(request);
				}
				catch (e:Error)
				{
					onFail();
				}
				
				
				function onCompleteGetTestUserAnswer(event:*):void
				{
					trace("onCompleteGetTestUserAnswer:", event.target.data);
					onComplete(event.target.data)
				}
			}
			else
			{
				try
				{
					ExternalInterface.addCallback("fromJS_getanswer", fromJS_getanswer);
				} 
				catch(error:Error) 
				{
					trace("error:",error);
					navigateToURL(new URLRequest("javascript:alert('Fail');"));
				}
				
				ExternalInterface.call("toJavascriptGetAnswer", uid, ch);
				
			}
			
			function fromJS_getanswer(text:String=null):void 
			{
				trace("fromJS_getanswer:", text);
				
				if(text)
				{
					onComplete(text);
				}
				else 
				{
					onFail();
				}
			}
		}
		
		public static function updateEduProgress(subjectCode:String, uid:String, ch:String, page:String, startTime:String, endTime:String):void
		{
			//아직 안열었음
			//return;
			
			trace("uid:" + uid + ", ch:" + ch + ", page:" + page + ", startTime: " + startTime + ", endTime:" + endTime);
			
			//if(CONFIG::mobile == true)
			if(SystemUtil.isDesktop != true)
			{	
				var request:URLRequest = new URLRequest(URL +"savedt");
				var requestVars:URLVariables = new URLVariables();
				
				
				//				requestVars.data = 
				//				'{ "subjectCode":"'+subjectCode+'", "id":"' + uid + '", "query":[' +
				//				'{"ch":0, "page":0, "st":1424806626208, "et":1424806626218},' +
				//				'{"ch":0, "page":1, "st":1424806626208, "et":1424806626228},' +
				//				'{"ch":0, "page":2, "st":1424806626208, "et":1424806626238}' +
				//				']}';
				
				requestVars.data = 
					'{ "subjectCode":"'+ subjectCode +'", "id":"' + uid + '", "query":[' +
					'{"ch":'+ch+', "page":'+page+', "st":'+startTime+', "et":'+endTime+'}' +
					']}';
				
				
				request.data = requestVars;
				request.method = URLRequestMethod.POST;
				
				var urlLoader:URLLoader = new URLLoader();
				urlLoader = new URLLoader();
				urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
				urlLoader.addEventListener(Event.COMPLETE, onComplete, false, 0, true);
				urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onError, false, 0, true);
				
				for (var prop:String in requestVars) {
					trace("Sent: " + prop + " is: " + requestVars[prop]);
				}
				
				try {
					urlLoader.load(request);
				}
				catch (e:Error)
				{
					
				}
			}
			else
			{
				try
				{
					ExternalInterface.addCallback("fromJS_savedt", fromJavascriptResult);
				} 
				catch(error:Error) 
				{
					navigateToURL(new URLRequest("javascript:alert('Fail');"));
				}
				
				
				ExternalInterface.call("toJavascriptSavedt", uid, ch, page, startTime ,endTime);
			}
			
			function fromJavascriptResult(text:String):void 
			{
				trace("fromJS_savedt:", text);
			}
		}
		
		protected static function onError(event:IOErrorEvent):void
		{
			trace("onError:", event.errorID, event.type);
		}
		
		protected static function onComplete(event:Event):void
		{
			// TODO Auto-generated method stub
			//trace("onComplete:", event.target.data);
		}
	}
}