package
{
	public class EmbeddedAssets
	{
		[Embed(source="assets/atlas/atlas.xml", mimeType="application/octet-stream")]
		public static const atlas_xml:Class;
		
		[Embed(source="assets/atlas/atlas.png")]
		public static const atlas:Class;
	}
}