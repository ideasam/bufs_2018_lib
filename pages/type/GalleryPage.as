package pages.type
{
	import com.greensock.TweenMax;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.DataLoader;
	import com.greensock.loading.LoaderMax;
	
	import flash.events.KeyboardEvent;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.ui.Keyboard;
	
	import app.AppSettings;
	
	import component.RectItem;
	
	import feathers.controls.Button;
	import feathers.controls.DecelerationRate;
	import feathers.controls.ImageLoader;
	import feathers.controls.ScrollContainer;
	import feathers.controls.ScrollText;
	import feathers.layout.HorizontalAlign;
	
	import net.NetManager;
	
	import pages.PageContent;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.filters.GlowFilter;
	import starling.text.TextFormat;
	
	import theme.CustomTheme;
	
	import utils.TouchSheet;

	public class GalleryPage extends BasePage
	{
		private var _loaderQueue:LoaderMax = null;
		private var _dataLoaderJsonRect:DataLoader = null;
		private var _dataLoaderJsonGRTxt:DataLoader = null;
		
		
		private var imgLoader:ImageLoader = null;
		private var scrollCont:ScrollContainer;
		private var galleryCont:Sprite = null;
		private var bgGallery:Quad;
		private var bgLabelGallery:Image;
		private var btnGalleryClose:Button;
		
		private var remoteImage:ImageLoader = null;
		private var touchSheet:TouchSheet = null;
		
		private var prevTouchIdx:int = -1;
		private var vecSoundItem:Vector.<RectItem>= null;
		private var vecGalleryText:Vector.<String>= null;
		private var labelGallery:ScrollText;
		
		public function GalleryPage()
		{
			super();
		}
		
		override public function draw():void
		{
			
			scrollCont = new ScrollContainer();
			scrollCont.decelerationRate = DecelerationRate.FAST;
			scrollCont.minimumDragDistance = 0.2;
			scrollCont.width = AppSettings.width;
			scrollCont.height = AppSettings.height;
			this.addChild(scrollCont);
			
			imgLoader = new ImageLoader();
			scrollCont.addChild(imgLoader);
			
			galleryCont = new Sprite();
			//this.addChild(galleryCont);
			Starling.current.stage.addChild(galleryCont);
			galleryCont.visible = false;
			
			bgGallery = new Quad(AppSettings.width, AppSettings.height, 0x000000);
			bgGallery.alpha = 0.3;
			//bgGallery.visible = false;
			galleryCont.addChild(bgGallery);
			
			touchSheet = new TouchSheet();
			touchSheet.addEventListener(TouchSheet.ON_TAB_ONCE, onTouchSheetTab);
			galleryCont.addChild(touchSheet);
			
			bgLabelGallery = new Image(Root.assets.getTexture("bottomLabelBg"));
			bgLabelGallery.height = 200;
			bgLabelGallery.y = AppSettings.height- bgLabelGallery.height;
			//bgLabelGallery.alpha = 0.7;
			//galleryCont.addChild(bgLabelGallery);
			
			labelGallery = new ScrollText();
			labelGallery.backgroundSkin = new Image(Root.assets.getTexture("bottomLabelBg"));
			labelGallery.fontStyles = new TextFormat(CustomTheme.FONT_NAME, 24, 0xffffff, HorizontalAlign.LEFT, "center");
			labelGallery.height = 200;//bgLabelGallery.height;
			labelGallery.width = AppSettings.width;
			//labelGallery.wordWrap = true;
			labelGallery.padding = 20;
			labelGallery.y = bgLabelGallery.y+10;
			galleryCont.addChild(labelGallery);
			
			btnGalleryClose = new Button();
			btnGalleryClose.x = AppSettings.width - 120;
			btnGalleryClose.y = 10;
			btnGalleryClose.defaultSkin = new Image(Root.assets.getTexture("btnExitN"));
			galleryCont.addChild(btnGalleryClose);
			
			btnGalleryClose.addEventListener(Event.TRIGGERED, onCloseGallery);
			
			super.draw();
		}
		
		private function onTouchSheetTab():void
		{
			//trace("onTouchSheetTab");
			/*if(labelGallery.alpha == 0)
			{
				trace("A");
				TweenMax.to(labelGallery, 0.2, {alpha:1});
				labelGallery.alpha =1;
			}
			else
			{
				trace("B");
				TweenMax.to(labelGallery, 0.2, {alpha:0});
				
				labelGallery.alpha =0;
				//labelGallery.visible = false;
			}*/
			
			labelGallery.visible = !labelGallery.visible;
		}
		
		override public function loadContent():void
		{
			imgLoader.addEventListener(Event.COMPLETE, onCompleteLoad);
			imgLoader.source = new URLRequest("data/img/chapter"+idxChapter+"/page"+idxPage+".png").url;
			
			if(!_dataLoaderJsonRect)
			{
				_dataLoaderJsonRect = new DataLoader("data/rect/ch"+idxChapter+"_"+idxPage+".txt", {name:TAG_RECT});
				_dataLoaderJsonGRTxt = new DataLoader("data/txt/ch"+idxChapter+"_"+idxPage+".txt", {name:TAG_GR_TEXT});
				_loaderQueue = new LoaderMax({name:"mainQueue", onComplete:completeHandler});
				_loaderQueue.append(_dataLoaderJsonRect);
				_loaderQueue.append(_dataLoaderJsonGRTxt);
				_loaderQueue.load();
			}
			
			super.loadContent();
		}
		
		private function completeHandler(event:LoaderEvent):void 
		{
			var strJsonRect:String = LoaderMax.getContent(TAG_RECT);
			var strJsonGrTxt:String = LoaderMax.getContent(TAG_GR_TEXT);
			
			if(!strJsonRect || !strJsonGrTxt) return;
			
			scrollCont.addChildAt(rectEffect,0);
			
			var jsData:Object = JSON.parse(strJsonRect);
			var jsDataTxtGallery:Object = JSON.parse(strJsonGrTxt);
			
			if(!vecSoundItem) vecSoundItem = new Vector.<RectItem>;
			if(!vecGalleryText) vecGalleryText = new Vector.<String>;
			
			var len:int = jsData.rects.length;
			for(var i:int=0; i<len;++i)
			{
				vecSoundItem.push(new RectItem(new Rectangle(jsData.rects[i].x, jsData.rects[i].y, jsData.rects[i].width,jsData.rects[i].height)));
				vecGalleryText.push(jsDataTxtGallery.gallery[i].content);
			}
		
			scrollCont.addEventListener(TouchEvent.TOUCH, onTouchCont);
		}
		
		private function onCompleteLoad():void
		{
			imgLoader.removeEventListener(Event.COMPLETE, onCompleteLoad);
			imgLoader.validate();
			
			imgLoader.visible = true;
						
			this.endIndicator.y = imgLoader.y + imgLoader.height+20;
			scrollCont.addChild(endIndicator);
		}
		
		
		private function onTouchCont(event:TouchEvent):void
		{
			if(scrollCont.isScrolling || PageContent.isContentScrolling) return;
			
			var touch:Touch = event.getTouch(imgLoader);
			
			if(touch)
			{
				switch(touch.phase)
				{
					case TouchPhase.BEGAN:
						prevTouchIdx = getTouchAreaIdx();
						break;
					
					case TouchPhase.ENDED:
						
						var curTouchIdx:int = getTouchAreaIdx();
						if(prevTouchIdx > -1 && curTouchIdx>-1)
						{
							rectEffect.filter = null;
							
							rectEffect.x = vecSoundItem[curTouchIdx].bounds.x;
							rectEffect.y = vecSoundItem[curTouchIdx].bounds.y;
							rectEffect.width = vecSoundItem[curTouchIdx].bounds.width;
							rectEffect.height = vecSoundItem[curTouchIdx].bounds.height;
							
							rectEffect.filter = new GlowFilter(0x29CDC8,0.8,8);
							rectEffect.visible = true;
							
							drawGallery(curTouchIdx);
						}
						
						prevTouchIdx = -1;
						break;
				}
			}
			
			function getTouchAreaIdx():int
			{
				var len:int =vecSoundItem.length;
				for (var i:int=0; i<len; ++i)
				{
					if(vecSoundItem[i].bounds.containsPoint(touch.getLocation(imgLoader)))
					{
						return i;
					}
				}
				return -1;
			}
		}
		
		private function drawGallery(idx:int):void
		{
			Starling.current.nativeStage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			
			dispatchEventWith(BasePage.TOGGLE_BTN_ARROW, true, false);
			
			rectEffect.visible = false;
			rectEffect.filter = null;
			TweenMax.to(imgLoader, 0.2, {alpha:0.3});
			TweenMax.to(bgGallery, 0.2, {alpha:0.8});
			
			remoteImage = new ImageLoader();
			remoteImage.source = NetManager.FILE_URL + "chapter"+idxChapter+"/gallery/gal"+idx+".jpg";
			remoteImage.addEventListener(Event.COMPLETE, onLoadRemoteImageComplete);
			galleryCont.addChild(remoteImage);
			
			galleryCont.visible = true;
			
			labelGallery.visible = true;
			labelGallery.text = vecGalleryText[idx]; 
		}
		
		private function onLoadRemoteImageComplete(event:*):void
		{
			remoteImage.alpha = 0.0;
			remoteImage.scale = 0.5;
			remoteImage.validate();
			
			TweenMax.to(remoteImage, 0.2, {alpha:1});
			touchSheet.content = remoteImage;
		}
		
		protected function onKeyDown(event:KeyboardEvent):void
		{
			if(event.isDefaultPrevented())
			{
				//someone else already handled this one
				return;
			}
			if(remoteImage && remoteImage.alpha != 1) return;
			
			if(event.keyCode == Keyboard.BACK || Keyboard.BACKSPACE)
			{
				Starling.current.nativeStage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
				event.preventDefault();
				
				onCloseGallery();
			}
		}
		
		private function onCloseGallery():void
		{
			TweenMax.to(galleryCont, 0.2, {alpha:0, onComplete:function():void{
				touchSheet.content = null;
				remoteImage.source =null;
				
				remoteImage.removeFromParent(true);
				galleryCont.visible = false;
				galleryCont.alpha = 1;
				galleryCont.width = AppSettings.width;
				bgGallery.alpha = 0.3;
			}});
			
			TweenMax.to(imgLoader, 0.2, {alpha:1.0});
			
			dispatchEventWith(BasePage.TOGGLE_BTN_ARROW, true, true);
		}
		
		override public function unloadContent():void
		{
			super.unloadContent();
			
			if(remoteImage) remoteImage.source  =null;
			//bgGallery.removeFromParent(true);
			
			imgLoader.source = null;
		}
	}
}