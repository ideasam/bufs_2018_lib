package pages
{
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import app.AppSettings;
	import app.LocalSavedData;
	
	import component.ChapterList;
	import component.HeaderContainer;
	
	import feathers.controls.Button;
	import feathers.controls.Drawers;
	import feathers.controls.StackScreenNavigator;
	import feathers.controls.StackScreenNavigatorItem;
	import feathers.motion.Fade;
	
	import pages.data.StudyInfo;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;

	public class PageNavigator extends Drawers
	{
		//LOGIN
		public static const LOGIN_PAGE		:String = "showIntroPage";
		
		//CONTENT
		public static const CONTENT_PAGE	:String = "showContentPage";
		
		//EVENT
		public static const ON_EXPANDING_HEAD		:String = "onExpandingHead";
		public static const ON_TOUCH_HEAD_BTN		:String = "onTouchHeadBtn";
		public static const ON_SCROLL_HEAD_CHANGE	:String = "onScrollHeadChange";
		
		public static var headerCont:HeaderContainer;
		public var chapterList:ChapterList;
		
		private var bgQuad:Quad;
		public var _navigator:StackScreenNavigator;
		
		public function PageNavigator()
		{
			super();
		}
		
		override protected function initialize():void
		{
			super.initialize();
		
			_navigator = new StackScreenNavigator();
			AppSettings.pageNavigator = this;
			
			this.content = _navigator;
			
			var loginItem:StackScreenNavigatorItem = new StackScreenNavigatorItem(PageLogin);
			loginItem.setScreenIDForPushEvent(CONTENT_PAGE, CONTENT_PAGE);
			_navigator.addScreen(LOGIN_PAGE, loginItem);
			
			var contentItem:StackScreenNavigatorItem = new StackScreenNavigatorItem(PageContent);
			_navigator.addScreen(CONTENT_PAGE, contentItem);
			
			if(LocalSavedData.getUserData().uid != null)
			{
				LocalSavedData.isAlreadyLogin = true;
				_navigator.rootScreenID = CONTENT_PAGE;
				
			}
			else
			{
				_navigator.rootScreenID = LOGIN_PAGE;
			}			
			_navigator.pushTransition = Fade.createFadeInTransition(1.5);
			
			bgQuad = new Quad(AppSettings.width, AppSettings.height, 0xe0e0e0);
			bgQuad.alpha = 0.0;
			bgQuad.touchable = false;
			this.addChild(bgQuad);
			
			drawChapterList();
			drawHeader(this);
			
			this.addEventListener(PageEvent.LOADED, onCompleteTransition);
			this.addEventListener(TouchEvent.TOUCH, onTouch);
			headerCont.dispatchEventWith(HeaderContainer.SET_MENU, false, chapterList);
			headerCont.dispatchEventWith(HeaderContainer.SET_TITLES, false, StudyInfo.currentChapter);
			
			this.addEventListener(PageNavigator.ON_EXPANDING_HEAD, onHeadExpand);
			this.addEventListener(PageNavigator.ON_TOUCH_HEAD_BTN, onTouchHeadBtn);
			this.addEventListener(PageNavigator.ON_SCROLL_HEAD_CHANGE, onScrollHeadChange);
		}
		
		public function onScrollHeadChange(event:Event, title:String):void
		{
			headerCont.setIndicator(title);
		}
		
		//Head 확장 버튼을 누르면 동작!
		public function onTouchHeadBtn(event:Event, title:String):void
		{
			var len:int = AppSettings.PAGE_CATEGORY_TITLES[StudyInfo.currentChapter].length;
			for (var iPage:int = 0; iPage < len; iPage++)
			{
				if (AppSettings.PAGE_CATEGORY_TITLES[StudyInfo.currentChapter][iPage] == title)
				{
					var pageContent:PageContent = _navigator.activeScreen as PageContent;
					pageContent.scrollToPageIndex(iPage, 0, 0.3);
					break;
				}
			}
		}
		
		public function onHeadExpand(event:Event, isWillExpand:Boolean):void
		{
			if (isWillExpand)
			{
				TweenLite.to(_navigator, 0.3, { y:headerCont.getOriginHeight()*2, onComplete:contentResize });
			}
			else
			{
				contentResize();
				TweenLite.to(_navigator, 0.3, { y:headerCont.getOriginHeight() });
			}
		}
		
		private function contentResize(isWillDisappearHead:Boolean = false):void {
			var heightHead:int = 0;
			if (!isWillDisappearHead)
				heightHead = (headerCont.isExpanded) ? headerCont.getOriginHeight()*2 : headerCont.getOriginHeight();
			
			_navigator.setSize(AppSettings.width, AppSettings.height - heightHead);
			var contentItem:StackScreenNavigatorItem = _navigator.getScreen(CONTENT_PAGE);
			var pageContent:PageContent = contentItem.getScreen() as PageContent;
			pageContent.resizeScreen(AppSettings.width, AppSettings.height - heightHead);
		}
		
		private var tutorCont0:Sprite;
		
		private function showTutor():void
		{
			tutorCont0 = new Sprite();
			tutorCont0.touchable = false;
			this.addChild(tutorCont0);
			
			var bgT0Quad:Quad = new Quad(AppSettings.width, AppSettings.height, 0x000000);
			bgT0Quad.alpha = 0.6;
			bgT0Quad.touchable = false;
			tutorCont0.addChild(bgT0Quad);
			
			var tutorImg0:Image = new Image(Root.assets.getTexture("effect/tutor0"));
			tutorImg0.x = (AppSettings.width - tutorImg0.width)>>1;
			tutorImg0.y = (AppSettings.height - tutorImg0.height)>>1;
			tutorCont0.addChild(tutorImg0);			
		}
		
		private function onCompleteTransition():void
		{
			if(!LocalSavedData.isAlreadyLogin && _navigator.activeScreenID == CONTENT_PAGE)
			{
				LocalSavedData.isAlreadyLogin = true;
				showTutor();
			}
			
			if (LocalSavedData.isAlreadyLogin)
			{
				TweenMax.to(chapterList, 0.5, {y:-AppSettings.height, onComplete:function():void{
					chapterList.visible = false;
					touchable = chapterList.touchable = true;
				}});
				TweenLite.to(_navigator, 0.3, { y:headerCont.getOriginHeight()*2 });
				TweenMax.to(headerCont, 0.5, {
					y: 0,
					onComplete: function ():void {
						headerCont.dispatchEventWith(HeaderContainer.SET_TITLES, false);
						headerCont.expansion();
						contentResize();
					}
				});
				headerCont.showBtnExpansion();
				TweenMax.to(bgQuad, 0.5, {alpha:0});
			}
		}
		
		private function drawChapterList():void
		{
			chapterList = new ChapterList();
			chapterList.y = 80;//headerCont.height;
			chapterList.visible = false;
			this.addChild(chapterList);			
			chapterList.addEventListener(Event.CHANGE, onTabChapterList);
		}
		
		private function onTabChapterList(event:Event, idxChapter:int):void
		{
			this.touchable = chapterList.touchable = false;
			var pageContent:PageContent = _navigator.activeScreen as PageContent;
			
			if (pageContent)
				pageContent.destroyPages();
			
			_navigator.popAll();
			StudyInfo.currentChapter = idxChapter;
			LocalSavedData.latestStudyChapter = StudyInfo.currentChapter;
			_navigator.pushScreen(CONTENT_PAGE);
		}
		
		private function drawHeader(pageNavigator:PageNavigator):void
		{
			headerCont = new HeaderContainer();
			this.stage.addChild(headerCont);
			headerCont.y = -80;
			contentResize(true);
			headerCont.addEventListener(HeaderContainer.TAP_MENU, onTriggredMenu);
		}
		
		private function onTriggredMenu():void
		{
			if(TweenMax.isTweening(chapterList)) return;
			
			if(!chapterList.visible)
			{
				chapterList.y = -chapterList.height;
				headerCont.contraction();
				TweenMax.to(chapterList, 0.5, { y:headerCont.getOriginHeight() });
				headerCont.hideBtnExpansion();
				
				chapterList.visible = true;
				TweenMax.to(bgQuad, 0.5, {alpha:1});
			}
			else
			{
				TweenMax.to(chapterList, 0.5, {y:-chapterList.height, onComplete:function():void{
					chapterList.visible = false;
				}});
				headerCont.showBtnExpansion();
				TweenMax.to(bgQuad, 0.5, {alpha:0});
			}
		}		
		
		private function onTouch(touchEvent:TouchEvent):void
		{
			var touch:Touch = touchEvent.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				var localPos:Point = touch.getLocation(this);
				
				if(touch.tapCount == 2)
				{
					if(_navigator.activeScreenID == LOGIN_PAGE || (touch.target as Button)) return;
					
					if(chapterList.visible) return;
					
					//Tutorial
					if(tutorCont0 && tutorCont0.visible)
					{
						tutorCont0.visible = false;
						tutorCont0.removeFromParent(true);
						LocalSavedData.isAlreadyLogin = true;
					}
					
					if(headerCont.y == 0)
					{
						headerCont.contraction();
						contentResize(true);
						TweenMax.to(headerCont, 0.3, {y:-80});
						TweenLite.to(_navigator, 0.3, {y: 0});
					}
					else
					{
						TweenMax.to(headerCont, 0.3, {y:0});
						TweenLite.to(_navigator, 0.3, { 
							y: (headerCont.isExpanded) ? headerCont.getOriginHeight()*2 : headerCont.getOriginHeight(),
							onComplete: contentResize
						});
					}
				}
			}
		}
	}
}