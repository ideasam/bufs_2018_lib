package utils
{
    import flash.geom.Point;
    import flash.geom.Rectangle;
    
    import app.AppSettings;
    
    import starling.animation.Tween;
    import starling.core.Starling;
    import starling.display.DisplayObject;
    import starling.display.Sprite;
    import starling.events.Touch;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;
    import starling.utils.RectangleUtil;
    import starling.utils.deg2rad;
    import starling.utils.rad2deg;

    public class TouchSheet extends Sprite
    {
		public static const ON_TAB_ONCE:String = "onTabOnce";
		
		public var defWidth:int = 640;
		public var maxScale:int = 3;
		private var _contents:DisplayObject;
		
        public function TouchSheet(contents:DisplayObject=null)
        {
            addEventListener(TouchEvent.TOUCH, onTouch);
            useHandCursor = true;
            
            if (contents)
            {
				_contents = contents;
				_contents.x = 0;//int(contents.width / -2);
				_contents.y = int(AppSettings.height - _contents.height)>>1//int(contents.height / -2);
				addChild(_contents);
            }
        }
        
		public function set content(contents:DisplayObject):void
		{
			if(_contents)
			{
				_contents.scale = 1;
				_contents.removeFromParent(true);
				_contents = null;
				
				this.x = this.y = this.pivotX = this.pivotY = 0;
				this.scale = 1;
			}
			
			if (contents)
			{
				_contents = contents;
				
				_contents.x = 0;
				_contents.y = int(AppSettings.height - _contents.height)>>1
				addChild(_contents);
			}
		}
		
		private var prvY:Number = 0;
		private var isMoved:Boolean = false;
        private function onTouch(event:TouchEvent):void
        {
            var touches:Vector.<Touch> = event.getTouches(this, TouchPhase.MOVED);
            
            if (touches.length == 1)
            {
                // one finger touching -> move
                var delta:Point = touches[0].getMovement(parent);
				
				var resultX:Number = x + delta.x;
				if(resultX  <= pivotX*scale && (this.width - pivotX*scale) + resultX > defWidth)
					x += delta.x;
				
				//if(this.height > AppSettings.height)
				if(scale > 1)
				{
					var contBound:Rectangle = _contents.getBounds(parent);
					var nBound:Rectangle =new Rectangle(contBound.x, contBound.y+delta.y, contBound.width, contBound.height);
					
					if(nBound.intersects(new Rectangle(0,400,AppSettings.width, AppSettings.height-800)))
						y += delta.y;
				}
				
				isMoved = true;
            }            
            else if (touches.length == 2)
            {
                // two fingers touching -> rotate and scale
                var touchA:Touch = touches[0];
                var touchB:Touch = touches[1];
                
                var currentPosA:Point  = touchA.getLocation(parent);
                var previousPosA:Point = touchA.getPreviousLocation(parent);
                var currentPosB:Point  = touchB.getLocation(parent);
                var previousPosB:Point = touchB.getPreviousLocation(parent);
                
                var currentVector:Point  = currentPosA.subtract(currentPosB);
                var previousVector:Point = previousPosA.subtract(previousPosB);
                
                var currentAngle:Number  = Math.atan2(currentVector.y, currentVector.x);
                var previousAngle:Number = Math.atan2(previousVector.y, previousVector.x);
                var deltaAngle:Number = currentAngle - previousAngle;

                // update pivot point based on previous center
                var previousLocalA:Point  = touchA.getPreviousLocation(this);
                var previousLocalB:Point  = touchB.getPreviousLocation(this);
				
				var pvX:Number = (previousLocalA.x + previousLocalB.x) * 0.5;
				var pvY:Number = (previousLocalA.y + previousLocalB.y) * 0.5;
				
				if(pvX < 0) pvX = 0;
				
               pivotY = pvY;

                // update location based on the current center
				var pX:Number = (currentPosA.x + currentPosB.x) * 0.5;
				
				var sW:int = this.width - Math.abs(Math.abs(pvX*scale) -Math.abs(pX));
				
				if(pX <= pvX*scale && sW >= defWidth)
				{
					pivotX = pvX;
					x = int(pX);
				}
				
				var pY:Number = (currentPosA.y + currentPosB.y) * 0.5;
				y = pY;

				
                // rotate
                //rotation += deltaAngle;

				
                // scale
                var sizeDiff:Number = currentVector.length / previousVector.length;
                scaleX *= sizeDiff;
                scaleY *= sizeDiff;
				
				if(scaleX > maxScale)
				{
					scale = maxScale;
				}
				else if(scaleX < 1)
				{
					scale = 1;
				}
				
				isMoved = true;
            }
            
            var touch:Touch = event.getTouch(this, TouchPhase.ENDED);
            
			if(touch)
			{
				if(touch.tapCount == 2)
				{
					var pv1:Point = touch.getLocation(this);
					
					pivotX = pv1.x;
					pivotY = pv1.y;
					
					var pv2:Point = touch.getLocation(parent);
					x = pv2.x;
					y = pv2.y;
					
					var tween:Tween = new Tween(this, 0.1);
					
					if(scale < maxScale)
					{
						tween.animate("scale", maxScale);
						tween.onComplete = function():void{
							if(pivotX*scale - x < 0)
							{
								pivotX = x = 0;
							}
							else if(width - Math.abs(Math.abs(pivotX*scale) -Math.abs(x)) < defWidth)
							{
								pivotX = defWidth;
								x = defWidth;
							}
						}
					}
					else 
					{
						//pivotX = pivotY = 0;
						//pivotY =y=0;
						tween.animate("pivotX", 320);
						tween.animate("x", 320);
						tween.animate("pivotY", AppSettings.height>>1);
						tween.animate("y", AppSettings.height>>1);
						tween.scaleTo(1);
					}
					
					Starling.juggler.add(tween);
				}
				else
				{
					if(scale == 1)
					{
						pivotX = pivotY = 0;
						x = y =0;
					}
					else
					{
						if(pivotX*scale - x < 0)
						{
							pivotX = x = 0;
						}
						else if(this.width - Math.abs(Math.abs(pivotX*scale) -Math.abs(x)) < defWidth)
						{
							pivotX = defWidth;
							x = defWidth;
						}
						
						if(this.height > AppSettings.height)
						{
							var contBound:Rectangle = _contents.getBounds(parent);
							
							if(!contBound.intersects(new Rectangle(0,400,AppSettings.width, AppSettings.height-800)))
							{ 
								y = pivotY = 0;
							}
						}
					}
					
					if(!isMoved) dispatchEventWith(ON_TAB_ONCE, true);
					
					isMoved = false;
				}
			}
			else if (touch && touch.tapCount == 2)
			{
				//parent.addChild(this); // bring self to front
				trace("double");
			}
        }
        
        public override function dispose():void
        {
            removeEventListener(TouchEvent.TOUCH, onTouch);
            super.dispose();
        }
    }
}