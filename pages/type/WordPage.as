package pages.type
{
	import com.greensock.TweenMax;
	import com.greensock.loading.DataLoader;
	import com.greensock.loading.LoaderMax;
	
	import app.AppSettings;
	
	import component.ScrollSprite;
	import component.SoundManager;
	
	import feathers.controls.Button;
	import feathers.controls.Label;
	import feathers.controls.ToggleButton;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.VerticalAlign;
	
	import pages.data.StudyInfo;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextFormat;
	
	import theme.CustomTheme;

	public class WordPage extends BasePage
	{
		private var topbarHeight:int;
		private var jsonChapterWords:Object;
		private var displayCont:Sprite;
		private var topCont:Sprite;
		private var midCont:Sprite;
		private var btmCont:Sprite;
		
		private var bgMid:Image;
		
		private var labelWord:Label = null;
		private var labelPron:Label = null;
		private var labelMean:Label = null;
		
		private var btnToggleWord:ToggleButton;
		private var btnTogglePron:ToggleButton;
		private var btnToggleMean:ToggleButton;
		
		private var btnPlay:Button;
		private var btnPrev:Button;
		private var btnNext:Button;
		
		private var curSequence:int = 0;
		
		private var imgSoundOnly:Image;
		private var infoLabel:Label;
		
		private var _loaderQueue:LoaderMax = null;
		private var _dataLoaderTxtSoundTime:DataLoader = null;
		
		public function WordPage()
		{
			super();
		}
		
		override public function loadContent():void
		{
			draw();
			
			var jsonWords:Object = Root.assets.getObject("words");
			var strChapter:String = "chapter" + (StudyInfo.currentChapter);
			jsonChapterWords = jsonWords[strChapter];
			
			labelWord.text = jsonChapterWords[curSequence].word + "\n\n";
			if(AppSettings.WORDPAGE_TYPE != 2)
			{
				labelPron.text = jsonChapterWords[curSequence].pron + "\n\n";
			}
			labelMean.text = jsonChapterWords[curSequence].mean + "\n\n";
			
			calculateDisplayLayout();
			infoLabel.text = "1 / " + jsonChapterWords.length;
			
			isLoaded = true;
			
			this.addEventListener(ScrollSprite.ON_RESIZE_OFFSET, onResizeOffset);
		}
		
		override public function unloadContent():void
		{
			super.unloadContent();
			
			jsonChapterWords = null;
			
			if(_loaderQueue) _loaderQueue.cancel();
			
			_dataLoaderTxtSoundTime =null;
			
			this.removeEventListener(ScrollSprite.ON_RESIZE_OFFSET, onResizeOffset);
		}
		
		override public function draw():void
		{
			//Top
			topCont = new Sprite();
			topCont.y = 0;
			this.addChild(topCont);
			
			var bgTop:Image = new Image(Root.assets.getTexture("word/top"));
			bgTop.touchable = false;
			topCont.addChild(bgTop);
			
			topbarHeight = bgTop.height;
			
			infoLabel = new Label();
			infoLabel.fontStyles = new TextFormat(CustomTheme.FONT_NOTO_MEDIUM, 32, 0xa0a0a0, HorizontalAlign.CENTER, VerticalAlign.MIDDLE);
			infoLabel.touchable = false;
			infoLabel.y = 27;
			topCont.addChild(infoLabel);
			infoLabel.validate();
			
			infoLabel.width = AppSettings.width;
			infoLabel.text = "";
			
			//Middle
			midCont = new Sprite();
			midCont.y = topCont.y + topCont.height;
			this.addChild(midCont);
			
			bgMid = new Image(Root.assets.getTexture("word/mid"));
			bgMid.touchable = false;
			midCont.addChild(bgMid);
			
			labelWord = new Label();
			labelWord.width = AppSettings.width;
			labelWord.fontStyles = CustomTheme.fsWordPageWord.clone();
			midCont.addChild(labelWord);
			
			labelPron = new Label();
			labelPron.width = AppSettings.width;
			labelPron.fontStyles = CustomTheme.fsWordPagePron.clone();
			midCont.addChild(labelPron);
			
			if(AppSettings.WORDPAGE_TYPE == 2)
			{
				labelPron.visible = false;
			}
			
			labelMean = new Label();
			labelMean.width = AppSettings.width;
			labelMean.fontStyles = CustomTheme.fsWordPageMean.clone();
			midCont.addChild(labelMean);
			
			imgSoundOnly = new Image(Root.assets.getTexture("word/soundOnly"));
			imgSoundOnly.visible = false;
			midCont.addChild(imgSoundOnly);
			
			//Bottom
			btmCont = new Sprite();
			this.addChild(btmCont);
			
			var bgBottom:Image = new Image(Root.assets.getTexture("word/bot"));
			bgBottom.touchable = false;
			btmCont.addChild(bgBottom);
			btmCont.y = this.height - btmCont.height;
			
			btnToggleWord = new ToggleButton();
			btnToggleWord.defaultSkin = new Image(Root.assets.getTexture("word/btn1N"));
			btnToggleWord.defaultSelectedSkin = new Image(Root.assets.getTexture("word/btn1S"));
			btmCont.addChild(btnToggleWord);
			
			btnTogglePron = new ToggleButton();
			btnTogglePron.defaultSkin = new Image(Root.assets.getTexture("word/btn2N"));
			btnTogglePron.defaultSelectedSkin = new Image(Root.assets.getTexture("word/btn2S"));
			btnTogglePron.visible = false;
			btmCont.addChild(btnTogglePron);
			
			btnToggleMean = new ToggleButton();
			btnToggleMean.defaultSkin = new Image(Root.assets.getTexture("word/btn3N"));
			btnToggleMean.defaultSelectedSkin = new Image(Root.assets.getTexture("word/btn3S"));
			btmCont.addChild(btnToggleMean);
			
			if(AppSettings.WORDPAGE_TYPE == 2)
			{
				btnToggleWord.x = 100;
				btnToggleMean.x = 360;
			}
			else
			{
				btnTogglePron.visible = true;
				btnToggleWord.x = 33;
				btnTogglePron.x = 228;
				btnToggleMean.x = 420;
			}
			btnToggleWord.y = btnTogglePron.y = btnToggleMean.y = 25;
			
			btnPlay = new Button();
			btnPlay.defaultSkin = new Image(Root.assets.getTexture("word/btnPlayN"));
			btnPlay.downSkin = new Image(Root.assets.getTexture("word/btnPlayS"));
			btmCont.addChild(btnPlay);
			btnPlay.validate();
			
			btnPrev = new Button();
			btnPrev.defaultSkin = new Image(Root.assets.getTexture("word/btnPrevN"));
			btnPrev.downSkin = new Image(Root.assets.getTexture("word/btnPrevS"));
			btmCont.addChild(btnPrev);
			
			btnNext = new Button();
			btnNext.defaultSkin = new Image(Root.assets.getTexture("word/btnNextN"));
			btnNext.downSkin = new Image(Root.assets.getTexture("word/btnNextS"));
			btmCont.addChild(btnNext);
			
			btnPlay.x = (AppSettings.width - btnPlay.width) >>1;
			btnPrev.x = 20;
			btnNext.x = 495;
			
			btnPlay.y = 140;
			btnPrev.y= btnNext.y = btnPlay.y + 25;
			
			//Event 
			btnPlay.addEventListener(Event.TRIGGERED, onTriggredBtn);
			btnNext.addEventListener(Event.TRIGGERED, onTriggredBtn);
			btnPrev.addEventListener(Event.TRIGGERED, onTriggredBtn);
			
			btnToggleWord.addEventListener(Event.TRIGGERED, onTriggredToggleBtn);
			btnTogglePron.addEventListener(Event.TRIGGERED, onTriggredToggleBtn);
			btnToggleMean.addEventListener(Event.TRIGGERED, onTriggredToggleBtn);
			
			super.draw();
		}
		
		private function onResizeOffset(event:Event, offset:int):void
		{
			var newHeight:int = bgMid.height - offset;
			TweenMax.to(bgMid, 0.3, {height: newHeight});
			tweenDisplayLayout(newHeight);
			TweenMax.to(btmCont, 0.3, {y: btmCont.y - offset});
		}
		
		private function onTriggredBtn(event:Event):void
		{
			event.stopPropagation();
			
			if(!this.isLoaded) return;
			
			var curBtn:Button = Button(event.currentTarget);
			var isSeqChange:Boolean = false;
			switch(curBtn)
			{
				case btnPlay :
					break;
				case btnPrev : 
					isSeqChange = true;
					if(curSequence > 0) curSequence--;
					else curSequence = jsonChapterWords.length-1;
					break;
				case btnNext :
					isSeqChange = true;
					if(curSequence < jsonChapterWords.length-1) curSequence++;
					else curSequence = 0;
					
					break;
			}
			
			if(curBtn)
			{
				var path:String = "data/sound/chapter" + idxChapter + "/" + jsonChapterWords[curSequence].sound + ".mp3";
				SoundManager.play(path);
			}
			
			if(isSeqChange)
			{
				labelWord.text = jsonChapterWords[curSequence].word + "\n\n";
				labelPron.text = jsonChapterWords[curSequence].pron + "\n\n";
				labelMean.text = jsonChapterWords[curSequence].mean + "\n\n";
				infoLabel.text = (curSequence+1) + " / " + jsonChapterWords.length;
			}
		}
		
		private function onTriggredToggleBtn(event:Event):void
		{
			if(!this.isLoaded) return;
			
			var curBtn:Button = Button(event.currentTarget);
			
			switch(curBtn)
			{
				case btnToggleWord:
					labelWord.visible = btnToggleWord.isSelected;
					break;
				case btnTogglePron:
					labelPron.visible = btnTogglePron.isSelected;
					break;
				case btnToggleMean:
					labelMean.visible = btnToggleMean.isSelected;
					break;
			}
			
			if(AppSettings.WORDPAGE_TYPE == 2)
			{
				if(!labelWord.visible && !labelMean.visible) imgSoundOnly.visible = true;
				else imgSoundOnly.visible = false;
			}
			else
			{
				if(!labelWord.visible && !labelPron.visible && !labelMean.visible)
					imgSoundOnly.visible = true;
				else
					imgSoundOnly.visible = false;
			}
		}
		
		private function calculateDisplayLayout():void
		{
			bgMid.height += 160;
			if(AppSettings.WORDPAGE_TYPE == 2)
			{
				labelWord.y = int(bgMid.height * 0.25);
				labelMean.y = int(bgMid.height * 0.55);
			}
			else
			{
				labelWord.y = int(bgMid.height * 0.15);
				labelPron.y = int(bgMid.height * 0.45);
				labelMean.y = int(bgMid.height * 0.70);
			}
			imgSoundOnly.y = (bgMid.height - imgSoundOnly.height) >> 1;
		}
		
		private function tweenDisplayLayout(newHeight:int):void
		{	
			if(AppSettings.WORDPAGE_TYPE == 2)
			{
				TweenMax.to(labelWord, 0.3, {y: newHeight * 0.25});
				TweenMax.to(labelMean, 0.3, {y: newHeight * 0.55});
			}
			else
			{
				TweenMax.to(labelWord, 0.3, {y: newHeight * 0.15});
				TweenMax.to(labelPron, 0.3, {y: newHeight * 0.40});
				TweenMax.to(labelMean, 0.3, {y: newHeight * 0.65});
			}
			TweenMax.to(imgSoundOnly, 0.3, {y: (newHeight-imgSoundOnly.height) >> 1});
		}
	}
}