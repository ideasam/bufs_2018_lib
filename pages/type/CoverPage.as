package pages.type
{
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	
	import app.AppSettings;
	
	import feathers.controls.ImageLoader;
	
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	
	public class CoverPage extends BasePage
	{
		private var bgLoader:ImageLoader = null;
		private var shadowImage:ImageLoader = null;
		private var titleImage:ImageLoader = null;
		private var coverFrame:Image = null;
		
		public function CoverPage()
		{
			super();			
		}
		
		override public function draw():void
		{
			bgLoader = new ImageLoader();
			this.addChild(bgLoader);
			
			shadowImage = new ImageLoader();
			shadowImage.source = Root.assets.getTexture("coverBg");
			this.addChild(shadowImage);
			
			titleImage = new ImageLoader();
			this.addChild(titleImage);
			
			coverFrame = new Image(Root.assets.getTexture("cover_frame"));
			coverFrame.textureSmoothing = TextureSmoothing.NONE;
			coverFrame.scale9Grid = new Rectangle(30,30,40,40);
			coverFrame.touchable =false;
			this.addChild(coverFrame);
			
			coverFrame.width = AppSettings.width;
			coverFrame.height = AppSettings.height;
			
			super.drawUI();
		}
		
		override public function loadContent():void
		{
			bgLoader.source = new URLRequest("data/img/chapter"+idxChapter+"/cover/c1.jpg").url;
			bgLoader.height = AppSettings.height > 1138 ? AppSettings.height : 1138;
			
			if(!bgLoader.isLoaded)
			{
				trace("cover load content");
				Root.showLoadingSpinner(true);
			}
			
			titleImage.source = new URLRequest("data/img/chapter"+idxChapter+"/cover/c0.png").url;
			titleImage.width = 640;
			super.loadContent();
			
			bgLoader.addEventListener(Event.COMPLETE, onCompleteLoad);
			
			function onCompleteLoad():void
			{
				trace("comp");
				bgLoader.removeEventListener(Event.COMPLETE, onCompleteLoad);
				dispatchEventWith(Event.READY, false);
				Root.showLoadingSpinner(false);
			}
		}
		
		override public function unloadContent():void
		{
			super.unloadContent();
			
			bgLoader.source = null;
			titleImage.source = null;
			//coverFrame.removeFromParent();
		}
	}
}