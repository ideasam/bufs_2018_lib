package pages.type
{
	import flash.errors.IllegalOperationError;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.utils.Timer;
	
	import app.AppSettings;
	import app.LocalSavedData;
	
	import component.ScrollSprite;
	import component.PlayerDock;
	
	import net.NetManager;
	
	import starling.display.Image;
	
	public class BasePage extends ScrollSprite
	{
		//Tags
		protected static const TAG_RECT:String = "tag_rect";
		protected static const TAG_GR_TEXT:String = "tag_gallery_text";
		protected static const TAG_SND_TEXT:String = "tag_sound_text";
		
		//Events
		public static const PAGE_CHANGED:String = "pageChanged";
		public static const TOGGLE_BTN_ARROW:String = "toggleBtnArrow";
		
		private var _idxChapter:int = -1;
		private var _idxPage:int = -1;
		
		public var hasPlayerDock:Boolean = false;
		
		private var bg:Image = null;
		
		public var isLoaded:Boolean = false;
		
		protected var mPlayerDock:PlayerDock;
		
		protected var rectEffect:Image = null;
		protected var endIndicator:Image = null;
		
		protected var leftShadow:Image;
		protected var rightShadow:Image;
		
		protected var hasLeftShadow:Boolean=false;
		protected var hasRightShadow:Boolean=false;
		
		//update to server
		private var timer:Timer = new Timer(5000, 12);
		
		public function BasePage()
		{
			super();
			
			rectEffect = new Image(Root.assets.getTexture("effect/rect_effect"));
			rectEffect.scale9Grid = new Rectangle(25,25,25,25);
			rectEffect.visible = false;
			rectEffect.touchable = false;
			
			endIndicator = new Image(Root.assets.getTexture("footer/endIndicator"));
			endIndicator.y = AppSettings.height;			
		}
		
		public function setIndexs(idxChapter:int, idxPage:int):void
		{
			_idxChapter = idxChapter;
			_idxPage = idxPage;
			
			if(!AppSettings.PAGE_CLASSES) return;
			if(!AppSettings.PAGE_CLASSES[idxChapter]) return;
		}
		
		public function draw():void
		{
			if (-1 == idxChapter || -1 == idxPage)
			{
				throw IllegalOperationError("Invalid idxChapter or idxPage");
			}
			if(true == hasPlayerDock) drawPlayerDock();
		}
		
		private function drawPlayerDock():void
		{
			mPlayerDock = new PlayerDock();
			mPlayerDock.y = AppSettings.height - mPlayerDock.height;
			this.addChild(mPlayerDock);
		}
		
		public function deactivate():void
		{
			trace("deactivated: " + _idxPage + "page");
			if(mPlayerDock) mPlayerDock.pause();
			if(rectEffect) rectEffect.filter = null;
			timer.stop();
			timer.removeEventListener(TimerEvent.TIMER, onTick);
		}
		
		public function pausePlayer():void
		{
			if(mPlayerDock) mPlayerDock.pause();
		}
		
		public function loadContent():void
		{
			if(_idxPage == -1){
				trace("set pageindex first");
				throw IllegalOperationError("set pageindex first");
			}
			
			isLoaded = true;
		}
		
		private var startTime:Number = -1;
		
		public function activate():void
		{
			trace("activated: " + _idxPage + "page");
			if(idxPage >0)
			{
				timer.addEventListener(TimerEvent.TIMER, onTick);
				timer.start();
				
				if(startTime == -1) startTime = new Date().time;
			}
		}
		
		private function onTick(event:TimerEvent):void
		{
			if(idxChapter < 0) return;
			
			var sTime:String = String(int(startTime/1000));
			var eTime:String = String(int(new Date().time/1000));
			
			//var tPage:int = isPreparaton ? subPage : subPage-2;
			
			//단어 페이지 등 아직 앱 콘텐츠가 완전치 않을때 
			//if(!AppSettings.hasAllContent) tPage = subPage-1;
			
			//Quiz?
			if(idxPage > 0)
			{
				NetManager.updateEduProgress(
					AppSettings.SUBJECT_CODE,
					LocalSavedData.getUserData().uid,
					String(idxChapter), String(idxPage-1), "5", "10");
			}
		}
		
		public function unloadContent():void
		{
			deactivate();
			
			if(!isLoaded) return;
			isLoaded = false;
		}

		public function get playerDock():PlayerDock
		{
			return mPlayerDock;
		}
		
		public function get idxChapter():int
		{
			return _idxChapter;
		}

		public function get idxPage():int
		{
			return _idxPage;
		}
	}
}