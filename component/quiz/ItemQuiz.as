package component.quiz
{
	import feathers.controls.Label;
	
	import pages.type.QuizPage;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	import theme.CustomTheme;
	
	public class ItemQuiz extends Sprite
	{
		private var _idGroup:int = -1;
		private var _idQuiz:int = -1;
		private var _answer:int = -1;
		private var _result:int = 0; //correct:1, fail:0
		private var _isCheck:Boolean = false;
		
		private var imgMarkingRight:Image = new Image(Root.assets.getTexture("quiz/markingRight"));
		private var imgMarkingWrong:Image = new Image(Root.assets.getTexture("quiz/markingWrong"));
		
		private static var _page:QuizPage = null;
		
		public var vecOptions:Vector.<ItemOption> = new Vector.<ItemOption>;
		
		public function get idGroup():int { return _idGroup };
		public function get idQuiz():int { return _idQuiz };
		public function set isCheck(b:Boolean):void { _isCheck = b };
		public function get isCheck():Boolean { return _isCheck };
		
		public function ItemQuiz()
		{
			super();
		}
		
		public function create(page:QuizPage, idGroup:int, idQuiz:int, dataQuiz:Object, width:Number, yPos:int):int
		{
			_page = page;
			_idGroup = idGroup;
			_idQuiz = idQuiz;
			
			var xPos:int = (_page.scrollCont.width - width) >> 1;
			_page.scrollCont.addChild(imgMarkingRight);
			_page.scrollCont.addChild(imgMarkingWrong);
			imgMarkingRight.visible = false;
			imgMarkingWrong.visible = false;
			
			if (dataQuiz.question != "")
			{
				imgMarkingRight.x = xPos - int(imgMarkingRight.width * 0.4);
				imgMarkingRight.y = yPos - int(imgMarkingRight.height * 0.3);
				imgMarkingWrong.x = xPos - int(imgMarkingWrong.width * 0.4);
				imgMarkingWrong.y = yPos - int(imgMarkingWrong.height * 0.3);
				
				var labelQuestion:Label = new Label();
				labelQuestion.wordWrap = true;
				
				labelQuestion.width = width;
				labelQuestion.fontStyles = CustomTheme.fsDark.clone();
				labelQuestion.validate();
				labelQuestion.x = xPos;
				labelQuestion.y = yPos;
				labelQuestion.text = dataQuiz.question;
			
				labelQuestion.touchable = false;
				_page.scrollCont.addChild(labelQuestion);
				labelQuestion.validate();
				
				yPos += labelQuestion.height + 15;
			}
			else
			{
				imgMarkingRight.x = xPos - int(imgMarkingRight.width * 0.6);
				imgMarkingRight.y = yPos - int(imgMarkingRight.height * 1);
				imgMarkingWrong.x = xPos - int(imgMarkingWrong.width * 0.6);
				imgMarkingWrong.y = yPos - int(imgMarkingWrong.height * 1);
			}
			
			for (var idOption:int = 0; idOption < dataQuiz.options.length; idOption++)
			{
				vecOptions[idOption] = new ItemOption();
				var optionLabelWidth:int = int(width);
				xPos = xPos;
				yPos = vecOptions[idOption].create(_page, idGroup, idQuiz, idOption, dataQuiz, optionLabelWidth, xPos, yPos);				
			}
			yPos += 25;
			
			return yPos;
		}
		
		public function showMarking(isRight:Boolean):void
		{
			if (isRight)
			{
				imgMarkingRight.visible = true;
			}
			else
			{
				imgMarkingWrong.visible = true;
				for (var idOption:int = 0; idOption < vecOptions.length; idOption++)
					if (vecOptions[idOption].isCorrect)
						vecOptions[idOption].highlightWrong();
			}
		}
		
		public function activate():void
		{
			for (var idOption:int = 0; idOption < vecOptions.length; idOption++)
				vecOptions[idOption].activate();
		}
		
		public function deactivate():void
		{
			for (var idOption:int = 0; idOption < vecOptions.length; idOption++)
				vecOptions[idOption].deactivate();
		}
	}
}