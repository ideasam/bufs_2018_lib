package pages
{
	import com.greensock.loading.LoaderMax;
	
	import flash.external.ExternalInterface;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import app.AppSettings;
	import app.LocalSavedData;
	
	import feathers.controls.Button;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.PanelScreen;
	import feathers.controls.TextInput;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.VerticalAlign;
	
	import net.NetManager;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.filters.BlurFilter;
	import starling.text.TextFormat;
	import starling.utils.SystemUtil;
	
	import theme.CustomTheme;
	
	import utils.DeviceInfo;
	
	public class PageLogin extends PanelScreen
	{
		private var bgLoader:ImageLoader = null;
		private var _loaderQueue:LoaderMax = null;
		
		private var contDialog:Sprite;
		private var fontFormat:TextFormat;
		private var btnLogin:Button;
		
		private var inputUsername:TextInput;
		private var inputPassword:TextInput;
		
		private var _tagCont:String = "bgCont";
		private var infoLabel:Label;
		
		public function PageLogin()
		{
			super();
		}
		
		override protected function initialize():void
		{
			super.initialize();
			
			this.verticalScrollPolicy = feathers.controls.ScrollPolicy.OFF;
			
			bgLoader = new ImageLoader();
			bgLoader.source = new URLRequest("data/img/login/bg.png").url;
			bgLoader.addEventListener(Event.COMPLETE, onCompleteBgload);
			var blurFilter:BlurFilter = new BlurFilter(4.5,4.5, 0.5);
			bgLoader.filter = blurFilter;
			this.addChild(bgLoader);
		}
		
		private function onCompleteBgload():void
		{
			bgLoader.y = (viewPort.height - bgLoader.base.height) >> 1;
			
			contDialog = new Sprite();
			this.addChild(contDialog);
			
			var bg:Image = new Image(Root.assets.getTexture("login/bg"));
			contDialog.addChild(bg);			
			
			inputUsername = new TextInput();
			inputUsername.width = 360;
			inputUsername.height = 60;
			inputUsername.restrict = "0-9";
			inputUsername.prompt = "학번을 입력해주세요.";		
			inputUsername.maxChars = 60;
			inputUsername.fontStyles = CustomTheme.fsConversationPageOrigin.clone();
			contDialog.addChild(inputUsername);
			inputUsername.x = 170;
			inputUsername.y = 110;
			
			//
			inputPassword = new TextInput();
			inputPassword.width = 360;
			inputPassword.height = 60;
			inputPassword.prompt = "이메일을 입력해주세요.";
			inputPassword.maxChars = 60;
			inputPassword.fontStyles = CustomTheme.fsConversationPageOrigin.clone();
			contDialog.addChild(inputPassword);
			
			inputPassword.x = 170;
			inputPassword.y = 187;
			
			btnLogin = new Button();
			btnLogin.fontStyles = new TextFormat("SourceSansPro");
			btnLogin.defaultSkin = new Image(Root.assets.getTexture("login/btnLoginN"));
			btnLogin.downSkin = new Image(Root.assets.getTexture("login/btnLoginS"));
			contDialog.addChild(btnLogin);
			btnLogin.validate();
			
			infoLabel = new Label();
			infoLabel.width = AppSettings.width;
			infoLabel.fontStyles = new TextFormat(CustomTheme.FONT_NOTO_MEDIUM, 22, 0xffffff, HorizontalAlign.CENTER, VerticalAlign.TOP);
			this.addChild(infoLabel);
			
			btnLogin.x = (AppSettings.width - btnLogin.width)>>1;
			btnLogin.y = 290;
			
			contDialog.y = (AppSettings.height - contDialog.height)>>1;
			infoLabel.y = contDialog.y+contDialog.height +5;
			
			btnLogin.addEventListener(Event.TRIGGERED, onTriggeredLogin);
			
			this.dispatchEventWith(PageEvent.LOADED,true);
		}
		
		private function onComleteLoad():void
		{
			bgLoader.removeEventListener(Event.COMPLETE, onComleteLoad);
			btnLogin.addEventListener(Event.TRIGGERED, onTriggeredLogin);
		}
		
		private function onTriggeredLogin():void
		{
			btnLogin.removeEventListener(Event.TRIGGERED, onTriggeredLogin);
			
			
			if(inputPassword.text == "" || inputUsername.text == "")
			{
				infoLabel.text = "아이디/비밀번호를 입력해주세요.";
				btnLogin.addEventListener(Event.TRIGGERED, onTriggeredLogin);
				return;
			}
			else
			{
				
				// test
				// inputUsername.text = "19991999";
				// inputPassword.text = "t@t.com";
				infoLabel.text = "로그인 중..";
				
				if(SystemUtil.isDesktop)
				{
					try
					{
						ExternalInterface.addCallback("fromJavascriptResult", fromJavascriptResult);
					} 
					catch(error:Error) 
					{
						trace("login error",error.message);
						navigateToURL(new URLRequest("javascript:alert('Fail');"));
						btnLogin.addEventListener(Event.TRIGGERED, onTriggeredLogin);
					}
					
					//original
					ExternalInterface.call("toJavascriptLogin", inputUsername.text, inputPassword.text);
					
					//TEST_FOR_VYH034 
					/*if(inputUsername.text == "19991999" && inputPassword.text == "t@t.com")
						ExternalInterface.call("toJavascriptLogin", inputUsername.text, inputPassword.text);
					else
						onFailLogin(null);*/
				}
				else
				{
					trace("subjectCode:", AppSettings.SUBJECT_CODE);
					NetManager.login(AppSettings.SUBJECT_CODE, inputUsername.text, inputPassword.text, onSuccessLogin, onFailLogin);
				}
			}
		}
		
		public function fromJavascriptResult(text:String):void 
		{
			trace("!fromJavascriptResult:", text);
			if (text && text.indexOf("success") > -1) onSuccessLogin();
			else onFailLogin()
		}
		
		protected function onFailLogin(event:*=null):void
		{
			infoLabel.text = "로그인 정보를 확인하십시오.";
			btnLogin.addEventListener(Event.TRIGGERED, onTriggeredLogin);
		}
		
		private function onSuccessLogin():void
		{
			LocalSavedData.setUserData(inputUsername.text, inputPassword.text);
			dispatchEventWith(PageNavigator.CONTENT_PAGE);
		}
		
	}
}