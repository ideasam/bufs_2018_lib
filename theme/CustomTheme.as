package theme
{
	import flash.geom.Rectangle;
	
	import feathers.controls.Button;
	import feathers.controls.ButtonState;
	import feathers.controls.Slider;
	import feathers.controls.TextInput;
	import feathers.controls.TrackLayoutMode;
	import feathers.controls.text.StageTextTextEditor;
	import feathers.controls.text.TextBlockTextRenderer;
	import feathers.controls.text.TextFieldTextRenderer;
	import feathers.core.FeathersControl;
	import feathers.core.ITextRenderer;
	import feathers.core.PopUpManager;
	import feathers.layout.Direction;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.VerticalAlign;
	import feathers.media.SeekSlider;
	import feathers.skins.ImageSkin;
	import feathers.themes.StyleNameFunctionTheme;
	
	import starling.display.DisplayObject;
	import starling.display.Quad;		
	import starling.text.TextFormat;
	import starling.textures.Texture;
	import starling.utils.Align;
	
	import app.AppSettings;
	
	public class CustomTheme extends StyleNameFunctionTheme
	{
		[Embed(source="/../assets/fonts/NotoSansCJKkr-Medium.otf",fontFamily="NotoSansCJKkr-Medium",fontWeight="normal",mimeType="application/x-font-truetype",embedAsCFF="false")]
		protected static const FontNotoSans:Class;
		[Embed(source="/../assets/fonts/DoulosSIL-R.ttf",fontFamily="DoulosSIL-R",fontWeight="normal",mimeType="application/x-font-truetype",embedAsCFF="false")]
		protected static const FontDoulos:Class;
		
		public static const FONT_NOTO_MEDIUM:String = "NotoSansCJKkr-Medium";
		public static const FONT_DOULOS:String = "DoulosSIL-R";
		
		protected static const THEME_NAME_HORIZONTAL_SLIDER_MINIMUM_TRACK:String = "mobile-horizontal-slider-minimum-track";
		protected static const THEME_NAME_HORIZONTAL_SLIDER_MAXIMUM_TRACK:String = "mobile-horizontal-slider-maximum-track";
		
		protected static const BUTTON_SCALE9_GRID:Rectangle = new Rectangle(5, 5, 50, 50);
		
		protected var sliderMinSkinTextrues:Texture;
		protected var sliderPointer:Texture;
		
		public static var fsDoulos:TextFormat;
	
		public static var defaultInputFontStyles:TextFormat;
		
		public static var fsConversationPageTitle:TextFormat;
		public static var fsConversationPageOrigin:TextFormat;
		public static var fsConversationPageTrans:TextFormat;
		
		public static var fsWordPageTitle:TextFormat;
		public static var fsWordPageCount:TextFormat;
		public static var fsWordPageWord:TextFormat;
		public static var fsWordPagePron:TextFormat;
		public static var fsWordPageMean:TextFormat;
		
		public static var fsQuizPageTitle:TextFormat;
		public static var fsQuizPageGroup:TextFormat;
		public static var fsQuizPageQuiz:TextFormat;
		public static var fsQuizPageOption:TextFormat;
		
		public static var fsDark:TextFormat;
		public static var fsLight:TextFormat;
		
		public function CustomTheme()
		{
			initializeFonts();
			initializeTextures();
			this.initializeGlobals();
			
			//Slider
			this.getStyleProviderForClass(Slider).defaultStyleFunction = this.setSliderStyles;
			this.getStyleProviderForClass(Button).setFunctionForStyleName(Slider.DEFAULT_CHILD_STYLE_NAME_THUMB, this.customButtonInitializer);
			this.getStyleProviderForClass(Button).setFunctionForStyleName(THEME_NAME_HORIZONTAL_SLIDER_MINIMUM_TRACK, this.setHorizontalSliderMinimumTrackStyles);
			this.getStyleProviderForClass(Button).setFunctionForStyleName(THEME_NAME_HORIZONTAL_SLIDER_MAXIMUM_TRACK, this.setHorizontalSliderMaximumTrackStyles);
			
			this.getStyleProviderForClass(TextInput).defaultStyleFunction = this.setTextInputStyles;
			
			this.getStyleProviderForClass(SeekSlider).defaultStyleFunction = this.setSeekSliderStyles;
			this.getStyleProviderForClass(Button).setFunctionForStyleName(SeekSlider.DEFAULT_CHILD_STYLE_NAME_THUMB, this.setSliderThumbStyles);
			this.getStyleProviderForClass(Button).setFunctionForStyleName(SeekSlider.DEFAULT_CHILD_STYLE_NAME_MINIMUM_TRACK, this.setHorizontalSliderMinimumTrackStyles);
			this.getStyleProviderForClass(Button).setFunctionForStyleName(SeekSlider.DEFAULT_CHILD_STYLE_NAME_MAXIMUM_TRACK, this.setHorizontalSliderMaximumTrackStyles);

		}
		
		protected static function popUpOverlayFactory():DisplayObject
		{
			var quad:Quad = new Quad(100, 100, 0x000000);
			quad.alpha = 0.7;
			return quad;
		}
		
		protected static function textRendererFactory():ITextRenderer
		{
			var tftr:TextFieldTextRenderer = new TextFieldTextRenderer;
			tftr.isHTML = true;
			tftr.embedFonts = true;
			//tftr.useGutter = true;
			//tftr.isFocusEnabled = true;
			//tftr.useGutter
			return tftr;
		}
		
		/**
		 * The default global text editor factory for this theme creates a
		 * StageTextTextEditor.
		 */
		protected static function textEditorFactory():StageTextTextEditor
		{
			return new StageTextTextEditor();
		}
		
		protected function initializeGlobals():void
		{
			FeathersControl.defaultTextRendererFactory = textRendererFactory;
			FeathersControl.defaultTextEditorFactory = textEditorFactory;
			
			PopUpManager.overlayFactory = popUpOverlayFactory;
		}
		
		private function initializeTextures():void
		{
			this.sliderPointer = Root.assets.getTexture("indicator");//this.atlas.getTexture("sliderPoint");
			this.sliderMinSkinTextrues = Root.assets.getTexture("sliderGauge");
		}
		
		protected function initializeFonts():void
		{
			defaultInputFontStyles = new TextFormat(FONT_NOTO_MEDIUM, 24, 0x0, HorizontalAlign.LEFT, VerticalAlign.TOP);
			
			fsLight  = new TextFormat(FONT_NOTO_MEDIUM, AppSettings.fsLightSize,  AppSettings.fsLightColor,  HorizontalAlign.CENTER, Align.CENTER);
			fsDark   = new TextFormat(FONT_NOTO_MEDIUM, AppSettings.fsDarkSize,   AppSettings.fsDarkColor,   HorizontalAlign.LEFT,   Align.CENTER);
			fsDoulos = new TextFormat(FONT_DOULOS,      AppSettings.fsDoulosSize, AppSettings.fsDoulosColor, HorizontalAlign.CENTER, VerticalAlign.MIDDLE);
			
			fsWordPageWord = new TextFormat(FONT_NOTO_MEDIUM, AppSettings.fsWordPageWordSize, AppSettings.fsWordPageWordColor, HorizontalAlign.CENTER, VerticalAlign.MIDDLE);
			fsWordPagePron = new TextFormat(FONT_DOULOS,      AppSettings.fsWordPagePronSize, AppSettings.fsWordPagePronColor, HorizontalAlign.CENTER, VerticalAlign.MIDDLE);
			fsWordPageMean = new TextFormat(FONT_NOTO_MEDIUM, AppSettings.fsWordPageMeanSize, AppSettings.fsWordPageMeanColor, HorizontalAlign.CENTER, VerticalAlign.MIDDLE);			
			fsWordPageWord.leading = fsWordPageWord.size * 0.5;
			fsWordPagePron.leading = fsWordPagePron.size * 0.5;
			fsWordPageMean.leading = fsWordPageMean.size * 0.5;
			
			fsConversationPageTitle  = new TextFormat(FONT_NOTO_MEDIUM, AppSettings.fsConversationPageTitleSize , AppSettings.fsConversationPageTitleColor , HorizontalAlign.LEFT, VerticalAlign.MIDDLE);
			fsConversationPageOrigin = new TextFormat(FONT_NOTO_MEDIUM, AppSettings.fsConversationPageOriginSize, AppSettings.fsConversationPageOriginColor, HorizontalAlign.LEFT, VerticalAlign.MIDDLE);
			fsConversationPageTrans  = new TextFormat(FONT_NOTO_MEDIUM, AppSettings.fsConversationPageTransSize , AppSettings.fsConversationPageTransColor , HorizontalAlign.LEFT, VerticalAlign.MIDDLE);
			fsConversationPageTitle .leading = fsConversationPageTitle .size * 0.5;
			fsConversationPageOrigin.leading = fsConversationPageOrigin.size * 0.5;
			fsConversationPageTrans .leading = fsConversationPageTrans .size * 0.5;
		}
		
		protected function customButtonInitializer(button:Button):void
		{
			var skin:ImageSkin = new ImageSkin(this.sliderPointer);
			skin.setTextureForState(ButtonState.DOWN, this.sliderPointer);
			skin.setTextureForState(ButtonState.DISABLED, this.sliderPointer);
			button.defaultSkin = skin;
		}
		
		protected function setSliderStyles(slider:Slider):void
		{
			slider.trackLayoutMode = feathers.controls.TrackLayoutMode.SPLIT;
			if(slider.direction == feathers.layout.Direction.VERTICAL)
			{
				
			}
			else
			{
				slider.customMinimumTrackStyleName = THEME_NAME_HORIZONTAL_SLIDER_MINIMUM_TRACK;
				slider.customMaximumTrackStyleName = THEME_NAME_HORIZONTAL_SLIDER_MAXIMUM_TRACK;
			}
		}
		
		protected function setHorizontalSliderMinimumTrackStyles(track:Button):void
		{
			var skin:ImageSkin = new ImageSkin(this.sliderMinSkinTextrues);
			skin.scale9Grid = new Rectangle(1,0,1,0); 
			track.defaultSkin = skin;
			track.hasLabelTextRenderer = false;
			
		}
		
		protected function setHorizontalSliderMaximumTrackStyles(track:Button):void
		{
			track.hasLabelTextRenderer = false;
		}
		
		protected function setSeekSliderStyles(slider:SeekSlider):void
		{
			slider.direction = Direction.HORIZONTAL;
			slider.trackLayoutMode = TrackLayoutMode.SINGLE;
			slider.progressSkin = new Quad(3, 6, 0xffffff);
		}
		
		protected function setSliderThumbStyles(thumb:Button):void
		{
			var skin:ImageSkin = new ImageSkin(this.sliderPointer);
			skin.setTextureForState(ButtonState.DISABLED, this.sliderPointer);
			thumb.defaultSkin = skin;
			
			thumb.hasLabelTextRenderer = false;
		}
		
		protected function setSeekSliderMinimumTrackStyles(track:Button):void
		{
			var skin:ImageSkin = new ImageSkin(this.sliderMinSkinTextrues);
			skin.scale9Grid = new Rectangle(1,0,1,0);
			track.defaultSkin = skin;
			track.hasLabelTextRenderer = false;
		}
		
		protected function setSeekSliderMaximumTrackStyles(track:Button):void
		{
			track.hasLabelTextRenderer = false;
		}

		
		protected function setBaseTextInputStyles(input:TextInput):void
		{
			input.fontStyles = defaultInputFontStyles;
			input.promptFontStyles = fsLight;
		}
		
		protected function setTextInputStyles(input:TextInput):void
		{
			this.setBaseTextInputStyles(input);
		}
	}
}