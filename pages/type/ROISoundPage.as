package pages.type
{
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	
	import component.ROISoundItem;
	import component.SoundManager;
	
	import feathers.controls.ImageLoader;
	
	import pages.PageContent;
	
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.filters.GlowFilter;

	public class ROISoundPage extends BasePage
	{
		private var imgLoader:ImageLoader = null;
		
		private var prevTouchIdx:int = -1;
		private var vecROISoundItem:Vector.<ROISoundItem> = new Vector.<ROISoundItem>;
		
		public function ROISoundPage()
		{
			super();
		}
		
		override public function loadContent():void
		{
			draw();
			isLoaded = true;
		}
		
		override public function unloadContent():void
		{
			super.unloadContent();
			imgLoader.source = null;
		}
		
		override public function activate():void
		{
			
		}
		
		override public function deactivate():void
		{
			SoundManager.stop();
			rectEffect.visible = false;
		}
		
		override public function draw():void
		{
			super.draw();
			
			imgLoader = new ImageLoader();
			imgLoader.source = new URLRequest("data/img/chapter"+idxChapter+"/page"+idxPage+".png").url;
			scrollCont.addChild(imgLoader);
			
			var json:Object = Root.assets.getObject("roi");
			var dataChapter:Object = json["chapter"+idxChapter];
			
			for(var i:int = 0; i < dataChapter.pages.length; i++)
			{
				var dataPage:Object = dataChapter.pages[i];
				if(dataPage.index == idxPage)
				{
					var lenROIs:int = dataPage.ROIs.length;
					for (var j:int = 0; j < lenROIs; j++)
					{
						var curROIData:Object = dataPage.ROIs[j];
						vecROISoundItem.push(new ROISoundItem(new Rectangle(curROIData.x, curROIData.y, curROIData.width, curROIData.height), curROIData.sound));
					}
				}
			}
			scrollCont.addEventListener(TouchEvent.TOUCH, onTouchCont);
			
			scrollCont.addChild(rectEffect);
		}
		
		private function onTouchCont(event:TouchEvent):void
		{
			if(scrollCont.isScrolling || PageContent.isContentScrolling) return;
			
			var touch:Touch = event.getTouch(imgLoader);
			
			if(touch)
			{
				switch(touch.phase)
				{
					case TouchPhase.BEGAN:
						prevTouchIdx = getTouchAreaIdx();
						break;
					
					case TouchPhase.ENDED:
						
						var curTouchIdx:int = getTouchAreaIdx();
						if(prevTouchIdx > -1 && curTouchIdx>-1)
						{
							SoundManager.stop();
							
							rectEffect.filter = null;
							
							rectEffect.x = vecROISoundItem[curTouchIdx].bounds.x;
							rectEffect.y = vecROISoundItem[curTouchIdx].bounds.y;
							rectEffect.width = vecROISoundItem[curTouchIdx].bounds.width;
							rectEffect.height = vecROISoundItem[curTouchIdx].bounds.height;
							
							rectEffect.filter = new GlowFilter(0x29CDC8,0.8,8);
							rectEffect.alpha = 0.3;
							rectEffect.visible = true;
							
							var path:String = "data/sound/chapter" + idxChapter + "/" + vecROISoundItem[curTouchIdx].sound;
							SoundManager.play(path, onCompleteSoundPlay);
						}
						
						prevTouchIdx = -1;
						break;
				}
			}
			
			function getTouchAreaIdx():int
			{
				var len:int =vecROISoundItem.length;
				for (var i:int=0; i<len; ++i)
				{
					if(vecROISoundItem[i].bounds.containsPoint(touch.getLocation(imgLoader)))
					{
						return i;
					}
				}
				return -1;
			}
		}
		
		private function onCompleteSoundPlay():void
		{
			rectEffect.visible = false;
		}
	}
}