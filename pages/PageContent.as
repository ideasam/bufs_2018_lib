package pages
{	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import app.AppSettings;
	
	import component.PlayerDock;
	
	import feathers.controls.Button;
	import feathers.controls.PanelScreen;
	import feathers.controls.ScrollPolicy;
	import feathers.events.FeathersEventType;
	import feathers.layout.HorizontalLayout;
	
	import pages.data.StudyInfo;
	import pages.type.BasePage;
	
		public class PageContent extends PanelScreen
	{
		private var _pageNavigator:PageNavigator = null;
		
		public static var isContentScrolling:Boolean = false;
		
		private var currentPage:BasePage;
		private static var vecPages:Vector.<BasePage> = new Vector.<BasePage>;
		
		private var mPlayerDock:PlayerDock		
		private var prevPageIndex:int = 0;
		
		private var tempNextCoverPage:BasePage = null;
		private var tempPrevLastPage:BasePage = null;
		
		private var addPrevIdxCount:int = -1;
		
		private var btnPageLeft:Button;
		private var btnPageRight:Button;
		
		private var timer:Timer = new Timer(10, 0);
		
		public function PageContent()
		{
			
		}
		
		override protected function initialize():void
		{
			super.initialize();
			this.layout = new HorizontalLayout();
			
			this.horizontalScrollPolicy = ScrollPolicy.ON;
			this.verticalScrollPolicy = ScrollPolicy.OFF;
			this.snapScrollPositionsToPixels = true;
			this.minimumDragDistance = 0.2;
			
			this.snapToPages = true;
			this.hasElasticEdges = false;
			this.touchable = false;
			
			initPages(StudyInfo.currentChapter);
			
			this.addEventListener(FeathersEventType.SCROLL_START,  onScrollStart);
		}
		
		private function initPages(idxChapter:int, initOnLast:Boolean=false):void
		{
			var arrTargetChapter:Array = AppSettings.PAGE_CLASSES[idxChapter];
			if(arrTargetChapter)
			{
				var len:int = arrTargetChapter.length;
				this.removeChildren();
				vecPages.length = 0;
				for(var i:int=0; i<len; ++i)
				{
					if(arrTargetChapter[i] != null)
					{
						var mPage:BasePage = new arrTargetChapter[i] as BasePage;
						mPage.setIndexs(idxChapter, i+1);
						mPage.loadContent();
						vecPages.push(mPage);
						this.addChild(mPage);
						
						if (i == 0 || i == 1)
							vecPages[i].visible = true;
						else
							vecPages[i].visible = false;
					}
				}
			}
			vecPages[0].activate();
			
			this.touchable = true;
			
			trace("loading pages...");
			timer.addEventListener(TimerEvent.TIMER, onTickLoadCheck);
			timer.start();
		}
		
		public function destroyPages():void
		{
			for (var i:int = 0; i < vecPages.length; i++)
				vecPages[i].unloadContent();
		}
		
		private function onTickLoadCheck(event:TimerEvent):void
		{
			for(var i:int=0; i< vecPages.length; ++i)
			{
				if (false == vecPages[i].isLoaded)
					return;
			}
			trace("pages are loaded");
			timer.stop();
			timer.removeEventListener(TimerEvent.TIMER, onTickLoadCheck);
			this.dispatchEventWith(PageEvent.LOADED,true);
		}
		
		public function resizeScreen(width:int, height:int):void
		{
			for (var i:int = 0; i < vecPages.length; i++)
				vecPages[i].resizeScroll(width, height);
		}
		
		private function onScrollStart():void
		{
			isContentScrolling = true;
			this.removeEventListener(FeathersEventType.SCROLL_START,  onScrollStart);
			this.addEventListener(FeathersEventType.SCROLL_COMPLETE,  onScrollComplete);
		}
		
		private function onScrollComplete():void
		{
			isContentScrolling = false;
			this.removeEventListener(FeathersEventType.SCROLL_COMPLETE,  onScrollComplete);
			this.addEventListener(FeathersEventType.SCROLL_START,  onScrollStart);
			
			//this.horizontalPageIndex는 0부터 시작
			StudyInfo.currentPage = this.horizontalPageIndex;
			trace("onScrollComplete - currentPage: " + this.horizontalPageIndex);
			var title:String = AppSettings.PAGE_CATEGORY_TITLES[StudyInfo.currentChapter][this.horizontalPageIndex];
		
			if (-1 != prevPageIndex)
			{
				vecPages[this.prevPageIndex].deactivate();
			}
			vecPages[this.horizontalPageIndex].activate();
			
			for (var i:int = 0; i < vecPages.length; i++)
			{
				if (this.horizontalPageIndex - 1< i || i < this.horizontalPageIndex + 1)
					vecPages[i].visible = true;
				else
					vecPages[i].visible = false;
			}
					
			this.prevPageIndex = this.horizontalPageIndex;
			
			if ("" != title)
				AppSettings.pageNavigator.dispatchEventWith(PageNavigator.ON_SCROLL_HEAD_CHANGE, false, title);
		}
	}
}