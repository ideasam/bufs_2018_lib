package component
{
	import flash.geom.Rectangle;

	public class RectItem
	{
		
		public function RectItem(bounds:Rectangle = null)
		{
			this.bounds = bounds;
		}
		
		public var min:int = 0;
		public var sec:int = 0;
		public var mil:int = 0;
		public var bounds:Rectangle;
	}
}