package pages.type
{
	import com.greensock.TweenMax;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.DataLoader;
	import com.greensock.loading.LoaderMax;
	
	import flash.net.URLRequest;
	
	import app.AppSettings;
	import app.LocalSavedData;
	
	import component.ConfirmDialog;
	import component.ConfirmMessage;
	import component.RectItem;
	
	import feathers.controls.Button;
	import feathers.controls.DecelerationRate;
	import feathers.controls.ImageLoader;
	import feathers.controls.ScrollContainer;
	import feathers.controls.TextInput;
	import feathers.core.PopUpManager;
	import feathers.events.FeathersEventType;
	
	import net.NetManager;
	
	import pages.PageContent;
	
	import starling.display.Image;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;

	public class TestPage extends BasePage
	{
		private var _loaderQueue:LoaderMax = null;
		private var _dataLoaderJsonRect:DataLoader = null;
		
		private var imgLoader:ImageLoader = null;
		private var scrollCont:ScrollContainer;
		
		private var btnSubmit:Button;
		
		private var vecInputText:Vector.<TextInput> = new Vector.<TextInput>();
		
		private var savedAnswers:Object = null;
		
		public function TestPage()
		{
			super();
		}
		
		override public function draw():void
		{
			scrollCont = new ScrollContainer();
			scrollCont.minimumDragDistance = 0.2;
			scrollCont.width = AppSettings.width;
			scrollCont.height = AppSettings.height;
			this.addChild(scrollCont);
			
			scrollCont.decelerationRate = DecelerationRate.FAST;
			
			imgLoader = new ImageLoader();
			scrollCont.addChild(imgLoader);
			
			btnSubmit = new Button();
			btnSubmit.defaultSkin	= new Image(Root.assets.getTexture("btnSendN"));
			btnSubmit.disabledSkin  = new Image(Root.assets.getTexture("btnSendD"));
			btnSubmit.downSkin		= new Image(Root.assets.getTexture("btnSendS"));
			scrollCont.addChild(btnSubmit);
			
			btnSubmit.isEnabled = false;
			btnSubmit.validate();
			
			btnSubmit.x = (AppSettings.width - btnSubmit.width)>>1; 
			btnSubmit.addEventListener(Event.TRIGGERED, function():void{
				PopUpManager.addPopUp(new ConfirmDialog("답안을 제출하시겠습니까?\n(제출 후, 내용을 수정할 수 없습니다.)", onTriggeredBtnSubmit));
			});
			super.draw();
		}
		
		override public function loadContent():void
		{
			imgLoader.source = new URLRequest("data/img/chapter"+idxChapter+"/page"+idxPage+".png").url;
			
			if(!_dataLoaderJsonRect)
			{
				_dataLoaderJsonRect = new DataLoader("data/rect/ch"+idxChapter+"_"+idxPage+".txt", {name:TAG_RECT});
				_loaderQueue = new LoaderMax({name:"mainQueue", onComplete:completeHandler});
				_loaderQueue.append(_dataLoaderJsonRect);
				_loaderQueue.load();
			}
			
			super.loadContent();
		}
		
		private var prevTouchIdx:int = -1;
		private var vecSoundItem:Vector.<RectItem> = new Vector.<RectItem>;
		
		override public function activate():void
		{
			//super.activate();
			NetManager.getTestUserAnswer(
				LocalSavedData.getUserData().uid,
				String(idxChapter),
				function(dataStr:String):void{
					var result:Object = JSON.parse(dataStr);
					
					if(result && result.length > 0)
					{
						for(var i:int=0; i<result.length;++i)
						{
							savedAnswers = result;
							if(vecInputText && vecInputText.length > 0)
							{
								vecInputText[result[i].no].text = String(result[i].answer);
								//btnSubmit.isEnabled = true;
								
								if(String(result[i].answer).length > 0)vecInputText[result[i].no].isEditable = false;
								
								btnSubmit.isEnabled = false;
							}
						}
					}
				},onFailGetUserTestAnswer);
		}
		
		
		private function onFailGetUserTestAnswer():void
		{
			trace("onFailGetUserTestAnswer");
		}
		
		private function completeHandler(event:LoaderEvent):void 
		{
			var strJsonRect:String = LoaderMax.getContent(TAG_RECT);
			
			if(!strJsonRect) return;
			
			var arrTimes:Array = strJsonRect.split("\n");
			
			scrollCont.addChildAt(rectEffect,0);
			
			var jsData:Object = JSON.parse(strJsonRect);
			
			var len:int = jsData.rects.length;
			
			for(var i:int=0; i<len;++i)
			{
				var inputText:TextInput = new TextInput();
				inputText.paddingLeft = 10;
				inputText.paddingRight = 10;
				inputText.x = jsData.rects[i].x;
				inputText.y = jsData.rects[i].y;
				inputText.width = jsData.rects[i].width;
				inputText.height = jsData.rects[i].height;
				//inputText.prompt = ""
				scrollCont.addChild(inputText);
				inputText.addEventListener(FeathersEventType.FOCUS_OUT, checkAllFill);
				vecInputText.push(inputText);
				
				if(savedAnswers)
				{
					if(i == savedAnswers[i].no) inputText.text = String(savedAnswers[i].answer); 
					btnSubmit.isEnabled = true;
				}
			}
			
			btnSubmit.y = vecInputText[len-1].y + 95;
			
		}
		
		private function checkAllFill(event:*):void
		{
			var len:int = vecInputText.length;
			
			if(vecInputText.length ==0) return;
			
			for(var i:int=0; i<len;++i)
			{
				if(vecInputText[i].text == null || vecInputText[i].text =="" || vecInputText[i].text ==" ")
				{
					btnSubmit.isEnabled = false;
					break;
				}
				else
				{
					btnSubmit.isEnabled = true;
				}
			}
		}
		
		private function onTouchCont(event:TouchEvent):void
		{
			if(scrollCont.isScrolling || PageContent.isContentScrolling) return;
			
			var touch:Touch = event.getTouch(imgLoader);
			
			if(touch)
			{
				switch(touch.phase)
				{
					case TouchPhase.BEGAN:
						prevTouchIdx = getTouchAreaIdx();
						break;
					
					case TouchPhase.ENDED:
						
						var curTouchIdx:int = getTouchAreaIdx();
						
						if(prevTouchIdx > -1 && curTouchIdx>-1)
						{
							//this.dispatchEventWith(Event.CHANGE, false, curTouchIdx+1);
							/*rectEffect.filter = null;
							
							rectEffect.x = vecSoundItem[curTouchIdx].bounds.x;
							rectEffect.y = vecSoundItem[curTouchIdx].bounds.y;
							rectEffect.width = vecSoundItem[curTouchIdx].bounds.width;
							rectEffect.height = vecSoundItem[curTouchIdx].bounds.height;
							
							rectEffect.filter = new GlowFilter(0x29CDC8,0.8,8);
							rectEffect.visible = true;*/
							
						}
						
						prevTouchIdx = -1;
						break;
				}
			}
			
			function getTouchAreaIdx():int
			{
				var len:int =vecSoundItem.length;
				for (var i:int=0; i<len; ++i)
				{
					if(vecSoundItem[i].bounds.containsPoint(touch.getLocation(imgLoader)))
					{
						return i;
					}
				}
				return -1;
			}
		}
		
		private var confirmMSG:ConfirmMessage = null;
		 
		private function onTriggeredBtnSubmit():void
		{
			btnSubmit.isEnabled = false;
			
			if(confirmMSG) confirmMSG.removeFromParent(true);
			
			confirmMSG = new ConfirmMessage();
			stage.addChild(confirmMSG);
			
			confirmMSG.x = (AppSettings.width - confirmMSG.width)>>1;
			confirmMSG.y = AppSettings.height * 0.8;
			
			confirmMSG.visible = false;
			
			
			var len:int =vecInputText.length;
			
			// 0 미입력
			var resCode:int = 1;
			
			/*for(var i:int=0; i<len;++i)
			{
				if(vecInputText[i].text == "" || vecInputText[i].text==null)
				{
					resCode = 0;
					break;
				}
			}*/
			
			
			var cnt:int=0;
			
			if(resCode == 1)
			{
				for(var i:int=0; i<len;++i)
				{
					NetManager.updateTestResult(
						LocalSavedData.getUserData().uid,
						String(idxChapter), 
						String(i),
						vecInputText[i].text,
						onCompleteUpdate,onFailUpdate);
				}
			}
			
			function onCompleteUpdate():void
			{
				cnt++;
				
				if(cnt == len)
				{
					confirmMSG.visible = true;
					btnSubmit.isEnabled = false;
					
					confirmMSG.text = "제출에 성공하였습니다."
					
					TweenMax.to(confirmMSG, 0.5, {alpha:0, delay:2, onComplete:function():void{
						if(confirmMSG)confirmMSG.removeFromParent(true);
						
						for(var i:int=0; i<len;++i)
						{
							vecInputText[i].isEditable = false;
						}
					}});
				}
			}
			
			function onFailUpdate():void
			{
				confirmMSG.visible = true;
				btnSubmit.isEnabled = true;
				
				confirmMSG.text = "네트워크 연결을 확인해주세요."
					
				TweenMax.to(confirmMSG, 0.5, {alpha:0, delay:2, onComplete:function():void{
					if(confirmMSG)confirmMSG.removeFromParent(true);
				}});
			}
		}
		
		override public function unloadContent():void
		{
			super.unloadContent();
			
			imgLoader.source = null;
			
			_dataLoaderJsonRect = null;
			
			if(confirmMSG) confirmMSG.removeFromParent(true);
		}
	}
}