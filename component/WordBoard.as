package component
{
	import starling.utils.Align;
	import feathers.text.BitmapFontTextFormat;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.core.ITextRenderer;
	import app.AppSettings;
	import starling.textures.Texture;
	import feathers.layout.VerticalAlign;
	import feathers.controls.ImageLoader;
	
	import feathers.layout.HorizontalAlign;
	import theme.CustomTheme;
	import starling.text.TextFormat;
	import feathers.controls.Label;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	import starling.events.Event;
	import starling.events.TouchPhase;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	//import utils.DrawingTools;
	import starling.display.Image;
	import starling.display.Sprite;

	/**
	 * @author everseen
	 */
	public class WordBoard extends Sprite
	{
		public static const MAX_HEIGHT:int = 692;
		public var matHeight:int = 0;
		
		private var largeLabel:Label;
		private var largeBMPLabel:Label;
		private var prnLabel:Label;
		
		private var contMat:Sprite;
		private var mHeight:int = 0;
		//private var drawingTools:DrawingTools;
		private var imgStroke:ImageLoader;
		private var tfmLargeLabel:TextFormat;
		private var myCanvas:MyCanvas;
		
		//katakana
		private var currentPrnMode:String = "hiragana";
		public var maxBound:Rectangle;
		

		public function WordBoard($height:int)
		{
			mHeight = $height;
			
			
			drawMatBg();
			drawLine();
			
//			if(AppSettings.isTablet)
//			{
//				maxBound = new Rectangle(50,contMat.bounds.y + 10, 540, contMat.bounds.height - 145);
//			}
//			else
//			{
//				maxBound = new Rectangle(50,contMat.bounds.y + 70, 540, contMat.bounds.height - 120);
//				
//				trace(contMat.bounds);
//			}
			
			this.addEventListener(Event.REMOVED_FROM_STAGE, onDispose);
		}
		
		private function drawLine():void
		{
//			drawingTools = new DrawingTools();
//			drawingTools.lineStyle(30);
//			drawingTools.renderAll();
			
			myCanvas =new MyCanvas();
			myCanvas.lineStyle(30);
			this.addChild(myCanvas);
			//this.addEventListener(TouchEvent.TOUCH, onTouchToDraw);
		}
		
		
		private var isTouchBegan:Boolean = false;
		private function onTouchToDraw(event:TouchEvent):void
		{
			//var touch:Touch = event.getTouch(stage);
			var touch:Touch = event.getTouch(stage);
			
			if(!touch) return;
			
			var point:Point = new Point(touch.globalX, touch.globalY);
			var mBound:Rectangle = maxBound;
			if(!mBound.containsPoint(point)) 
			{
				isTouchBegan = false;
				return;
			}
			
			var location : Point = touch.getLocation(stage);
			
			switch(touch.phase)
			{
				case TouchPhase.BEGAN :
					isTouchBegan = true;
					//drawingTools.moveTo(touch.globalX, touch.globalY);
					//trace(touch.globalY);
					myCanvas.moveTo(myCanvas.globalToLocal(location).x, myCanvas.globalToLocal(location).y);
					break;
				case TouchPhase.MOVED :	
					if(isTouchBegan)
					{
						var dx : Number = touch.globalX - touch.previousGlobalX,
	            			dy : Number = touch.globalY - touch.previousGlobalY,
	            			ds : Number = Math.sqrt (dx * dx + dy * dy);
						
						if(ds > 1.5) myCanvas.lineTo(myCanvas.globalToLocal(location).x, myCanvas.globalToLocal(location).y);
						//if(ds > 1.5) drawingTools.lineTo(touch.globalX, touch.globalY);	
					}
					break;
				case TouchPhase.ENDED :
					isTouchBegan = false;
					//drawingTools.renderAll(this);
					break;
			}
		}
		
		private function drawMatBg():void
		{
			contMat = new Sprite();
			this.addChild(contMat);
			
			var exY:int = 0;
			
			if(MAX_HEIGHT > mHeight)
			{
				exY = (MAX_HEIGHT -mHeight)>>1;
				if(exY> 50) exY = 50;
			}
			
			var bgTop:Image = new Image(Root.assets.getTexture("letter/matTop"));
			contMat.addChild(bgTop);
			
			var bgCenter:Image = new Image(Root.assets.getTexture("letter/matCenter"));
			bgCenter.y = bgTop.height - exY;
			contMat.addChildAt(bgCenter,0);
			
			var bgBottom:Image = new Image(Root.assets.getTexture("letter/matBottom"));
			bgBottom.y = bgCenter.y + bgCenter.height - exY;
			contMat.addChild(bgBottom);
			
			matHeight = bgBottom.y + bgBottom.height;
			
			tfmLargeLabel = new TextFormat(CustomTheme.FONT_HINDI, 350, 0x85A0DE, HorizontalAlign.CENTER, VerticalAlign.TOP);
			tfmLargeLabel.bold = true;
			
			largeLabel = new Label();
			largeLabel.fontStyles = tfmLargeLabel;
			largeLabel.width = 560;
			largeLabel.height = 430;
			largeLabel.touchable = false;
			contMat.addChild(largeLabel);
			
			
			largeLabel.x = ((contMat.width - largeLabel.width)>>1);
			largeLabel.y = contMat.height * 0.01;//(contMat.height - largeLabel.height)>>1;
			
			//예외 폰트
//			var bformat:BitmapFontTextFormat = new BitmapFontTextFormat( "bmfont" );
//			bformat.size = 390;
//			bformat.color = 0x85A0DE;
//			bformat.align = Align.CENTER;
//			
//			largeBMPLabel = new Label();
//			largeBMPLabel.textRendererFactory =  function():ITextRenderer
//			{
//			    var textRenderer:BitmapFontTextRenderer = new BitmapFontTextRenderer();
//				textRenderer.textFormat = bformat;
//				//textRenderer.selectedTextFormat = b_selformat;
//			    return textRenderer;
//			};
//			largeBMPLabel.width = 560;
//			largeBMPLabel.height = 430;
//			largeBMPLabel.touchable = false;
//			contMat.addChild(largeBMPLabel);
//			largeBMPLabel.x = ((contMat.width - largeLabel.width)>>1);
//			largeBMPLabel.y = contMat.height * 0.01;//(contMat.height - largeLabel.height)>>1;
			//
			
			imgStroke = new ImageLoader();
			imgStroke.x = 68;
			imgStroke.y = largeLabel.y;
			imgStroke.visible = false;
			contMat.addChild(imgStroke);
			
			var prnTfm:TextFormat = tfmLargeLabel.clone();
			prnTfm.bold = false;
			prnTfm.size = 40;
			
			prnLabel = new Label();
			prnLabel.touchable = false;
			prnLabel.fontStyles = prnTfm;
			prnLabel.width = contMat.width;
			prnLabel.y = AppSettings.isTablet ? contMat.height * 0.76 : contMat.height * 0.72;
			contMat.addChild(prnLabel);
		}
		
		public function setText(text:String, prn:String = null):void
		{
			imgStroke.source = null;
			
			if(text.length > 1)
			{
				tfmLargeLabel.size = 300;
				tfmLargeLabel.letterSpacing = -50;
			}
			else
			{
				tfmLargeLabel.size = 350;
				tfmLargeLabel.letterSpacing = 0;
			}
			
//			if(text == "り" || text == "タ" || text == "ダ")
//			{
//				largeLabel.visible = false;
//				largeBMPLabel.visible = true;
//				largeBMPLabel.text = text;
//				
//				if(text == "り")
//				{
//					largeBMPLabel.paddingTop = 50;
//					largeBMPLabel.paddingLeft = -165; 
//				}
//				else if(text == "タ")
//				{
//					largeBMPLabel.paddingTop = 65;
//					largeBMPLabel.paddingLeft = -75; 
//				}
//				else if(text == "ダ")
//				{
//					largeBMPLabel.paddingLeft = 0;
//					largeBMPLabel.paddingTop = 55; 
//				}
//				else 
//				{
//					largeBMPLabel.paddingTop = 0;
//					largeBMPLabel.paddingLeft = 0;					
//				}
//				
//				largeBMPLabel.validate();
//			}
//			else
			{
				largeLabel.visible = true;
//				largeBMPLabel.visible = false;
				largeLabel.fontStyles = tfmLargeLabel;
				largeLabel.text = text;
				largeLabel.validate();
			}
			
			if(text.length > 1)
			{
				largeLabel.x = ((contMat.width - largeLabel.width)>>1) - 15;				
			}
			else
			{
				largeLabel.x = ((contMat.width - largeLabel.width)>>1);
//				largeBMPLabel.x = ((contMat.width - largeBMPLabel.width)>>1);
			}
			
			var txtPrn:String="";
			
			if(prn)
			{
				txtPrn = prn;
				if(txtPrn == "wo") txtPrn = "o";
				prnLabel.text = "[" + txtPrn + "]";
				
				prn = prn.toLocaleLowerCase();
				
				ExtPicLoader.loadTexture("embed/stroke/word_" + currentPrnMode + "_"+prn+".png", onCompleteLoad);
			}
		}

		private function onCompleteLoad(texture:Texture):void
		{
			if(imgStroke.source)
			{
				(imgStroke.source as Texture).dispose();
			}
			
			imgStroke.source = texture;
		}
		
		public function toggleStrokeMode(isToggled:Boolean):void
		{
			imgStroke.visible = isToggled;
		}
		
		public function toggleWriteMode(isToggled:Boolean):void
		{
			if(isToggled)
			{
				this.addEventListener(TouchEvent.TOUCH, onTouchToDraw);
				largeLabel.alpha = 0.5;
//				largeBMPLabel.alpha = 0.5;
			}
			else
			{
				largeLabel.alpha = 1;
//				largeBMPLabel.alpha = 1;
				//drawingTools.clearAll();
				myCanvas.clear();
				this.removeEventListener(TouchEvent.TOUCH, onTouchToDraw);
			}
		}
		
		public function changeHKMode(isHiragana:Boolean):void
		{
			//word_katakana_su
			clearDrawing();
			currentPrnMode = isHiragana ? "hiragana" : "katakana"; 
		}
		
		public function clearDrawing():void
		{
			//drawingTools.clearAll();
			myCanvas.clear();
		}
		
		private function onDispose(event:Event):void
		{
			if(imgStroke.source as Texture) 
			{
				(imgStroke.source as Texture).dispose();
			}
			this.removeEventListener(TouchEvent.TOUCH, onTouchToDraw);
			clearDrawing();
			//drawingTools.disposeAll();
		}
	}
}