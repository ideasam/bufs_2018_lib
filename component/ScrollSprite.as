package component
{
	import app.AppSettings;
	
	import feathers.controls.ScrollContainer;
	import feathers.controls.ScrollPolicy;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	
	public class ScrollSprite extends Sprite
	{
		public static const ON_RESIZE_OFFSET:String = "onResizeOffset";
		
		private var _scrollCont:ScrollContainer;
		private var backGround:Quad;
		
		public function ScrollSprite()
		{
			super();
			
			setScrollSprite(AppSettings.width, AppSettings.height);
			
		}
		public function get scrollCont():ScrollContainer
		{
			return _scrollCont;
		}
		
		public function get isScrolling():Boolean
		{
			return scrollCont.isScrolling;
		}
		
		private function setScrollSprite(width:int, height:int):void
		{
			backGround = new Quad(width, height, AppSettings.DefaultColor);
			
			_scrollCont = new ScrollContainer();
			scrollCont.width = width;
			scrollCont.height = height;
			this.addChild(scrollCont);
			scrollCont.addChild(backGround);
						
			scrollCont.minimumDragDistance = 0.2;
			scrollCont.verticalScrollPolicy = ScrollPolicy.ON;
		}
		
		//Scroll Size 늘리는 경우: 먼저 늘린 다음에 Tween처리
		//Scroll Size 줄이는 경우: 먼저 Tween처리한 다음에 줄이기
		public function resizeScroll(width:int, height:int):void
		{
			if (scrollCont)
			{
				var offset:int = int(scrollCont.height - height);
				scrollCont.width = width;
				scrollCont.height = height;
				backGround.width = width;
				dispatchEventWith(ON_RESIZE_OFFSET, false, offset);
			}
		}
	}
}