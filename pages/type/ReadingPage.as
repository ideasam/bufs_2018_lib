package pages.type
{
	import com.greensock.TweenMax;
	
	import flash.net.URLRequest;
	
	import app.AppSettings;
	
	import feathers.controls.DecelerationRate;
	import feathers.controls.ImageLoader;
	import feathers.controls.ScrollContainer;
	import feathers.events.FeathersEventType;
	
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	
	public class ReadingPage extends BasePage
	{
		private var imgLoader:ImageLoader = null;
		private var scrollCont:ScrollContainer;
		
		
		public function ReadingPage()
		{
			this.hasPlayerDock = true;
			this.hasLeftShadow = true;
			
			super();
			
			endIndicator.visible = false;
		}
		
		override public function draw():void
		{
			scrollCont = new ScrollContainer();
			scrollCont.width = AppSettings.width;
			scrollCont.height = AppSettings.height;
			
			scrollCont.minimumDragDistance = 0.2;
			this.addChild(scrollCont);
			
			scrollCont.decelerationRate = DecelerationRate.FAST;
			
			imgLoader = new ImageLoader();
			scrollCont.addChild(imgLoader);
			
			super.draw();
		}
		
		override public function loadContent():void
		{
			imgLoader.addEventListener(Event.COMPLETE, onCompleteLoad);
			imgLoader.source = new URLRequest("data/img/chapter"+idxChapter+"/page"+idxPage+".png").url;
			
			mPlayerDock.setSound(new URLRequest("data/sound/ch"+idxChapter +"_"+idxPage+".mp3").url);
			
			scrollCont.addEventListener(FeathersEventType.SCROLL_START, onScrollStart);
			scrollCont.addEventListener(TouchEvent.TOUCH, onTouchScrollCont);
			super.loadContent();
		}
		
		override public function activate():void
		{
			super.activate();
			
			//mPlayerDock.setSound(new URLRequest("data/sound/ch"+idxChapter +"_"+idxPage+".mp3").url);
		}
		
		private function onTouchScrollCont(touchEvent:TouchEvent):void
		{
			var touch:Touch = touchEvent.getTouch(this, TouchPhase.ENDED);
			if (touch)
			{
				if(touch.tapCount == 1 && !scrollCont.isScrolling)
				{
					if(mPlayerDock.alpha == 0)
					{
						TweenMax.to(mPlayerDock, 0.2, {alpha:1});
						mPlayerDock.touchable = true;
					}
					else
					{
						TweenMax.to(mPlayerDock, 0.2, {alpha:0});
						mPlayerDock.touchable = false;
					}
				}
			}
		}
		
		private function onScrollStart():void
		{
			TweenMax.to(mPlayerDock, 0.2, {alpha:0});
		}
		
		private function onCompleteLoad():void
		{
			imgLoader.removeEventListener(Event.COMPLETE, onCompleteLoad);
			imgLoader.validate();
			
			imgLoader.visible = true;
			
			//this.endIndicator.y = imgLoader.y + imgLoader.height+20;
			//scrollCont.addChild(endIndicator);
		}
		
		override public function unloadContent():void
		{
			super.unloadContent();
			scrollCont.removeEventListener(TouchEvent.TOUCH, onTouchScrollCont);
			scrollCont.removeEventListener(FeathersEventType.SCROLL_START, onScrollStart);
			imgLoader.source = null;
		}
	}
}