package pages.type
{
	import flash.net.URLRequest;
	
	import app.AppSettings;
	import feathers.controls.ImageLoader;
	
	public class InformationPage extends BasePage
	{
		private var imgLoader:ImageLoader = null;
		
		public function InformationPage()
		{
			super();
		}
		
		override public function loadContent():void
		{
			draw();
			isLoaded = true;
		}
		
		override public function unloadContent():void
		{
			super.unloadContent();
			imgLoader.source = null;
		}
		
		override public function draw():void
		{
			super.draw();
			
			imgLoader = new ImageLoader();
			imgLoader.source = new URLRequest("data/img/chapter"+idxChapter+"/page"+idxPage+".png").url;
			scrollCont.addChild(imgLoader);
		}
	}
}