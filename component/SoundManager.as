package component
{
	import com.greensock.loading.MP3Loader;
	
	import flash.events.Event;

	public class SoundManager
	{
		private static var mp3Loader:MP3Loader;
		private static var mp3Loader_clockcount:MP3Loader;
		private static var mp3Loader_Bgm:MP3Loader;
		private static var currentPlayUrl:String = null;
		
		public function SoundManager()
		{
		}
		
		public static function playGameBgm():void
		{
			mp3Loader_Bgm=new MP3Loader("sound/ui/bgm.mp3",{name:"bgm",autoPlay:true,repeat:-1});
			//mp3Loader_Bgm.addEventListener(MP3Loader.SOUND_COMPLETE, onSoundComplete);
			mp3Loader_Bgm.load();
		}
		
		public static function resumeGameBgm():void
		{
			if(mp3Loader_Bgm) mp3Loader_Bgm.playSound();
		}
		
		public static function pauseGameBgm():void
		{
			if(mp3Loader_Bgm) mp3Loader_Bgm.pauseSound();
		}
		
		public static function stopGameBgm():void
		{
			if(mp3Loader_Bgm) 
			{
				mp3Loader_Bgm.pause();
				mp3Loader_Bgm.pauseSound();
				mp3Loader_Bgm.unload();
				mp3Loader_Bgm = null;
			}
		}
		
		public static function playClockCount():void
		{
			//mp3Loader_clockcount=new MP3Loader("sound/ui/count-down-clock.mp3",{name:"count-down-clock",autoPlay:true,repeat:-1});
			//mp3Loader_clockcount.load();
		}
		
		public static function resumeClockCount():void
		{
			//if(mp3Loader_clockcount) mp3Loader_clockcount.playSound();
		}
		
		public static function pauseClockCount():void
		{
			//if(mp3Loader_clockcount) mp3Loader_clockcount.pauseSound();
		}
		
		public static function stopClockCount():void
		{
//			if(mp3Loader_clockcount) 
//			{
//				mp3Loader_clockcount.pause();
//				mp3Loader_clockcount.pauseSound();
//				mp3Loader_clockcount.unload();
//				mp3Loader_clockcount = null;
//			}
		}
		
		public static function resume():void
		{
			if(mp3Loader)
			{
				mp3Loader.playSound();
			}
		}
		
		public static function pause():void
		{
			
			if(mp3Loader)
			{
				mp3Loader.pauseSound();
			}
		}
		
		public static function stop():void
		{
			if(mp3Loader)
			{
				mp3Loader.pauseSound();
				mp3Loader.pause();
				mp3Loader.unload();
				mp3Loader = null;
			}
			currentPlayUrl = null;
		}
		
		
		
		public static function play(url:String, onComplete:Function=null):void
		{
			
			stop();
			
			currentPlayUrl = url;

			mp3Loader=new MP3Loader(url,{name:url,autoPlay:true,repeat:0});
			mp3Loader.addEventListener(MP3Loader.SOUND_COMPLETE, onSoundComplete);
			mp3Loader.load();
			
			function onSoundComplete(event:Event):void
			{
				if(null != onComplete) onComplete();
			}
			
		} 
	}
}