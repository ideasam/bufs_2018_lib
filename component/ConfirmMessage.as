package component
{
	import flash.geom.Rectangle;
	
	import feathers.controls.Button;
	import feathers.controls.Label;
	import feathers.core.PopUpManager;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.VerticalAlign;
	import feathers.skins.ImageSkin;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextFormat;
	
	import theme.BaseTheme;
	import theme.CustomTheme;
	
	public class ConfirmMessage extends Sprite
	{
		private var _btnConfirm	:Button;
		private var label:Label;
		
		public function ConfirmMessage()
		{
			super();
			
			drawUI();
		}
		
		public function set text(value:String):void
		{
			label.text = value;
		}
		
		private function drawUI():void
		{
			var _bg:ImageSkin = new ImageSkin(Root.assets.getTexture("StBgResult"));
			_bg.scale9Grid = new Rectangle(20,20,10,10); 
			_bg.width = 480;
			_bg.height = 120;
			//_bg.color = 0xd0d0d0;
			_bg.alpha = 0.8;
			this.addChild(_bg);
			
			label = new Label();
			label.x = 15;
			label.y = 35;
			label.fontStyles = new TextFormat(CustomTheme.FONT_NAME, 24, 0x000000, HorizontalAlign.CENTER, "center");
			label.width = 450;
			label.height = 50;
			this.addChild( label );
			
			label.text = "";
			
			/*_btnConfirm = new Button;
			_btnConfirm.label = "확인";
			_btnConfirm.width = 180;
			_btnConfirm.minTouchWidth = 150;
			_btnConfirm.minTouchHeight = 100;
			this.addChild(_btnConfirm);
			
			_btnConfirm.x = (_bg.width - _btnConfirm.width) >> 1;
			_btnConfirm.y = 220;
			
			_btnConfirm.addEventListener(Event.TRIGGERED, onTriggeredConfirm);*/
		}
		
		private function onTriggeredConfirm():void
		{
			PopUpManager.removePopUp(this);
		}		
		
	}
}