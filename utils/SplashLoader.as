package utils
{
	import com.greensock.TweenLite;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.LoaderMax;
	import com.greensock.loading.SWFLoader;
	import com.greensock.loading.display.ContentDisplay;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.system.ApplicationDomain;
	import flash.system.Capabilities;
	import flash.system.LoaderContext;
	
	import app.AppSettings;
	
	import starling.display.DisplayObject;
	import starling.utils.SystemUtil;
	
	public class SplashLoader
	{
		private static var _root:Sprite = null;
		
		public static function init(root:Sprite, swfPath:String, time:int, func:Function, isScaling:Boolean = true):void
		{
			_root = root;
			
			var queue:LoaderMax = new LoaderMax({name:"mainQueue", onProgress:progressHandler, onComplete:completeHandler, onError:errorHandler});
			
			var myContext:LoaderContext = new LoaderContext(false, ApplicationDomain.currentDomain, null);
			var swfLoader:SWFLoader = new SWFLoader(swfPath, {name:"logo", estimatedBytes:time, context:myContext, container:root, x:0, autoPlay:true, scaleMode:"proportionalOutside"}) ;
			queue.append( swfLoader );
			LoaderMax.prioritize("logo"); 
			queue.load();
			
			
			function progressHandler(event:LoaderEvent):void
			{
				   // trace("progress: " + event.target.progress);
			}
			
			function completeHandler(event:LoaderEvent):void
			{
				var swf:ContentDisplay = LoaderMax.getContent("logo");
				
				var scrW:int;
				var scrH:int;
				
				if (SystemUtil.isDesktop)
				{
					scrW = AppSettings.width;
					scrH = AppSettings.height;
				}
				else
				{
					scrW = Capabilities.screenResolutionX;
					scrH = Capabilities.screenResolutionY;
					
					/*var ratio:Number = scrW/swf.width;
					
					swf.width = int(swf.width * ratio);
					swf.height = int(swf.height * ratio);*/
					
					swfLoader.content.scaleMode = "proportionalOutside";
					swfLoader.content.fitWidth = scrW; 
					swfLoader.content.fitHeight = scrH;
				}
				
				
				trace("2:",swf.width, swf.height);
				
				if (isScaling)
				{
					//swf.scaleX = NScreen.scaleNum;
					//swf.scaleY = NScreen.scaleNum;
					
					swf.x = (scrW-swf.width)/2;
					swf.y = (scrH-swf.height)/2;
				}
				else
				{
					if (SystemUtil.isDesktop) 
					{
						swf.x = (scrW-swf.width)/2;
						swf.y = (scrH-swf.height)/2;
					}
				}
				
				    TweenLite.to(swf, 0.5, {alpha:1});
				    trace(event.target + " is complete!");
				
				TweenLite.to(swf, time/1000, {onComplete:func});
			}
			  
			function errorHandler(event:LoaderEvent):void
			{
				    trace("error occured with " + event.target + ": " + event.text);
			}
		}
		
		public static function terminate():void
		{
			if(_root==null) return;
			var swf:ContentDisplay = LoaderMax.getContent("logo");
			
			swf.visible = false;
			_root.removeChild(swf);
			swf = null;
		}
	}
}