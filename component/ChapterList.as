package component
{
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	
	import app.AppSettings;
	
	import feathers.controls.ImageLoader;
	
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class ChapterList extends ScrollSprite
	{
		private var listImage:ImageLoader;
		private var numChapter:int = 0;
		
		private var prevTouchIdx:int = -1;
		private var vecTouchRect:Vector.<Rectangle> = new Vector.<Rectangle>;
		
		private const MENU_ITEM_HEIGHT:int = 103;
		
		public function ChapterList()
		{
			super();
			
			if(numChapter ==0)
			{
				numChapter = AppSettings.PAGE_CLASSES.length;
				
				if(AppSettings.PAGE_CLASSES[0].length == 0)
				{
					numChapter = numChapter-1;
				}
			}
			
			draw();
		}
		
		private function draw():void
		{
			resizeScroll(AppSettings.width, AppSettings.height - 80);
			scrollCont.addEventListener(TouchEvent.TOUCH, onTouchCont);
			
			listImage = new ImageLoader();
			listImage.source = new URLRequest("data/img/menu/menu.png").url;
			scrollCont.addChild(listImage);
			
			for(var i:int=0; i<numChapter; ++i)
			{
				vecTouchRect.push(new Rectangle(0, i*MENU_ITEM_HEIGHT, 640, MENU_ITEM_HEIGHT))
			}
		}
		
		private function onTouchCont(event:TouchEvent):void
		{
			if(isScrolling) return;
			
			var touch:Touch = event.getTouch(listImage);
			
			if(touch)
			{
				switch(touch.phase)
				{
					case TouchPhase.BEGAN:
						prevTouchIdx = getTouchAreaIdx();
						break;
					
					case TouchPhase.ENDED:
						
						var curTouchIdx:int = getTouchAreaIdx();
						if(prevTouchIdx > -1 && curTouchIdx>-1)
						{
							this.dispatchEventWith(Event.CHANGE, false, curTouchIdx+1);
						}						
						prevTouchIdx = -1;
						break;
				}
			}
			
			function getTouchAreaIdx():int
			{
				for (var i:int=0; i<numChapter; ++i)
				{
					if(vecTouchRect[i].containsPoint(touch.getLocation(listImage)))
					{
						return i;
					}
				}
				return -1;
			}
		}
	}
}