package app
{
	import flash.net.SharedObject;

	public class LocalSavedData
	{
		private static var shObj:SharedObject;
		
		public static function init():void
		{
			if(!shObj) shObj = SharedObject.getLocal("kr.ac.bufs"); 
		}
		
		public static function get isAlreadyLogin():Boolean{return shObj.data.isFirstLogin;}
		public static function set isAlreadyLogin(value:Boolean):void{shObj.data.isFirstLogin = value; shObj.flush();} 
		
		public static function get latestStudyChapter():int{return shObj.data.latestStudyChapter;}
		public static function set latestStudyChapter(value:int):void{shObj.data.latestStudyChapter = value; shObj.flush();}
		
		public static function getUserData():Object
		{
			var mObject:Object = {uid:shObj.data.uid, upass:shObj.data.upass};
			return mObject;
		}
		public static function setUserData(uid:String, upass:String):void
		{
			shObj.data.uid = uid;
			shObj.data.upass = upass;
			shObj.flush();
		}
	}
}