package pages
{
	public interface IPage
	{
		function drawUI():void;
		
		function loadContent(idxChapter:int, idxPage:int):void;
		
		function unloadContent():void;
	}
}