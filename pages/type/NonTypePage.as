package pages.type
{
	import flash.net.URLRequest;
	
	import app.AppSettings;
	
	import feathers.controls.DecelerationRate;
	import feathers.controls.ImageLoader;
	import feathers.controls.ScrollContainer;
	
	import starling.events.Event;

	public class NonTypePage extends BasePage
	{
		private var imgLoader:ImageLoader = null;
		protected var scrollCont:ScrollContainer;
		
		public function NonTypePage()
		{
			super();
		}
		
		override public function draw():void
		{
			scrollCont = new ScrollContainer();
			scrollCont.minimumDragDistance = 0.2;
			scrollCont.width = AppSettings.width;
			scrollCont.height = AppSettings.height;
			this.addChild(scrollCont);
			
			scrollCont.decelerationRate = DecelerationRate.FAST;
			
			imgLoader = new ImageLoader();
			scrollCont.addChild(imgLoader);
			
			super.draw();
		}
		
		override public function loadContent():void
		{
			imgLoader.addEventListener(Event.COMPLETE, onCompleteLoad);
			imgLoader.source = new URLRequest("data/img/chapter"+idxChapter+"/page"+idxPage+".png").url;
			
			super.loadContent();
		}
		
		private function onCompleteLoad():void
		{
			imgLoader.removeEventListener(Event.COMPLETE, onCompleteLoad);
			imgLoader.validate();
			
			this.endIndicator.y = imgLoader.y + imgLoader.height+20;
			scrollCont.addChild(endIndicator);
		}
		
		override public function unloadContent():void
		{
			super.unloadContent();
			
			imgLoader.source = null;
		}
	}
}